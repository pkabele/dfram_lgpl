# DFraM - Discrete Fracture Network Modeler

## Decription
DFraM has two main functionalities: a) identification of DFN parameters using structural-geological data of fractures’ traces obtained from rock outcrops, tunnel walls and other 2D observations (DFN optimization); b) construction of a single or multiple random realizations of DFNs with given parameters (DFN simulation). 
The methods and algorithms implemented in DFraM are described in report:

KABELE P., ŠVAGERA O., SOMR M., NEŽERKA V., ZEMAN J., BUKOVSKÁ Z., FRANĚK J., JELÍNEK J., SOEJONO I. (2017): Mathematical Modeling of Brittle Fractures in Rock Mass by Means of the DFN Method. Final report, - SÚRAO - Radioactive Waste Repository Authority, Prague, Czech Republic.

## Authors and acknowledgment
The conceptual and theoretical background of DFraM is being developed by Petr Kabele, Václav Nežerka, Michael Somr and Jan Zeman at the Faculty of Civil Engineering, CTU in Prague.
Most of the coding has been done, so far, by Václav Nežerka and Michael Somr.

Financial support of DFraM development by the Czech Radioactive Waste Repository Authority - SÚRAO (www.surao.cz) is gratefully acknowledged.

## License
DFraM is licensed under GNU Lesser General Public License v.3 or any later version. See <https://www.gnu.org/licenses/>.

## Project status
DFraM is work in progress under active development.

## Contributing
At the moment, DFraM is being developed by a small group at CTU in Prague. Any interested contributors, please contact the maintainer, Petr Kabele <petr.kabele (at) fsv.cvut.cz>.

