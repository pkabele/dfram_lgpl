#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
import matplotlib.pyplot as plt
import numpy as np
# import scipy.stats as sc_stats
from stats import jsd
import xlwt


def hist_plots(lengths, n_bins, rgb, title, x_label, length_unit, y_label, output_file, n_figure):
    fig = plt.figure(n_figure)
    plt.hist(lengths, bins=n_bins, range=[min(lengths), max(lengths)], density=False, facecolor=rgb, align='mid')
    plt.xlabel(x_label)
    plt.ylabel('%s [%s]' % (y_label, length_unit))
    plt.grid(True)
    fig.canvas.set_window_title(title)
    plt.savefig(output_file)
    plt.close(fig)


def hist_plots_with_range(lengths, n_bins, input_range, rgb, title, x_label, length_unit, y_label, output_file, n_figure):
    fig = plt.figure(n_figure)
    plt.hist(lengths, bins=n_bins, range=input_range, density=False, facecolor=rgb, align='mid')
    plt.xlabel(x_label)
    plt.ylabel('%s [%s]' % (y_label, length_unit))
    plt.grid(True)
    fig.canvas.set_window_title(title)
    plt.savefig(output_file)
    plt.close(fig)


def create_cdf(x):
    y = [0.0]*len(x)
    for i, xi in enumerate(x):
        for xj in x:
            if xi <= xj:
                y[i] += 1.0
    max_y = max(y)
    y_norm = [k/max_y for k in y]
    return y_norm


def log_plots(lengths, rgb, title, length_unit, output_file, n_figure):
    y = create_cdf(lengths)
    fig = plt.figure(n_figure)
    ax = fig.gca()
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.plot(lengths, y, 'o', color=rgb, markersize=2)
    plt.ylabel(r'$P_{\mathrm{trace}} \leq$ trace length $l$')
    plt.xlabel(r'Trace length $l$ [\mathrm{%s}]$' % length_unit)
    plt.grid(True)
    fig.canvas.set_window_title(title)
    plt.savefig(output_file)
    plt.close(fig)


def plot_iteration_alpha(alphas_plot, errors_plot, popopulation, rgb, min_idx, output_file, n_figure):
    fig = plt.figure(n_figure)
    errors_plot = [i*100 for i in errors_plot]
    ticks_plot = [i for i in range(len(errors_plot))]
    ticks_labels = ['%d' % int(i) for i in range(len(errors_plot))]
    plt.plot(errors_plot, '-o', color=rgb)
    plt.plot(min_idx, errors_plot[min_idx], 'o', color=rgb, markersize=8)
    plt.plot(min_idx, errors_plot[min_idx], 'o', color='w', markersize=6)
    ax = fig.gca()
    for idx, a in enumerate(alphas_plot):
        if idx == len(alphas_plot)-1:
            if len(alphas_plot) == 0:
                ax.annotate(r'$\alpha$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx+0.4, errors_plot[idx]))
            else:
                ax.annotate(r'$\alpha$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx-0.4, 5+errors_plot[idx]))
        else:
            ax.annotate(r'$\alpha$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx, 5+errors_plot[idx]))
    plt.ylabel('Iteration step [%]')
    plt.xlabel('Iteration number')

    # set y-limits
    plt.ylim(bottom=0)
    _, cur_ymax = plt.ylim()
    real_ymax = max(cur_ymax, 30)
    plt.ylim(top=real_ymax)

    # set x-limits
    plt.xlim(left=0)
    _, cur_xmax = plt.xlim()
    real_xmax = max(cur_xmax, 3)
    plt.xlim(right=real_xmax)

    plt.xticks(ticks_plot, ticks_labels)
    plt.grid(True)
    fig.canvas.set_window_title('Fracture size optimization, population %d' % popopulation)
    plt.savefig(output_file)
    plt.close(fig)


def plot_iteration_of(opt_var, x_plot, ofs_plot, rgb, min_idx, output_file, n_figure):
    fig = plt.figure(n_figure)
    plt.plot(x_plot, ofs_plot, '--o', color=[0.75, 0.75, 0.75])
    plt.plot(x_plot[min_idx], ofs_plot[min_idx], 'o', color=rgb, markersize=8)
    plt.plot(x_plot[min_idx], ofs_plot[min_idx], 'o', color='w', markersize=6)
    ax = fig.gca()
    for idx in range(len(ofs_plot)):
        if idx == len(ofs_plot)-1:
            if len(ofs_plot) == 0:
                ax.annotate(r'%d' % idx, xy=(x_plot[idx], ofs_plot[idx]), xytext=(x_plot[idx], ofs_plot[idx]))
            else:
                ax.annotate(r'%d' % idx, xy=(x_plot[idx], ofs_plot[idx]), xytext=(x_plot[idx], ofs_plot[idx]))
        else:
            ax.annotate(r'%d' % idx, xy=(x_plot[idx], ofs_plot[idx]), xytext=(x_plot[idx], ofs_plot[idx]))
    if opt_var == 'alpha':
        plt.xlabel(r'$\alpha$')
    elif opt_var == 'x_min':
        plt.xlabel(r'$x_{\mathrm{min}}$')
    else:
        plt.xlabel(r'$P_{30}$')
    plt.ylabel('OF value')
    plt.grid(True)
    plt.savefig(output_file)
    plt.close(fig)


def plot_iteration_x_min(x_mins_plot, errors_plot, popopulation, rgb, min_idx, output_file, n_figure):
    fig = plt.figure(n_figure)
    errors_plot = [i*100 for i in errors_plot]
    ticks_plot = [i for i in range(len(errors_plot))]
    ticks_labels = ['%d' % int(i) for i in range(len(errors_plot))]
    plt.plot(errors_plot, '-o', color=rgb)
    plt.plot(min_idx, errors_plot[min_idx], 'o', color=rgb, markersize=8)
    plt.plot(min_idx, errors_plot[min_idx], 'o', color='w', markersize=6)
    ax = fig.gca()
    for idx, a in enumerate(x_mins_plot):
        if idx == len(x_mins_plot)-1:
            if len(x_mins_plot) == 0:
                ax.annotate(r'$x_{\mathrm{min}}$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx+0.4, errors_plot[idx]))
            else:
                ax.annotate(r'$x_{\mathrm{min}}$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx-0.4, 5+errors_plot[idx]))
        else:
            ax.annotate(r'$x_{\mathrm{min}}$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx, 5+errors_plot[idx]))
    plt.ylabel('Iteration step [%]')
    plt.xlabel('Iteration number')

    # set y-limits
    plt.ylim(bottom=0)
    _, cur_ymax = plt.ylim()
    real_ymax = max(cur_ymax, 30)
    plt.ylim(top=real_ymax)

    # set x-limits
    plt.xlim(left=0)
    _, cur_xmax = plt.xlim()
    real_xmax = max(cur_xmax, 3)
    plt.xlim(right=real_xmax)

    plt.xticks(ticks_plot, ticks_labels)
    plt.grid(True)
    fig.canvas.set_window_title('Fracture size optimization, population %d' % popopulation)
    plt.savefig(output_file)
    plt.close(fig)


def plot_iteration_fr_dens(fr_dens_plot, density, errors_plot, popopulation, rgb, min_idx, output_file, n_figure):
    fig = plt.figure(n_figure)
    errors_plot = [i*100 for i in errors_plot]
    ticks_plot = [i for i in range(len(errors_plot))]
    ticks_labels = ['%d' % int(i) for i in range(len(errors_plot))]
    plt.plot(errors_plot, '-o', color=rgb)
    plt.plot(min_idx, errors_plot[min_idx], 'o', color=rgb, markersize=8)
    plt.plot(min_idx, errors_plot[min_idx], 'o', color='w', markersize=6)
    ax = fig.gca()
    for idx, a in enumerate(fr_dens_plot):
        if idx == len(fr_dens_plot)-1:
            if len(fr_dens_plot) == 0:
                ax.annotate(r'$P_{30}$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx+0.4, errors_plot[idx]))
            else:
                ax.annotate(r'$P_{30}$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx-0.4, 5+errors_plot[idx]))
        else:
            ax.annotate(r'$P_{30}$ = %.3f' % a, xy=(idx, errors_plot[idx]), xytext=(idx, 5+errors_plot[idx]))
    if density:
        plt.ylabel('Iteration step [%]')
    else:
        plt.ylabel('Iteration step [%]')
    plt.xlabel('Iteration number')

    # set y-limits
    plt.ylim(bottom=0)
    _, cur_ymax = plt.ylim()
    real_ymax = max(cur_ymax, 30)
    plt.ylim(top=real_ymax)

    # set x-limits
    plt.xlim(left=0)
    _, cur_xmax = plt.xlim()
    real_xmax = max(cur_xmax, 3)
    plt.xlim(right=real_xmax)

    plt.xticks(ticks_plot, ticks_labels)
    plt.grid(True)
    fig.canvas.set_window_title('Fracture density optimization, population %d' % popopulation)
    plt.savefig(output_file)
    plt.close(fig)


def fracture_traces_all_outcrops_histograms(out_path, n_pops, pops_obs_all, pops_2d_all, lens_obs_all, lens_2d_all,
                                            length_unit, min_length, n_figure):
    for i in range(n_pops):
        pop_num = i+1
        lens_pop_obs, lens_pop_calc = [], []
        for j, pop in enumerate(pops_obs_all):
            if pop_num == pop:
                lens_pop_obs.append(lens_obs_all[j])
        for j, pop in enumerate(pops_2d_all):
            if pop_num == pop:
                lens_pop_calc.append(lens_2d_all[j])

        np_obs = np.array(lens_pop_obs)
        np_calc = np.array(lens_pop_calc)

        plot_histogram = True
        if np_obs.size > 0:
            _, bin_edges = np.histogram(np_obs, bins='fd')
        elif np_calc.size > 0:
            _, bin_edges = np.histogram(np_calc, bins='fd')
        else:
            plot_histogram = False
            print('Histogram of fracture trace lengths for population %d not saved - there are no fractures.' % (i + 1))

        if plot_histogram:
            bin_edges[0] = min_length*1.0
            x = bin_edges[0:]
            fig = plt.figure(n_figure)
            n_figure += i
            plt.hist([np_obs, np_calc], bins=x, label=['Observations', 'Calculations'], color=['b', 'r'])
            plt.legend(loc='upper right')
            plt.xlim(left=0)
            plt.ylabel('Number of fractures []')
            plt.xlabel('Trace length [%s]' % length_unit)
            fig.canvas.set_window_title('Fracture trace length distribution on all outcrops, population %d' % pop_num)
            plt.savefig(out_path+'/results_populations/fracture_traces_on_outcrops-hist-population_'+str(pop_num)+'.png')
            plt.close(fig)


def transmissivity_all_outcrops_histograms(out_path, n_pops, pops_obs_all, pops_2d_all, transmissivities_obs,
                                           transmissivities_2d_all, n_figure):
    for i in range(n_pops):
        pop_num = i+1
        transmissivities_2d_all_pop = []
        for j, pop in enumerate(pops_2d_all):
            if pop_num == pop:
                transmissivities_2d_all_pop.append(transmissivities_2d_all[j])

        np_calc = np.array(transmissivities_2d_all_pop)
        np_calc_histogram, bin_edges = np.histogram(np_calc, bins=3, density=True)
        np_calc_histogram_normed = (np_calc_histogram*np.diff(bin_edges))*100
        bin_edges_1 = np.array([-0.1, 0.9, 1.9])
        bin_edges_2 = np.array([0.1, 1.1, 2.1])

        fig = plt.figure(n_figure)
        n_figure += i
        plt.bar(bin_edges_1, transmissivities_obs[i], width=0.2, color='b', label='Observations')
        plt.bar(bin_edges_2, np_calc_histogram_normed, width=0.2, color='r', label='Calculations')
        plt.legend(loc='upper right')
        plt.xlim(left=-0.2)
        plt.ylabel('Percentage of fractures [%]')
        plt.xlabel('Transmissivity')
        fig.canvas.set_window_title('Fracture transmissivity distribution on all outcrops, population %d' % pop_num)
        plt.savefig(out_path+'/results_populations/transmissivity-traces-hist-population_'+str(pop_num)+'.png')
        plt.close(fig)


def transmissivities_histograms(out_path, transmissivities_all, n_figure):
    for i in range(len(transmissivities_all)):
        pop_num = i+1

        np_calc = np.array(transmissivities_all[i])
        np_calc_histogram, bin_edges = np.histogram(np_calc, bins=3, density=True)
        np_calc_histogram_normed = (np_calc_histogram*np.diff(bin_edges))*100
        bin_edges = np.array([0.0, 1.0, 2.0])

        fig = plt.figure(n_figure)
        n_figure += i
        plt.bar(bin_edges, np_calc_histogram_normed, width=0.4, color='b', label='Population %d' % pop_num)
        print('\nTRASMISSIVITY OF FRACTURES:')
        print('Population ', pop_num, ': ', np_calc_histogram_normed)
        print(' ')
        plt.legend(loc='upper right')
        plt.xlim(left=-0.2)
        plt.ylabel('Percentage of fractures [%]')
        plt.xlabel('Transmissivity')
        fig.canvas.set_window_title('Fracture transmissivity distribution, population %d' % pop_num)
        plt.savefig(out_path+'/images/simulations/transmissivity-all-fractures-population_'+str(pop_num)+'.png')
        plt.close(fig)


def log_log_plots(out_path, n_pops, pops_obs_all, pops_2d_all, lens_obs_all, lens_2d_all, length_unit, min_length,
                  n_figure):

    jensen_shannon_divergence_all_pops = []
    f_out = open(out_path+'/jensen-shannon_entropy.txt', 'w')
    for i in range(n_pops):
        pop_num = i+1
        lens_pop_obs, lens_pop_calc = [], []
        for j, pop in enumerate(pops_obs_all):
            if pop_num == pop:
                lens_pop_obs.append(lens_obs_all[j])
        for j, pop in enumerate(pops_2d_all):
            if pop_num == pop:
                lens_pop_calc.append(lens_2d_all[j])

        np_obs = np.array(lens_pop_obs)
        np_calc = np.array(lens_pop_calc)

        _, bin_edges = np.histogram(np_obs, bins='fd')
        bin_edges[0] = min_length*1.0
        x = bin_edges[0:]

        y_obs = [0]*len(x)
        for ii in range(len(x)):
            for j in np_obs:
                if j > x[ii]:
                    y_obs[ii] += 1
        y_calc = [0]*len(x)
        for ii in range(len(x)):
            for j in np_calc:
                if j > x[ii]:
                    y_calc[ii] += 1

        jensen_shannon_divergence = jsd(y_obs, y_calc)
        f_out.write('%.4f\n' % jensen_shannon_divergence)
        jensen_shannon_divergence_all_pops.append(jensen_shannon_divergence)

        try:
            y_cdf_obs = create_cdf(lens_pop_obs)
            y_cdf_calc = create_cdf(lens_pop_calc)

            fig = plt.figure(n_figure)
            n_figure += 1
            ax = fig.gca()
            ax.set_xscale('log')
            ax.set_yscale('log')
            plt.plot(lens_pop_obs, y_cdf_obs, 'bo', markersize=2, label='Observations')
            plt.plot(lens_pop_calc, y_cdf_calc, 'ro', markersize=2, label='Calculations')
            plt.legend(loc='upper right')
            plt.xlim(left=0)
            plt.ylabel(r'$P_{\mathrm{trace}} \leq$ trace length $l$')
            plt.xlabel(r'Trace length $l$ [%s]' % length_unit)
            fig.canvas.set_window_title('Fracture trace length distribution on all outcrops, population %d' % pop_num)
            plt.savefig(out_path+'/results_populations/fracture_traces_on_outcrops-loglog-population_'+str(pop_num)+'.png')
            plt.grid(True)
            plt.close(fig)
        except ValueError:
            pass
    f_out.close()
    return jensen_shannon_divergence_all_pops


def fracture_traces_all_outcrops_histograms_and_loglog_more_simulations(out_path, n_pops, pops_obs_all, lens_obs_all,
                                                                        lens_all_pops, strikes_all_pops, dips_all_pops,
                                                                        o_n, o_a, o_w, length_unit, min_length,
                                                                        n_figure):
    n_simulations = len(lens_all_pops[0])
    f_out = open(('%s/jensen-shannon_entropy_mean_of_%d_simulations.txt' % (out_path, n_simulations)), 'w')
    book_jsd = xlwt.Workbook()
    sheet_jsd = book_jsd.add_sheet('JSD values')
    all_fractures_txt = open('%s/_fractures_all_%d_simulations.txt' %(out_path, n_simulations), 'w')

    for i in range(n_pops):
        sheet_jsd.write(0, i+1, 'Population %d' % (i+1))

    for i in range(n_simulations):
        sheet_jsd.write(i+1, 0, 'Simulation %d' % (i+1))

    all_fractures_txt.write('Simulation \tPopulation \tTrace length \tStrike \tDip \tOutcrop name \tOutcrop area \tOutcrop weight\n')

    lens_all_pops_calc = []
    lens_all_pops_obs = []
    for i in range(n_pops):
        pop_num = i+1
        lens_pop_obs = []
        for j, pop in enumerate(pops_obs_all):
            if pop_num == pop:
                lens_pop_obs.append(lens_obs_all[j])
                lens_all_pops_obs.append(lens_obs_all[j])

        np_obs = np.array(lens_pop_obs)
        y_obs_hist, bin_edges = np.histogram(np_obs, bins='fd')
        bin_edges[0] = min_length*1.0
        x = bin_edges[0:]
        try:
            bin_size = bin_edges[2]-bin_edges[1]
        except:
            bin_size = 1

        y_obs = [0]*len(x)
        for ii in range(len(x)):
            for j in np_obs:
                if j > x[ii]:
                    y_obs[ii] += 1

        y_calc = np.zeros((len(x), n_simulations))
        lens_in_pop, strikes_in_pop, dips_in_pop, o_n_pop, o_a_pop, o_w_pop = [], [], [], [], [], []
        for ns in range(n_simulations):
            lens_in_pop.append([])
            strikes_in_pop.append([])
            dips_in_pop.append([])
            o_n_pop.append([])
            o_a_pop.append([])
            o_w_pop.append([])

        lens_in_pop_all = []
        for k in range(n_simulations):
            for ifr, frac in enumerate(lens_all_pops[i][k]):
                lens_in_pop[k].append(frac)
                lens_in_pop_all.append(frac)
                lens_all_pops_calc.append(frac)
                strikes_in_pop[k].append(strikes_all_pops[i][k][ifr])
                dips_in_pop[k].append(dips_all_pops[i][k][ifr])
                o_n_pop[k].append(o_n[i][k][ifr])
                o_a_pop[k].append(o_a[i][k][ifr])
                o_w_pop[k].append(o_w[i][k][ifr])

        for k in range(n_simulations):
            # for ii in range(len(x)):
            for ji, j in enumerate(lens_in_pop[k]):
                all_fractures_txt.write('%d\t%d\t%.4f\t%.4f\t%.4f\t%s\t%.4f\t%.2f\n' % (k, int(i+1), j,
                                                                                      strikes_in_pop[k][ji],
                                                                                      dips_in_pop[k][ji],
                                                                                      o_n_pop[k][ji],
                                                                                      o_a_pop[k][ji],
                                                                                      o_w_pop[k][ji]))
        y_calc_hist = np.zeros((n_simulations, len(x)-1))
        for k in range(n_simulations):
            y_calc_hist_sim, _ = np.histogram(lens_in_pop[k], bins=bin_edges)
            y_calc_hist[k, :] = y_calc_hist_sim
            jensen_shannon_divergence = jsd(y_obs, y_calc[:, k])
            sheet_jsd.write(int(k+1), int(i+1), jensen_shannon_divergence)

        y_calc_mean = np.zeros(len(x))
        y_calc_min = np.zeros(len(x))
        y_calc_max = np.zeros(len(x))
        for ii in range(len(x)):
            y_calc_mean[ii] = np.mean(y_calc[ii])
            y_calc_min[ii] = np.min(y_calc[ii])
            y_calc_max[ii] = np.max(y_calc[ii])

        y_calc_hist_mean = np.mean(y_calc_hist, axis=0)
        # y_calc_hist_min = np.min(y_calc_hist, axis=0)
        # y_calc_hist_max = np.max(y_calc_hist, axis=0)

        jensen_shannon_divergence = jsd(y_obs, y_calc_mean)
        f_out.write('%.4f\n' % jensen_shannon_divergence)

        # histogram
        fig = plt.figure(n_figure)
        n_figure += i
        ax = plt.subplot(111)
        x_bar = x[1:]
        ax.bar(x_bar-bin_size/8, y_obs_hist, width=bin_size/4, color='b', align='center', label='Observations')
        ax.bar(x_bar+bin_size/8, y_calc_hist_mean, width=bin_size/4, color='r', align='center', label='Simulations')
        plt.legend(loc='upper right')
        plt.xlim(left=0)

        plt.ylabel('Mean number of fractures [-]')
        plt.xlabel('Trace length [%s]' % length_unit)
        fig.canvas.set_window_title('Fracture trace length distribution on all outcrops, population %d' % pop_num)
        plt.savefig(out_path+'/results_populations/_fracture_traces_on_outcrops-hist-population_'+str(pop_num)+
                    '-mean_from_'+str(n_simulations)+'_simulations.png')
        plt.close(fig)

        y_cdf_obs = create_cdf(lens_pop_obs)
        y_cdf_calc = create_cdf(lens_in_pop_all)

        # loglog plot
        fig = plt.figure(n_figure)
        n_figure += 1
        ax = fig.gca()
        ax.set_xscale('log')
        ax.set_yscale('log')
        plt.plot(lens_pop_obs, y_cdf_obs, 'bo', markersize=2, label='Observations')
        plt.plot(lens_in_pop_all, y_cdf_calc, 'ro', markersize=2, label='Calculations')
        plt.legend(loc='upper right')
        plt.xlim(left=0)
        plt.ylabel(r'$P_{\mathrm{trace}} \leq$ trace length $l$')
        plt.xlabel(r'Trace length $l$ [%s]' % length_unit)
        fig.canvas.set_window_title('Fracture trace length distribution on all outcrops, population %d' % pop_num)
        # ax.fill_between(x_bar, y_calc_hist_min, y_calc_hist_max, facecolor='red', alpha=0.5)
        plt.xlim(left=0)
        fig.canvas.set_window_title('Fracture trace length distribution on all outcrops, population %d' % pop_num)
        plt.savefig(out_path+'/results_populations/_fracture_traces_on_outcrops-loglog-population_'+str(pop_num)+
                    '-mean_from_'+str(n_simulations)+'_simulations.png')
        plt.grid(True)
        plt.close(fig)

    f_out.close()
    all_fractures_txt.close()
    book_jsd.save('%s/results_populations/_jsd_all_%d_simulations.xls' %(out_path, n_simulations))

    # put all fractures from all populations together
    np_obs = np.array(lens_obs_all)
    y_obs_hist, bin_edges = np.histogram(np_obs, bins='fd')
    bin_edges[0] = min_length*1.0
    x = bin_edges[1:]
    try:
        bin_size = bin_edges[2]-bin_edges[1]
    except:
        bin_size = 1

    y_obs = [0]*len(x)
    for ii in range(len(x)):
        for j in np_obs:
            if j > x[ii]:
                y_obs[ii] += 1

    y_calc = np.zeros((len(x), n_simulations))
    lens_in_pop = []
    for ns in range(n_simulations):
        lens_in_pop.append([])
    for k in range(n_simulations):
        for i in range(n_pops):
            for frac in lens_all_pops[i][k]:
                lens_in_pop[k].append(frac)

    for k in range(n_simulations):
        for ii in range(len(x)):
            for j in lens_in_pop[k]:
                if j > x[ii]:
                    y_calc[ii][k] += 1

    y_calc_hist = np.zeros((n_simulations, len(x)))
    for k in range(n_simulations):
        y_calc_hist_sim, _ = np.histogram(lens_in_pop[k], bins=bin_edges)
        y_calc_hist[k, :] = y_calc_hist_sim

    y_calc_mean = np.zeros(len(x))
    y_calc_min = np.zeros(len(x))
    y_calc_max = np.zeros(len(x))
    for ii in range(len(x)):
        y_calc_mean[ii] = np.mean(y_calc[ii])
        y_calc_min[ii] = np.min(y_calc[ii])
        y_calc_max[ii] = np.max(y_calc[ii])

    y_calc_hist_mean = np.mean(y_calc_hist, axis=0)
    y_calc_hist_min = np.min(y_calc_hist, axis=0)
    y_calc_hist_max = np.max(y_calc_hist, axis=0)

    # histogram
    fig = plt.figure(n_figure)
    n_figure += 1
    ax = plt.subplot(111)
    # x = x[1:]
    ax.bar(x-bin_size/8, y_obs_hist, width=bin_size/4, color='b', align='center', label='Observations')
    ax.bar(x+bin_size/8, y_calc_hist_mean, width=bin_size/4, color='r', align='center', label='Simulations')
    plt.legend(loc='upper right')
    plt.xlim(left=0)

    plt.ylabel('Mean number of fractures [-]')
    plt.xlabel('Trace length [%s]' % length_unit)
    fig.canvas.set_window_title('Fracture trace length distribution on all outcrops, all populations')
    plt.savefig(out_path+'/results_populations/_fracture_traces_on_outcrops-hist-all_populations-mean_from_'
                +str(n_simulations)+'_simulations.png')
    plt.close(fig)

    y_cdf_obs_all = create_cdf(lens_all_pops_obs)
    y_cdf_calc_all = create_cdf(lens_all_pops_calc)

    # loglog plot
    fig = plt.figure(n_figure)
    n_figure += 1
    ax = fig.gca()
    ax.set_xscale('log')
    ax.set_yscale('log')
    plt.plot(lens_all_pops_obs, y_cdf_obs_all, 'bo', markersize=2, label='Observations')
    plt.plot(lens_all_pops_calc, y_cdf_calc_all, 'ro', markersize=2, label='Calculations')
    plt.legend(loc='upper right')
    plt.xlim(left=0)
    plt.ylabel(r'$P_{\mathrm{trace}} \leq$ trace length $l$')
    plt.xlabel(r'Trace length $l$ [%s]' % length_unit)
    fig.canvas.set_window_title('Fracture trace length distribution on all outcrops, all populations')
    plt.savefig(out_path+'/results_populations/_fracture_traces_on_outcrops-loglog-all_populations-mean_from_'
                +str(n_simulations)+'_simulations.png')
    plt.grid(True)
    plt.close(fig)
