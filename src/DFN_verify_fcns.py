#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Functions for DFN_verify.py
- This program was constructed by merging tracelen_v01.py and Fisher_params_v03.py
- This file keeps all function for DFN_verify.py

2018.04.05
 - print tables of
   average length, number, and densities of traces on outcrops
"""

from math import *
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import mpl_toolkits.mplot3d as mpl3
from output_file import print_html_report


# Auxilary finctions
# Aux function for constructing list of cells
def iter_rows(wsx):
    for row in wsx.iter_rows():
        yield [cell.value for cell in row]


# Auxilary function for sort
def getKey3(item):
    return item[3]


def getKey0(item):
    return item[0]


def initialize_global_vars():
    global tabcounter, figcounter, seccounter
    tabcounter = 0
    figcounter = 0
    seccounter = 0


def get_figcounter():
    global figcounter
    return figcounter - 1

#
# Functions for outcrop geometry calculations
#

def OCarea_by_tri(verts):
    """
    Returns area and unit normal of a spatial polygon. VErtices must be
    aranged counter-clockwise. Area is calculated by triangulation from
    geometric center. Unit normal is calculated as mean
    of normals to the triangles.
    Vertices (verts) are in the form of array([[x0,y0,z0],[x1,y1,z1],...]
    """

    # Geometric center
    cent = np.mean(verts, axis=0)
    # Calculate area of polygon and mean normal by triangulation from center
    A = 0
    nvec = np.array([0, 0, 0])
    for i in range(len(verts)):
        if i == 0:
            v1 = verts[len(verts) - 1] - cent
        else:
            v1 = verts[i - 1] - cent
        v2 = verts[i] - cent
        v3 = np.cross(v1, v2)
        A += 0.5 * np.linalg.norm(v3)
        nvec = nvec + v3
    nvec = nvec / np.linalg.norm(nvec)
    return (A, nvec)


def OCplane(verts):
    """
    Finds a plane as the least-sqare fit to a spatial polygon
    (ref. http://www.ilikebigbits.com/blog/2017/9/24/fitting-a-plane-to-noisy-points-in-3d)
    Then projects the vertices to this plane (note 3.12.2017).
    Vertices (verts) are in the form of array([[x0,y0,z0],[x1,y1,z1],...].
    Returns the vertices of the projected polygon, area, normal and sum of the
    norms of the projection vectors (gives idea how much the original polygon
    deviated from plane).
    """

    # Geometric center
    cent = np.mean(verts, axis=0)
    # Covariance matrix
    covm = np.cov(verts - cent, rowvar=False)
    # Eigenvalues and eigenvectors of covariance matrix
    eigen = np.linalg.eigh(covm)
    # The eigenvector correstponding to the least eigenvalue is the sought plane normal
    nvec = eigen[1][:, 0]
    # Now project the original vertices to this plane and calculate the total length of the projection vector.
    pverts = []
    pnsum = 0
    for ivert, vert in enumerate(verts):
        p = -np.dot(vert - cent, nvec)
        pvec = p * nvec
        pvert = vert + pvec
        pverts.append(pvert)
        pnsum += abs(p)
    # Calculate the area of the projected polygon
    parea, _ = OCarea_by_tri(pverts)
    # Return
    return (pverts, parea, nvec, pnsum)


# Functions related to Fisher statistics and fracture orientation (from Fisher_params_v03.py)
def sa_to_da(s):
    "Calculate dip azimuth (směr sklonu) from strike azimuth (směr plochy) in deg."
    da = s + 90
    if da >= 360:
        da = da - 360
    return (da)


def sd_to_vec(s, d):
    "Transforms strike and dip to pole vector (on lower hemispehere)."
    nx = -cos(radians(s)) * sin(radians(d))
    ny = sin(radians(s)) * sin(radians(d))
    nz = -cos(radians(d))
    return (np.array([nx, ny, nz]))


def vec_to_tp(vec):
    "Transforms pole vector (on lower hemispehere) to trend and plunge."
    ex = vec[0]
    ey = vec[1]
    ez = vec[2]
    if ey > 0:
        trend = degrees(atan(ex / ey))
    else:
        if ey < 0:
            trend = degrees(atan(ex / ey)) + 180
        else:
            trend = "NaN"
    plunge = -degrees(asin(ez))
    return ([trend, plunge])


def vec_to_sd(vec):
    "Transforms pole vector (on lower hemispehere) to strike and dip."
    ex = vec[0]
    ey = vec[1]
    ez = vec[2]
    if ey > 0:
        strike = degrees(atan(ex / ey)) + 90
    else:
        if ey < 0:
            strike = degrees(atan(ex / ey)) + 270
        else:
            strike = "NaN"
    dip = degrees(acos(-ez))  # Error in Waldron ??
    return ([strike, dip])


def vecs_angle(v1, v2):
    "Calculate angle between vectors in deg. <0,+180>"
    dp = np.dot(v1, v2)
    v1n = np.linalg.norm(v1)
    v2n = np.linalg.norm(v2)
    arg = dp / (v1n * v2n)
    #    arg=abs(dp/(v1n*v2n)) # acute angle
    if arg > 1.0:
        arg = 1.0
    if arg < -1.0:
        arg = -1.0
    angl = degrees(acos(arg))
    return (angl)


def cal_kappa(N, R):
    "Estimate kappa of Fisher distribution."
    # Ref. Stereonet manual.
    if N == R:
        #        return("Infinity")
        return (1.e8)
    if N < 16:
        kappa = (N / (N - R)) * np.power((1 - (1 / N)), 2)
    else:
        kappa = (N - 1) / (N - R)
    return (kappa)


def Admrb(kappa, N, R):
    return (sp.special.iv(3 / 2, kappa) / sp.special.iv(3 / 2 - 1, kappa) - R / N)


def cal_kappa_tr(N, R):
    "Find kappa of Fisher distribution by solving transcendental eq."
    # Ref. Fisher (1993), Mardia and Jupp (1999) or Dhillon and Sra (2003)
    try:
        kappa = sp.optimize.newton(Admrb, 0.01, args=(N, R,))
    except:
        kappa = cal_kappa(N, R)
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("Warning: solution of transcendental equation for kappa failed, using simplified formula instead.")
        print("N=", N, "R=", R, "kappa=", kappa)
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        pass
    return (kappa)


def cal_Terzaghi(fstrike, fdip, ocnvec, tcmax):
    # Calculate Terzaghi correction factor. tcmax ... max. value of correction factor, e.g 10 (Priest 1993)
    fnvec = sd_to_vec(fstrike, fdip)
    crvec = np.cross(fnvec, ocnvec)
    cr = np.linalg.norm(crvec)
    fn = np.linalg.norm(fnvec)
    on = np.linalg.norm(ocnvec)
    sinbeta = abs(cr / (fn * on))
    tcfact = min([1.0 / sinbeta, tcmax])
    return (tcfact)


def cal_Fisher(dslist, Pr, transcend):
    """
    Reorient unit normal vectors of fractures and calculate Fisher statistics.
    The reorientation is determined by the principal plane, whose normal is found
    as the max. eigenvector of covariance matrix of the fractures' normals. See
    derivation, notes 6.10.2017.
    The fractures' normal vectors are weighed by weights given as the 3rd
    element in dslist.
    The weighting is normalized according to Priest (2012).
    This weighting can be used for the Terzaghi correction.
    Calculate confidence cone angle (in deg) for given Pr.
    If "transcend" is True, then kappa is solved from transcendental equation
    in addition to simplified formula.
    """

    # Calculate principal direction
    # calculate A-matrix
    nvecs = []
    Amat = np.zeros([3, 3])
    for ids, ds in enumerate(dslist):
        d = ds[0]
        s = ds[1]
        #  Vector components of pole (on lower hemisphere)
        nvec = sd_to_vec(s, d)
        nvecs.append(nvec)
        Amat = Amat + np.outer(np.matrix(nvec), np.matrix(nvec).T)
    # calculate eigen values and eigenvectors
    eigenA = np.linalg.eigh(Amat)
    # select the eigenvector corresonding to the maximum eigenvalue which corresponds to the principal direction
    imax = np.argmax(eigenA[0])
    evec = eigenA[1][:, imax]
    # reverse the vector if it has upward orientation
    if evec[2] > 0:
        evec = np.negative(evec)

    # calculate the resultant vector R and unit r with reorientation apply Terzaghi correction according to Priest
    # (2012, eq. 3.13)
    Rvec = np.array([0, 0, 0])
    Nw = sum(item[2] for item in dslist)
    N = len(dslist)
    for ivec, nvec in enumerate(nvecs):
        # Terzaghi correction factor
        tcf = dslist[ivec][2]
        # Change orientation of all n-vectors according to e-vector
        if np.inner(evec, nvec) < 0:
            nvec = np.negative(nvec)
            nvecs[ivec] = nvec
        # Add n to R, multiply by Terzaghi correction factor
        Rvec = Rvec + nvec * tcf * N / Nw
    # |R|
    Rnorm = np.linalg.norm(Rvec)
    # Unit vector r=R/|R|
    rvec = Rvec / Rnorm

    # Estimate of kappa
    kappa = cal_kappa(N, Rnorm)
    if transcend:
        kappa_tr = cal_kappa_tr(N, Rnorm)
    else:
        kappa_tr = None
    #       kappa_tr=kappa

    # Estimate of cone angle for zone of confidence "Pr" (Fisher 1993, Eq. 5.34)
    if N > 1:
        arg = 1.0 - (N - Rnorm) / Rnorm * (np.power(1.0 / (1.0 - Pr), 1.0 / (N - 1.0)) - 1.0)
        cang = degrees(acos(arg))
    else:
        cang = 0.0

    #    print(N,Rnorm,cang)
    return (evec, Rvec, Rnorm, rvec, kappa, kappa_tr, cang)


#     - principal direction vector
#     - mean direction vector
#     - kappa (by simple formula and Bessel fcn.)
#     - cone angle for zone of confidence "Pr"


def densities_vs_OCarea(pop, obstraces, simtraces, OClist, OCareas, SIMcount, out_path, report_path):
    # Analyze relation of P20, P21, N and lav vs. outcrop area, pop ... population # or 'all'
    lav_obs_list = []
    ltot_obs_list = []
    N_obs_list = []
    lav_sim_list = []
    ltot_sim_list = []
    N_sim_list = []
    for iocr, ocr in enumerate(OClist):
        if pop != 'all':
            # list of traces for pop and ocr
            t_obs = [t.length for t in obstraces if t.outcrop == ocr and t.pop == pop]
            # list of traces for ocr, all pops and sims
            t_sim = [t.length for t in simtraces if t.outcrop == iocr and t.pop == pop]
        else:
            t_obs = [t.length for t in obstraces if t.outcrop == ocr]  # list of traces for pop and ocr
            t_sim = [t.length for t in simtraces if t.outcrop == iocr]  # list of traces for ocr, all pops and sims
        N_obs = len(t_obs)  # number of traces - observation
        N_sim = len(t_sim) / SIMcount  # number of traces - average of all simulations
        ltot_obs = np.sum(t_obs)
        ltot_sim = np.sum(t_sim) / SIMcount  # total length of traces - average of all simulations
        if N_sim != 0:
            lav_sim = np.mean(t_sim)
        else:  # if there is no trace on ocr
            lav_sim = 0.0
        if N_obs != 0:
            lav_obs = np.mean(t_obs)
        else:
            lav_obs = 0.0
        lav_obs_list.append([OCareas[iocr], lav_obs])
        ltot_obs_list.append([OCareas[iocr], ltot_obs])
        N_obs_list.append([OCareas[iocr], N_obs])
        lav_sim_list.append([OCareas[iocr], lav_sim])
        ltot_sim_list.append([OCareas[iocr], ltot_sim])
        N_sim_list.append([OCareas[iocr], N_sim])

    # Print tables
    print()
    tab_label("Average trace length and fracture intensity P21 on outcrops" \
              + " - observation and mean of " \
              + str(SIMcount) + " DFraM simulations. (See Table 2 for outcrop names.)")
    template0 = '{0:^7} | {1:^9}  | {2:^9} | {3:^9} | {4:^9} | {5:^9}'
    template1 = '{0:^7}   {1:9.2f}   {2:9.2f}   {3:9.2f}   {4:9.3f}   {5:9.3f}'
    print(template0.format("Outcrop", "Area", "l_av(obs)", "l_av(sim)", "P_21(obs)", "P_21(sim)"))
    print(template0.format("", "[m^2]", "[m]", "[m]", "[m^-1]", "[m^-1]"))
    for iocr, ocr in enumerate(OClist):
        print(template1.format \
                  (ocr, OCareas[iocr], lav_obs_list[iocr][1], lav_sim_list[iocr][1], \
                   ltot_obs_list[iocr][1] / ltot_obs_list[iocr][0], \
                   ltot_sim_list[iocr][1] / ltot_sim_list[iocr][0]))

    print()
    tab_label("Number of traces and fracture density P20 on outcrops" \
              + " - observation and mean of " \
              + str(SIMcount) + " DFraM simulations. (See Table 2 for outcrop names.)")
    template0 = '{0:^7} | {1:^9}  | {2:^9} | {3:^9} | {4:^9} | {5:^9}'
    template1 = '{0:^7}   {1:9.2f}   {2:9.0f}   {3:9.1f}   {4:9.3f}   {5:9.3f}'
    print(template0.format("Outcrop", "Area", "N(obs)", "N(sim)", "P_20(obs)", "P_20(sim)"))
    print(template0.format("", "[m^2]", "[-]", "[-]", "[m^-2]", "[m^-2]"))
    for iocr, ocr in enumerate(OClist):
        print(template1.format \
                  (ocr, OCareas[iocr], N_obs_list[iocr][1], N_sim_list[iocr][1], \
                   N_obs_list[iocr][1] / N_obs_list[iocr][0], \
                   N_sim_list[iocr][1] / N_sim_list[iocr][0]))

    # Sort lists
    lav_obs_list.sort(key=getKey0)
    ltot_obs_list.sort(key=getKey0)
    N_obs_list.sort(key=getKey0)
    lav_sim_list.sort(key=getKey0)
    ltot_sim_list.sort(key=getKey0)
    N_sim_list.sort(key=getKey0)
    # Transform to arrays and drop outcrops with no traces
    A_obs_ar = np.asarray([l[0] for l in lav_obs_list if l[1] != 0])
    A_sim_ar = np.asarray([l[0] for l in lav_sim_list if l[1] != 0])
    lav_obs_ar = np.asarray([l[1] for l in lav_obs_list if l[1] != 0])
    lav_sim_ar = np.asarray([l[1] for l in lav_sim_list if l[1] != 0])
    ltot_sim_ar = np.asarray([l[1] for l in ltot_sim_list if l[1] != 0])
    ltot_obs_ar = np.asarray([l[1] for l in ltot_obs_list if l[1] != 0])
    N_obs_ar = np.asarray([l[1] for l in N_obs_list if l[1] != 0])
    N_sim_ar = np.asarray([l[1] for l in N_sim_list if l[1] != 0])

    # Print graphs
    img_name = graph_xy_01([[A_obs_ar, lav_obs_ar], [A_sim_ar, lav_sim_ar]],
                           ['Observation', 'Simulation'], r'OC area [m$^2$]', r'$l_{av}$ [m]',
                           'Population ' + str(pop), out_path, 'outcrop_area_mean_vs_trace_length')
    fig_label("Average trace length vs. outcrop areas - observation and mean of "+str(SIMcount)+" DFraM simulations")
    cur_img = '            <img src="%s" alt="Average trace length vs. outcrop areas" width="600">' % img_name
    print_html_report(report_path, '_IMAGE TRACE LENGTH VS OUTCROP_', cur_img, keep_keyword=True)
    print_html_report(report_path, '_IMAGE TRACE LENGTH VS OUTCROP_', ' ', keep_keyword=False)

    img_name = graph_xy_01([[A_obs_ar, ltot_obs_ar / A_obs_ar], [A_sim_ar, ltot_sim_ar / A_sim_ar]],
                           ['Observation', 'Simulation'], r'OC area [m$^2$]', r'$P21$ [m$^{-1}$]',
                           'Population ' + str(pop), out_path, 'outcrop_area_vs_P21')
    fig_label("Fracture density P21 vs. outcrop areas - observation and mean of "+str(SIMcount)+" DFraM simulations")
    cur_img = '            <img src="%s" alt="Fracture density P21 vs. outcrop areas" width="600">' % img_name
    print_html_report(report_path, '_IMAGE P21 VS OUTCROP_', cur_img, keep_keyword=True)
    print_html_report(report_path, '_IMAGE P21 VS OUTCROP_', ' ', keep_keyword=False)

    img_name = graph_xy_01([[A_obs_ar, N_obs_ar], [A_sim_ar, N_sim_ar]],
                           ['Observation', 'Simulation'], r'OC area [m$^2$]', r'$N$ [-]',
                           'Population ' + str(pop), out_path, 'outcrop_area_vs_number_of_traces')
    fig_label("Number of traces vs. outcrop areas - observation and mean of "+str(SIMcount)+" DFraM simulations")
    cur_img = '            <img src="%s" alt="Number of traces vs. outcrop areas" width="600">' % img_name
    print_html_report(report_path, '_IMAGE N TRACES VS OUTCROP_', cur_img, keep_keyword=True)
    print_html_report(report_path, '_IMAGE N TRACES VS OUTCROP_', ' ', keep_keyword=False)

    img_name = graph_xy_01([[A_obs_ar, N_obs_ar / A_obs_ar], [A_sim_ar, N_sim_ar / A_sim_ar]],
                           ['Observation', 'Simulation'], r'OC area [m$^2$]', r'$P20$ [m$^{-2}$]',
                           'Population ' + str(pop), out_path, 'outcrop_area_vs_P20')
    fig_label("Fracture density P20 vs. outcrop areas - observation and mean of "+str(SIMcount)+" DFraM simulations")
    cur_img = '            <img src="%s" alt="Fracture density P20 vs. outcrop areas" width="600">' % img_name
    print_html_report(report_path, '_IMAGE P20 VS OUTCROP_', cur_img, keep_keyword=True)
    print_html_report(report_path, '_IMAGE P20 VS OUTCROP_', ' ', keep_keyword=False)


#
# Plotting
#
def graph_xy_01(data, labs, xlab, ylab, tit, path, name):
    plt.figure(figsize=(4.5, 3), dpi=300)
    ax = plt.gca()
    plt.title(tit)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    for i in range(len(data)):
        plt.plot(data[i][0], data[i][1], marker='o', label=labs[i])
    ax.legend()
    global figcounter
    n_fig = '%02d_' % (figcounter+1)
    plt.tight_layout()
    plt.savefig(path + '/images/' + n_fig + name + '.png')
    img_name_for_report = 'images/' + n_fig + name + '.png'
    return img_name_for_report


def graph_loglog_01(data, labs, xlab, ylab, tit, path):
    plt.figure(figsize=(4.5, 3), dpi=300)
    ax = plt.gca()
    plt.title(tit)
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    for i in range(len(data)):
        plt.loglog(data[i][0], data[i][1], marker='o', label=labs[i])
    ax.legend()
    global figcounter
    n_fig = '%02d_' % (figcounter + 1)
    plt.tight_layout()
    plt.savefig(path + '/images/' + n_fig + tit + '.png')
    img_name_for_report = 'images/' + n_fig + tit + '.png'
    return img_name_for_report


def plot3d_OC(vtx, pvtx, tit, path):
    xmin = min([v[0] for v in vtx])
    xmax = max([v[0] for v in vtx])
    ymin = min([v[1] for v in vtx])
    ymax = max([v[1] for v in vtx])
    zmin = min([v[2] for v in vtx])
    zmax = max([v[2] for v in vtx])
    rng = 0.5 * max([xmax - xmin, ymax - ymin, zmax - zmin])
    xmid = (xmax + xmin) / 2.0
    ymid = (ymax + ymin) / 2.0
    zmid = (zmax + zmin) / 2.0

    # ax = mpl3.Axes3D(plt.figure(figsize=(3, 3), dpi=300))
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_title(tit, loc='left', size='x-large')
    ax.set_aspect('equal')
    ax.set_zlim3d(zmid - rng, zmid + rng)
    ax.set_ylim3d(ymid - rng, ymid + rng)
    ax.set_xlim3d(xmid - rng, xmid + rng)
    ax.set_xlabel("\n\nX (East) [m]")
    ax.set_ylabel("\n\nY (North) [m]")
    ax.set_zlabel("\nZ [m]")
    xx = [v[0] for v in vtx]
    yy = [v[1] for v in vtx]
    zz = [v[2] for v in vtx]
    ax.scatter(xx, yy, zz, color='b', marker='o', s=70)
    xx = [v[0] for v in pvtx]
    yy = [v[1] for v in pvtx]
    zz = [v[2] for v in pvtx]
    ax.scatter(xx, yy, zz, color='r', marker='o', s=70)
    polyg1 = mpl3.art3d.Poly3DCollection([vtx], alpha=0.5)
    polyg1.set_color(colors.rgb2hex([.5, .5, 1]))
    polyg1.set_edgecolor('k')
    polyg2 = mpl3.art3d.Poly3DCollection([pvtx], alpha=0.5)
    polyg2.set_color(colors.rgb2hex([1, .5, .5]))
    polyg2.set_edgecolor('k')
    ax.add_collection3d(polyg1)
    ax.add_collection3d(polyg2)
    #    Poly3DCollection(poly3d, linewidths=1, alpha=0.2)
    global figcounter
    n_fig = '%02d_' % (figcounter + 1)
    plt.savefig(path + '/images/' + n_fig + tit + '.png')
    img_name_for_report = 'images/' + n_fig + tit + '.png'
    return img_name_for_report


#
# Labels
#
def fig_label(label):
    """Prints label for figure with automatically increased number.
    Label must be a string. figcounter is increased by 1."""
    global figcounter
    figcounter = figcounter + 1
    print("Figure", str(figcounter) + ":", label)


def tab_label(label):
    """Prints label for table with automatically increased number.
    Label must be a string. tabcounter is increased by 1."""
    global tabcounter
    tabcounter = tabcounter + 1
    print("Table", str(tabcounter) + ":", label)


def sec_heading(label):
    """Prints section header with automatically increased number.
    Label must be a string. seccounter is increased by 1."""
    global seccounter
    seccounter = seccounter + 1
    print("\n*********************************************************************************")
    print("  ", str(seccounter) + ".", label)
    print("*********************************************************************************\n")
