#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
import time
import os
from math import floor


def tic():
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()


def toc():
    if 'startTime_for_tictoc' in globals():
        time_seconds = time.time() - startTime_for_tictoc
        time_h = floor(time_seconds/3600)
        time_m = floor((time_seconds % 3600)/60)
        time_s = time_seconds % 60
        the_time = 'Elapsed time: %d:%02d:%05.2f' % (time_h, time_m, time_s)
        print(the_time)
    else:
        print("Toc: start time not set")


def get_start_time():
    global global_time_counter
    global_time_counter = time.time()


def get_duration():
    if 'global_time_counter' in globals():
        time_seconds = time.time()-global_time_counter
        time_h = floor(time_seconds/3600)
        time_m = floor((time_seconds % 3600)/60)
        time_s = round(time_seconds % 60)
        the_time = '%d:%02d:%02d' % (time_h, time_m, time_s)
        return the_time
    else:
        print("Start time not set")


def create_time_folder(project_name, optimize_dfn):
    temp_dir = os.getcwd()
    os.chdir('../data/output')
    timestr = time.strftime('-%Y_%m_%d-%H_%M_%S')
    time_of_start = time.strftime('%H:%M:%S, %d %b %Y')
    if optimize_dfn:
        new_folder_name = project_name + '-DFN_optimization' + timestr
    else:
        new_folder_name = project_name + '-DFN_generation' + timestr
    os.makedirs(new_folder_name, 0o777)
    os.chdir(new_folder_name)
    os.makedirs('vtk', 0o777)
    os.makedirs('images', 0o777)
    os.chdir(temp_dir)

    path_to_project_outputs = '../data/output/' + new_folder_name
    return time_of_start, path_to_project_outputs


def create_time_folder_verify(project_name):
    temp_dir = os.getcwd()
    os.chdir('../data/output')
    timestr = time.strftime('-%Y_%m_%d-%H_%M_%S')
    time_of_start = time.strftime('%H:%M:%S, %d %b %Y')
    new_folder_name = project_name + timestr
    os.makedirs(new_folder_name, 0o777)
    os.chdir(new_folder_name)
    os.makedirs('images', 0o777)
    os.chdir(temp_dir)
    path_to_project_outputs = '../data/output/' + new_folder_name
    report_path = path_to_project_outputs + '/report_' + project_name + '.html'

    try:
        os.popen('cp report_template.html %s' % report_path)  # linux
    except:
        os.popen('copy report_template.html %s' % report_path)  # windows

    return time_of_start, path_to_project_outputs, report_path
