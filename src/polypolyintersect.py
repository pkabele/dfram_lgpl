#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
from matplotlib import path
import mathutils
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
from numpy.linalg import norm
import random
from segpolyintersect import *
import matplotlib.pyplot as plt

def poly_area_2d(x, y):
    return 0.5*np.abs(np.dot(x, np.roll(y, 1))-np.dot(y, np.roll(x, 1)))


def sort_points_3d(points):
    n_points = len(points)

    # find centroid
    x, y = np.zeros(n_points), np.zeros(n_points)
    for idx, point in enumerate(points):
        x[idx] = point[0]
        y[idx] = point[1]

    # find polar angle
    theta = np.arctan2(y-np.ones(n_points)*np.sum(y)/n_points, x-np.ones(n_points)*np.sum(x)/n_points)

    # sort points by theta
    order = [i[0] for i in sorted(enumerate(theta), key=lambda th:th[1])]

    # save the ordered points
    ordered_points = []
    for idx in order:
        ordered_points.append((points[idx][0], points[idx][1], points[idx][2]))
    return ordered_points

'''
def rotate_in_plane_points_3d_to_2d(points):
    for idx, coords in enumerate(points):
        for index, item in enumerate(coords):
            points[idx][index] = float(item)

    loc0 = np.array(points[0])  # local origin
    locx = np.array(points[1])-loc0  # local X axis
    nx = norm(locx)
    normal = np.cross(locx, np.array(points[2])-loc0)  # vector orthogonal to polygon plane
    locy = np.cross(normal, locx)  # local Y axis
    ny = norm(locy)
    if nx == 0 or ny == 0:
        locx = np.array(points[2])-loc0  # local X axis
        nx = norm(locx)
        if len(points) > 3:
            normal = np.cross(locx, np.array(points[3])-loc0)  # vector orthogonal to polygon plane
        else:
            locx = np.array(points[0])-loc0
            nx = norm(locx)
            normal = np.cross(locx, np.array(points[1])-loc0)
        locy = np.cross(normal, locx)  # local Y axis
        ny = norm(locy)

    # normalize the computed local axes
    locx /= max(1e-5,nx)
    locy /= max(1e-5,ny)

    local_coords = [(np.dot(p-loc0, locx),  # local X coordinate
                     np.dot(p-loc0, locy))  # local Y coordinate
                    for p in points]

    return local_coords, loc0, locx, locy
'''


def rotate_in_plane_points_3d_to_2d(points):
    for idx, coords in enumerate(points):
        for index, item in enumerate(coords):
            points[idx][index] = float(item)

    loc0 = np.array(points[0])  # local origin
    locx = np.array(points[1])-loc0  # local X axis
    nx = norm(locx)
    normal = np.cross(locx, np.array(points[2])-loc0)  # vector orthogonal to polygon plane
    locy = np.cross(normal, locx)  # local Y axis
    ny = norm(locy)

    # normalize the computed local axes
    locx /= nx
    locy /= ny

    local_coords = [(np.dot(p-loc0, locx),  # local X coordinate
                     np.dot(p-loc0, locy))  # local Y coordinate
                    for p in points]

    return local_coords, loc0, locx, locy


def area_by_tri(verts):
    """
    Returns area and unit normal of a spatial polygon. VErtices must be
    aranged counter-clockwise. Area is calculated by triangulation from
    geometric center. Unit normal is calculated as mean
    of normals to the triangles.
    Vertices (verts) are in the form of array([[x0,y0,z0],[x1,y1,z1],...]
    """
    cent=np.mean(verts, axis=0)
    area = 0
    nvec = np.array([0,0,0])
    for i in range(len(verts)):
        if i == 0:
            v1 = verts[len(verts)-1]-cent
        else:
            v1 = verts[i-1]-cent
        v2 = verts[i]-cent
        v3 = np.cross(v1, v2)
        area += 0.5*np.linalg.norm(v3)
        nvec = nvec+v3
    nvec = nvec/np.linalg.norm(nvec)
    return area, nvec


def poly_area_3d(poly):
    '''
    [local_coords, loc0, locx, locy] = rotate_in_plane_points_3d_to_2d(poly)
    coords_2d = np.array(local_coords)
    a1 = 0.5*np.abs(np.dot(coords_2d[:, 0], np.roll(coords_2d[:, 1], 1)) -np.dot(coords_2d[:, 1], np.roll(coords_2d[:, 0], 1)))
    '''
    a = area_by_tri(poly)

    return a[0]


def line_line_intersection(a1, a2, b1, b2):
    x1 = a1[0]
    y1 = a1[1]
    x2 = a2[0]
    y2 = a2[1]
    x3 = b1[0]
    y3 = b1[1]
    x4 = b2[0]
    y4 = b2[1]

    if ((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)) == 0:
        print('zero length of a line in calculation of fracture polygon-polygon intersections')
        px = x1
        py = y1
        pz = a1[2]
    else:
        px = ((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
        py = ((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))

        l_a = np.sqrt((a2[0]-a1[0])**2+(a2[1]-a1[1])**2)  # length of segment a in xy plane
        l_a1p = np.sqrt((px-a1[0])**2+(py-a1[1])**2)
        delta_z_a = a2[2]-a1[2]
        pz = delta_z_a/l_a*l_a1p+a1[2]

    p = [px, py, pz]

    return p


def poly_poly_intersections(polygon, polygons, min_length):

    x1 = polygon['x']
    y1 = polygon['y']
    z1 = polygon['z']
    n_points_poly_1 = len(x1)  # polygon 1 = outcrop (plane p)

    # find cartesian box surrounding the polygon
    x1_min = min(x1)
    x1_max = max(x1)
    y1_min = min(y1)
    y1_max = max(y1)
    z1_min = min(z1)
    z1_max = max(z1)

    polygon_1 = [None]*n_points_poly_1
    poly_1_list = [None]*n_points_poly_1
    for i in range(0, n_points_poly_1):
        polygon_1[i] = (x1[i], y1[i], z1[i])
        poly_1_list[i] = [x1[i], y1[i], z1[i]]

    p_normal = mathutils.geometry.normal([polygon_1[0], polygon_1[1], polygon_1[2]])
    max_value = max(np.fabs(p_normal))
    max_index = [i for i, j in enumerate(p_normal) if j == max_value or j == -max_value]  # find position of non-maximum normal component
    max_index = max_index[0]

    # reduce polygon 1 to 2D projection
    reduced_poly_1 = [None]*n_points_poly_1
    for idx, cur_point in enumerate(polygon_1):
        reduced_poly_1[idx] = cur_point[:max_index] + cur_point[max_index+1:]

    # create path from polygon 1
    poly_1_path = path.Path(reduced_poly_1)

    # rotate the points to xy-plane for plotting
    [local_coords, loc0, locx, locy] = rotate_in_plane_points_3d_to_2d(poly_1_list)
    local_coords = np.array(local_coords)
    polygon_2d = {'x': local_coords[:, 0], 'y': local_coords[:, 1]}
    area_p1_2d = poly_area_2d(local_coords[:, 0], local_coords[:, 1])

    # investigate the polygons (2) and find intersections with polygon 1
    intersections_2d = []
    outcrop_intersections_3d = []
    for fr in polygons:
        try:
            x2 = fr['xT']
            y2 = fr['yT']
            z2 = fr['zT']
            n_points_poly_2 = len(x2)  # polygon 2 = crack (plane q)

            # find cartesian box surrounding the polygon
            x2_min = min(x2)
            x2_max = max(x2)
            y2_min = min(y2)
            y2_max = max(y2)
            z2_min = min(z2)
            z2_max = max(z2)

            check_intersections = False
            if (x2_min < x1_max and x1_min < x2_max) and (y2_min < y1_max and y1_min < y2_max) and (z2_min < z1_max and z1_min < z2_max):
                check_intersections = True

            if check_intersections:
                polygon_2 = [None]*n_points_poly_2
                for i in range(0, n_points_poly_2):
                    polygon_2[i] = (x2[i], y2[i], z2[i])

                # find segments in polygon 2 that intersect the plane of polygon 1
                pq_distances = [None] * n_points_poly_2
                for idx, q_point in enumerate(polygon_2):
                    pq_distances[idx] = mathutils.geometry.distance_point_to_plane(q_point, polygon_1[0], p_normal)

                if pq_distances[-1] < 0:
                    positive_dist = False
                else:
                    positive_dist = True

                inter_segments_idx = []
                for idx, dist in enumerate(pq_distances):
                    if dist < 0:
                        if positive_dist:
                            inter_segments_idx.append((idx-1, idx))
                            positive_dist = False
                        else:
                            positive_dist = False
                    else:
                        if positive_dist:
                            positive_dist = True
                        else:
                            inter_segments_idx.append((idx-1, idx))
                            positive_dist = True

                if len(inter_segments_idx) == 2:
                    # find intersections of the polygon 1 plane with polygon 2
                    line_p1 = polygon_2[inter_segments_idx[0][0]]
                    line_p2 = polygon_2[inter_segments_idx[0][1]]
                    intersection_point_1 = mathutils.geometry.intersect_line_plane(line_p1, line_p2, polygon_1[0], p_normal)

                    line_p1 = polygon_2[inter_segments_idx[1][0]]
                    line_p2 = polygon_2[inter_segments_idx[1][1]]
                    intersection_point_2 = mathutils.geometry.intersect_line_plane(line_p1, line_p2, polygon_1[0], p_normal)

                    # find intersections of a segment connecting points 1 and 2 and edges of polygon 1
                    reduced_intersection_point_1 = intersection_point_1[:max_index] + intersection_point_1[max_index+1:]
                    reduced_intersection_point_2 = intersection_point_2[:max_index] + intersection_point_2[max_index+1:]

                    points_in_poly_1 = poly_1_path.contains_points([reduced_intersection_point_1, reduced_intersection_point_2])

                    if points_in_poly_1[0] and points_in_poly_1[1]:
                        is_there_intersection = True
                        intersection_point_1 = [intersection_point_1[0], intersection_point_1[1], intersection_point_1[2]]
                        intersection_point_2 = [intersection_point_2[0], intersection_point_2[1], intersection_point_2[2]]
                    else:
                        pq_intersections = []
                        segment_1_path = path.Path([reduced_intersection_point_1, reduced_intersection_point_2])
                        for i in range(0, n_points_poly_1):
                            temp_point_1 = polygon_1[i-1]
                            temp_point_2 = polygon_1[i]
                            reduced_temp_point_1 = temp_point_1[:max_index] + temp_point_1[max_index+1:]
                            reduced_temp_point_2 = temp_point_2[:max_index] + temp_point_2[max_index+1:]
                            segment_2_path = path.Path([reduced_temp_point_1, reduced_temp_point_2])
                            path_intersects = segment_1_path.intersects_path(segment_2_path)
                            '''
                            if path_intersects:
                                intersection_point = line_line_intersection(intersection_point_1, intersection_point_2,
                                                                       polygon_1[i-1], polygon_1[i])
                                pq_intersections.append([intersection_point[0], intersection_point[1], intersection_point[2]])
                            '''
                            if path_intersects:
                                intersection_point = mathutils.geometry.intersect_line_line(intersection_point_1, intersection_point_2,
                                                                                            polygon_1[i-1], polygon_1[i])
                                intersection_point = intersection_point[0]
                                pq_intersections.append([intersection_point[0], intersection_point[1], intersection_point[2]])
                        if points_in_poly_1[0] and len(pq_intersections) > 0:
                            is_there_intersection = True
                            intersection_point_1 = [intersection_point_1[0], intersection_point_1[1], intersection_point_1[2]]
                            intersection_point_2 = pq_intersections[0]
                        elif points_in_poly_1[1] and len(pq_intersections) > 0:
                            is_there_intersection = True
                            intersection_point_1 = pq_intersections[0]
                            intersection_point_2 = [intersection_point_2[0], intersection_point_2[1], intersection_point_2[2]]
                        elif len(pq_intersections) > 0:
                            is_there_intersection = True
                            intersection_point_1 = pq_intersections[0]
                            intersection_point_2 = pq_intersections[1]
                        else:
                            is_there_intersection = False

                    intersection_segment = [intersection_point_1, intersection_point_2]
                else:
                    is_there_intersection = False

                if is_there_intersection:

                    # rotate the segment to 2D xy-plane
                    local_coords = [(np.dot(p-loc0, locx),  # local X coordinate
                                    np.dot(p-loc0, locy))  # local Y coordinate
                                    for p in intersection_segment]
                    local_coords = np.array(local_coords)

                    # save the intersection data
                    x_2d = local_coords[:, 0]
                    y_2d = local_coords[:, 1]
                    len_2d = np.sqrt((x_2d[1]-x_2d[0])**2+(y_2d[1]-y_2d[0])**2)
                    if len_2d > min_length:
                        intersections_2d.append({'x': x_2d, 'y': y_2d,
                                                 'strike': fr['strike'], 'dip': fr['dip'], 'population': fr['population'],
                                                 'rgb': fr['rgb'], 'length': len_2d,
                                                 'transmissivity': fr['transmissivity']})
                        outcrop_intersections_3d.append({'x': [intersection_point_1[0], intersection_point_2[0]],
                                                         'y': [intersection_point_1[1], intersection_point_2[1]],
                                                         'z': [intersection_point_1[2], intersection_point_2[2]]})
        except:
            pass

    return polygon_2d, intersections_2d, area_p1_2d, outcrop_intersections_3d


def plot_polygon_polygons_intersections(polygon_2d, intersections_2d, title, output_path, n_figure):

    min_x = min(polygon_2d['x'])
    max_x = max(polygon_2d['x'])-min_x
    min_y = min(polygon_2d['y'])
    max_y = max(polygon_2d['y'])-min_y

    fig = plt.figure(n_figure)
    ax = plt.gca()

    x_fill = polygon_2d['x']-min_x
    y_fill = polygon_2d['y']-min_y
    ax.fill(x_fill, y_fill, color=[0.4, 0.4, 0.4, 0.1])

    # plotting intersection of polygon 2 with polygon 1
    for cr in intersections_2d:
        x_plot = cr['x']-min_x
        y_plot = cr['y']-min_y
        ax.plot(x_plot, y_plot, color=cr['rgb'], linewidth=2)

    ax.axis('equal')
    ax.axis([0, max_x, 0, max_y])
    fig.canvas.set_window_title(title)
    plt.savefig('%s/%s.png' % (output_path, title))
    # fig.show()


def poly_poly_intersections_2(polygon, polygon_index, polygons, terminations, intersections):
    x1 = polygon['xT']
    y1 = polygon['yT']
    z1 = polygon['zT']
    n_points_poly_1 = len(x1)  # polygon 1 = just investigated polygon
    pop1 = polygon['population']

    # find cartesian box surrounding the polygon
    x1_min = min(x1)
    x1_max = max(x1)
    y1_min = min(y1)
    y1_max = max(y1)
    z1_min = min(z1)
    z1_max = max(z1)

    polygon_1 = [None]*n_points_poly_1
    for i in range(0, n_points_poly_1):
        polygon_1[i] = (x1[i], y1[i], z1[i])

    p_normal = mathutils.geometry.normal([polygon_1[0], polygon_1[1], polygon_1[2]])
    max_value = max(np.fabs(p_normal))
    max_index = [i for i, j in enumerate(p_normal) if j == max_value or j == -max_value]  # find position of non-maximum normal component
    max_index = max_index[0]

    # reduce polygon 1 to 2D projection
    reduced_poly_1 = [None]*n_points_poly_1
    for idx, cur_point in enumerate(polygon_1):
        reduced_poly_1[idx] = cur_point[:max_index] + cur_point[max_index+1:]

    # create path from polygon 1
    poly_1_path = path.Path(reduced_poly_1)

    try:
        # investigate the polygons (2) and find intersections with polygon 1
        for n_polygon, fr in enumerate(polygons):
            if n_polygon != polygon_index:
                x2 = fr['xT']
                y2 = fr['yT']
                z2 = fr['zT']
                n_points_poly_2 = len(x2)
                pop2 = fr['population']

                # find cartesian box surrounding the polygon
                x2_min = min(x2)
                x2_max = max(x2)
                y2_min = min(y2)
                y2_max = max(y2)
                z2_min = min(z2)
                z2_max = max(z2)

                check_intersections = False
                cut_polygons = True
                if (x2_min < x1_max and x1_min < x2_max) and (y2_min < y1_max and y1_min < y2_max) and (z2_min < z1_max and z1_min < z2_max):
                    check_intersections = True

                # take into account terminations
                if check_intersections:
                    if random.random() < terminations[pop2-1][pop1-1]:
                        cut_polygons = False

                if check_intersections:
                    polygon_2 = [None]*n_points_poly_2
                    for i in range(0, n_points_poly_2):
                        polygon_2[i] = (x2[i], y2[i], z2[i])

                    # find segments in polygon 2 that intersect the plane of polygon 1
                    pq_distances = [None] * n_points_poly_2
                    for idx, q_point in enumerate(polygon_2):
                        pq_distances[idx] = mathutils.geometry.distance_point_to_plane(q_point, polygon_1[0], p_normal)

                    if pq_distances[-1] < 0:
                        positive_dist = False
                    else:
                        positive_dist = True

                    inter_segments_idx = []
                    for idx, dist in enumerate(pq_distances):
                        if dist < 0:
                            if positive_dist:
                                inter_segments_idx.append((idx-1, idx))
                                positive_dist = False
                            else:
                                positive_dist = False
                        else:
                            if positive_dist:
                                positive_dist = True
                            else:
                                inter_segments_idx.append((idx-1, idx))
                                positive_dist = True

                    if len(inter_segments_idx) == 2:
                        # find intersections of the polygon 1 plane with polygon 2
                        line_p1 = polygon_2[inter_segments_idx[0][0]]
                        line_p2 = polygon_2[inter_segments_idx[0][1]]
                        intersection_point_1 = mathutils.geometry.intersect_line_plane(line_p1, line_p2, polygon_1[0], p_normal)

                        line_p1 = polygon_2[inter_segments_idx[1][0]]
                        line_p2 = polygon_2[inter_segments_idx[1][1]]
                        intersection_point_2 = mathutils.geometry.intersect_line_plane(line_p1, line_p2, polygon_1[0], p_normal)

                        # find intersections of a segment connecting points 1 and 2 and edges of polygon 1
                        reduced_intersection_point_1 = intersection_point_1[:max_index] + intersection_point_1[max_index+1:]
                        reduced_intersection_point_2 = intersection_point_2[:max_index] + intersection_point_2[max_index+1:]

                        points_in_poly_1 = poly_1_path.contains_points([reduced_intersection_point_1, reduced_intersection_point_2])

                        if points_in_poly_1[0] and points_in_poly_1[1]:
                            is_there_intersection = True
                            intersection_point_1 = [intersection_point_1[0], intersection_point_1[1], intersection_point_1[2]]
                            intersection_point_2 = [intersection_point_2[0], intersection_point_2[1], intersection_point_2[2]]
                        else:
                            pq_intersections = []
                            segment_1_path = path.Path([reduced_intersection_point_1, reduced_intersection_point_2])
                            for i in range(n_points_poly_1):
                                temp_point_1 = polygon_1[i-1]
                                temp_point_2 = polygon_1[i]
                                reduced_temp_point_1 = temp_point_1[:max_index] + temp_point_1[max_index+1:]
                                reduced_temp_point_2 = temp_point_2[:max_index] + temp_point_2[max_index+1:]
                                segment_2_path = path.Path([reduced_temp_point_1, reduced_temp_point_2])
                                path_intersects = segment_1_path.intersects_path(segment_2_path)
                                if path_intersects:
                                    intersection_point = line_line_intersection(intersection_point_1, intersection_point_2,
                                                                           polygon_1[i-1], polygon_1[i])
                                    pq_intersections.append([intersection_point[0], intersection_point[1], intersection_point[2]])
                                    '''
                                    intersection_point = mathutils.geometry.intersect_line_line(intersection_point_1, intersection_point_2,
                                                                                            polygon_1[i-1], polygon_1[i])
                                    intersection_point = intersection_point[0]
                                    pq_intersections.append([intersection_point[0], intersection_point[1], intersection_point[2]])
                                    '''
                            if points_in_poly_1[0]:
                                is_there_intersection = True
                                intersection_point_1 = [intersection_point_1[0], intersection_point_1[1], intersection_point_1[2]]
                                intersection_point_2 = pq_intersections[0]
                            elif points_in_poly_1[1]:
                                is_there_intersection = True
                                intersection_point_1 = pq_intersections[0]
                                intersection_point_2 = [intersection_point_2[0], intersection_point_2[1], intersection_point_2[2]]
                            elif len(pq_intersections) > 1:
                                is_there_intersection = True
                                intersection_point_1 = pq_intersections[0]
                                intersection_point_2 = pq_intersections[1]
                            else:
                                is_there_intersection = False

                        intersection_segment = [intersection_point_1, intersection_point_2]

                        if is_there_intersection:
                            positive_side = bool(random.getrandbits(1))
                            if positive_side:
                                chosen_points = np.greater(pq_distances,0)
                            else:
                                chosen_points = np.less(pq_distances,0)

                            intersections_in_polygon = polygons[n_polygon]['intersection']
                            intersections_in_polygon_to_save = polygons[n_polygon]['intersection']
                            intersections_in_polygon_1 = polygons[polygon_index]['intersection']

                            if cut_polygons:
                                for inpos, intersection in enumerate(intersections_in_polygon):
                                    if intersection != 0:
                                        actual_intersection = intersections[intersection-1]

                                        if len(actual_intersection) > 1:
                                            orig_length_intersection = np.sqrt((actual_intersection['x'][1]-actual_intersection['x'][0])**2
                                                                               +(actual_intersection['y'][1]-actual_intersection['y'][0])**2
                                                                               +(actual_intersection['z'][1]-actual_intersection['z'][0])**2)
                                            [seg_intersection, seg_intersected_polygons] = segment_polygons_intersections(actual_intersection, [polygon])

                                            points_of_intersection = [(actual_intersection['x'][0], actual_intersection['y'][0], actual_intersection['z'][0]),
                                                                      (actual_intersection['x'][1], actual_intersection['y'][1], actual_intersection['z'][1])]

                                            point_distances = [mathutils.geometry.distance_point_to_plane(points_of_intersection[0], polygon_1[0], p_normal),
                                                                   mathutils.geometry.distance_point_to_plane(points_of_intersection[1], polygon_1[0], p_normal)]

                                            if len(seg_intersection) > 0 and seg_intersection[0] is not None:

                                                for poi in seg_intersection:  # just a single fr, but must be done in this way to extract data from vector type
                                                    new_points_of_intersection = [(poi[0], poi[1], poi[2])]

                                                if (point_distances[0] < 0 and point_distances[1] > 0) or (point_distances[0] > 0 and point_distances[1] < 0):
                                                    for i in range(2):
                                                        if positive_side:
                                                            if point_distances[i] > 0:
                                                                new_points_of_intersection.append(points_of_intersection[i])
                                                        else:
                                                            if point_distances[i] < 0:
                                                                new_points_of_intersection.append(points_of_intersection[i])

                                                    intersection_to_save = {'x': [new_points_of_intersection[0][0], new_points_of_intersection[1][0]],
                                                                            'y': [new_points_of_intersection[0][1], new_points_of_intersection[1][1]],
                                                                            'z': [new_points_of_intersection[0][2], new_points_of_intersection[1][2]],
                                                                            'polygon_indices': intersections[intersection-1]['polygon_indices']}

                                                    new_length_intersection = np.sqrt((intersection_to_save['x'][1]-intersection_to_save['x'][0])**2
                                                                               +(intersection_to_save['y'][1]-intersection_to_save['y'][0])**2
                                                                               +(intersection_to_save['z'][1]-intersection_to_save['z'][0])**2)
                                                    if orig_length_intersection > new_length_intersection:
                                                        if new_length_intersection < 0.001:
                                                            intersections[intersection-1] = [0]
                                                            intersections_in_polygon_to_save.pop(inpos)
                                                            polygons[n_polygon]['intersection'] = intersections_in_polygon_to_save
                                                        else:
                                                            intersections[intersection-1] = intersection_to_save

                                                elif point_distances[0] < 0 and point_distances[1] < 0:
                                                    if positive_side:
                                                        intersections[intersection-1] = [0]
                                                        intersections_in_polygon_to_save.pop(inpos)
                                                        polygons[n_polygon]['intersection'] = intersections_in_polygon_to_save
                                                else:
                                                    if not positive_side:
                                                        intersections[intersection-1] = [0]
                                                        intersections_in_polygon_to_save.pop(inpos)
                                                        polygons[n_polygon]['intersection'] = intersections_in_polygon_to_save
                                            else:
                                                if point_distances[0] < 0 and point_distances[1] < 0:
                                                    if positive_side:
                                                        intersections[intersection-1] = [0]
                                                        intersections_in_polygon_to_save.pop(inpos)
                                                        polygons[n_polygon]['intersection'] = intersections_in_polygon_to_save
                                                else:
                                                    if not positive_side:
                                                        intersections[intersection-1] = [0]
                                                        intersections_in_polygon_to_save.pop(inpos)
                                                        polygons[n_polygon]['intersection'] = intersections_in_polygon_to_save
                                        else:
                                            intersections_in_polygon_to_save.pop(inpos)
                                            polygons[n_polygon]['intersection'] = intersections_in_polygon_to_save

                                first_position = np.argmin(chosen_points)
                                chosen_points = chosen_points.tolist()
                                second_position = len(chosen_points)-chosen_points[::-1].index(False)-1

                                n_erased_points = chosen_points.count(False)
                                new_polygon_1 = []
                                for n_p, point in enumerate(chosen_points):
                                    if point:
                                        new_polygon_1.append(polygon_2[n_p])
                                    elif n_p == first_position:
                                        new_polygon_1.append(tuple(intersection_segment[0]))
                                        if n_erased_points == 1:
                                           new_polygon_1.append(tuple(intersection_segment[1]))
                                    elif n_p == second_position:
                                        new_polygon_1.append(tuple(intersection_segment[1]))
                                new_polygon = sort_points_3d(new_polygon_1)

                                n_points_poly_new = len(new_polygon)
                                x_new, y_new, z_new, area_to_save = [None]*n_points_poly_new, [None]*n_points_poly_new, \
                                                                    [None]*n_points_poly_new, [None]*n_points_poly_new
                                for i in range(n_points_poly_new):
                                    x_new[i] = new_polygon[i][0]
                                    y_new[i] = new_polygon[i][1]
                                    z_new[i] = new_polygon[i][2]
                                    area_to_save[i] = [x_new[i], y_new[i], z_new[i]]

                            intersections.append({'x': [intersection_segment[0][0], intersection_segment[1][0]],
                                                  'y': [intersection_segment[0][1], intersection_segment[1][1]],
                                                  'z': [intersection_segment[0][2], intersection_segment[1][2]],
                                                  'polygon_indices': [polygon_index, n_polygon]})

                            intersections_in_polygon.append(len(intersections))
                            intersections_in_polygon_1.append(len(intersections))

                            if cut_polygons:
                                polygons[n_polygon] = {'xT': x_new, 'yT': y_new, 'zT': z_new, 'dip': fr['dip'],
                                                       'strike': fr['strike'], 'population': fr['population'], 'rgb': fr['rgb'],
                                                       'intersection': intersections_in_polygon, 'area_orig': fr['area_orig'],
                                                       'area_new': poly_area_3d(area_to_save)}
                            else:
                                polygons[n_polygon] = {'xT': x2, 'yT': y2, 'zT': z2, 'dip': fr['dip'],
                                                       'strike': fr['strike'], 'population': fr['population'], 'rgb': fr['rgb'],
                                                       'intersection': intersections_in_polygon, 'area_orig': fr['area_orig'],
                                                       'area_new': fr['area_new']}

                            polygons[polygon_index]['intersection'] = intersections_in_polygon_1
    except:
        pass

    return polygons, intersections


def poly_poly_intersections_2_no_intersections(polygon, polygon_index, polygons, terminations):

    poly_1_loaded = False
    pop1 = polygon['population']

    try:
        # investigate the polygons (2) and find intersections with polygon 1
        for n_polygon, fr in enumerate(polygons):
            if n_polygon != polygon_index:
                x2 = fr['xT']
                y2 = fr['yT']
                z2 = fr['zT']
                n_points_poly_2 = len(x2)
                pop2 = fr['population']

                if random.random() > terminations[pop2-1][pop1-1]:
                    if not poly_1_loaded:
                        x1 = polygon['xT']
                        y1 = polygon['yT']
                        z1 = polygon['zT']
                        n_points_poly_1 = len(x1)  # polygon 1 = just investigated polygon

                        # find cartesian box surrounding the polygon
                        x1_min = min(x1)
                        x1_max = max(x1)
                        y1_min = min(y1)
                        y1_max = max(y1)
                        z1_min = min(z1)
                        z1_max = max(z1)

                        polygon_1 = [None]*n_points_poly_1
                        for i in range(0, n_points_poly_1):
                            polygon_1[i] = (x1[i], y1[i], z1[i])

                        p_normal = mathutils.geometry.normal([polygon_1[0], polygon_1[1], polygon_1[2]])
                        max_value = max(np.fabs(p_normal))
                        max_index = [i for i, j in enumerate(p_normal) if j == max_value or j == -max_value]
                        max_index = max_index[0]

                        # reduce polygon 1 to 2D projection
                        reduced_poly_1 = [None]*n_points_poly_1
                        for idx, cur_point in enumerate(polygon_1):
                            reduced_poly_1[idx] = cur_point[:max_index] + cur_point[max_index+1:]

                        # create path from polygon 1
                        poly_1_path = path.Path(reduced_poly_1)
                        poly_1_loaded = True

                    # find cartesian box surrounding the polygon
                    x2_min = min(x2)
                    x2_max = max(x2)
                    y2_min = min(y2)
                    y2_max = max(y2)
                    z2_min = min(z2)
                    z2_max = max(z2)

                    check_intersections = False
                    cut_polygons = True
                    if (x2_min < x1_max and x1_min < x2_max) and (y2_min < y1_max and y1_min < y2_max) and (z2_min < z1_max and z1_min < z2_max):
                        check_intersections = True

                    if check_intersections:
                        polygon_2 = [None]*n_points_poly_2
                        for i in range(0, n_points_poly_2):
                            polygon_2[i] = (x2[i], y2[i], z2[i])

                        # find segments in polygon 2 that intersect the plane of polygon 1
                        pq_distances = [None] * n_points_poly_2
                        for idx, q_point in enumerate(polygon_2):
                            pq_distances[idx] = mathutils.geometry.distance_point_to_plane(q_point, polygon_1[0], p_normal)

                        if pq_distances[-1] < 0:
                            positive_dist = False
                        else:
                            positive_dist = True

                        inter_segments_idx = []
                        for idx, dist in enumerate(pq_distances):
                            if dist < 0:
                                if positive_dist:
                                    inter_segments_idx.append((idx-1, idx))
                                    positive_dist = False
                                else:
                                    positive_dist = False
                            else:
                                if positive_dist:
                                    positive_dist = True
                                else:
                                    inter_segments_idx.append((idx-1, idx))
                                    positive_dist = True

                        if len(inter_segments_idx) == 2:
                            # find intersections of the polygon 1 plane with polygon 2
                            line_p1 = polygon_2[inter_segments_idx[0][0]]
                            line_p2 = polygon_2[inter_segments_idx[0][1]]
                            intersection_point_1 = mathutils.geometry.intersect_line_plane(line_p1, line_p2, polygon_1[0], p_normal)

                            line_p1 = polygon_2[inter_segments_idx[1][0]]
                            line_p2 = polygon_2[inter_segments_idx[1][1]]
                            intersection_point_2 = mathutils.geometry.intersect_line_plane(line_p1, line_p2, polygon_1[0], p_normal)

                            # find intersections of a segment connecting points 1 and 2 and edges of polygon 1
                            reduced_intersection_point_1 = intersection_point_1[:max_index] + intersection_point_1[max_index+1:]
                            reduced_intersection_point_2 = intersection_point_2[:max_index] + intersection_point_2[max_index+1:]

                            points_in_poly_1 = poly_1_path.contains_points([reduced_intersection_point_1, reduced_intersection_point_2])

                            if points_in_poly_1[0] and points_in_poly_1[1]:
                                is_there_intersection = True
                                intersection_point_1 = [intersection_point_1[0], intersection_point_1[1], intersection_point_1[2]]
                                intersection_point_2 = [intersection_point_2[0], intersection_point_2[1], intersection_point_2[2]]
                            else:
                                pq_intersections = []
                                segment_1_path = path.Path([reduced_intersection_point_1, reduced_intersection_point_2])
                                for i in range(n_points_poly_1):
                                    temp_point_1 = polygon_1[i-1]
                                    temp_point_2 = polygon_1[i]
                                    reduced_temp_point_1 = temp_point_1[:max_index] + temp_point_1[max_index+1:]
                                    reduced_temp_point_2 = temp_point_2[:max_index] + temp_point_2[max_index+1:]
                                    segment_2_path = path.Path([reduced_temp_point_1, reduced_temp_point_2])
                                    path_intersects = segment_1_path.intersects_path(segment_2_path)
                                    if path_intersects:
                                        intersection_point = line_line_intersection(intersection_point_1, intersection_point_2,
                                                                               polygon_1[i-1], polygon_1[i])
                                        pq_intersections.append([intersection_point[0], intersection_point[1], intersection_point[2]])
                                        '''
                                        intersection_point = mathutils.geometry.intersect_line_line(intersection_point_1, intersection_point_2,
                                                                                                polygon_1[i-1], polygon_1[i])
                                        intersection_point = intersection_point[0]
                                        pq_intersections.append([intersection_point[0], intersection_point[1], intersection_point[2]])
                                        '''
                                if points_in_poly_1[0]:
                                    is_there_intersection = True
                                    intersection_point_1 = [intersection_point_1[0], intersection_point_1[1], intersection_point_1[2]]
                                    intersection_point_2 = pq_intersections[0]
                                elif points_in_poly_1[1]:
                                    is_there_intersection = True
                                    intersection_point_1 = pq_intersections[0]
                                    intersection_point_2 = [intersection_point_2[0], intersection_point_2[1], intersection_point_2[2]]
                                elif len(pq_intersections) > 1:
                                    is_there_intersection = True
                                    intersection_point_1 = pq_intersections[0]
                                    intersection_point_2 = pq_intersections[1]
                                else:
                                    is_there_intersection = False

                            intersection_segment = [intersection_point_1, intersection_point_2]

                            if is_there_intersection:
                                positive_side = bool(random.getrandbits(1))
                                if positive_side:
                                    chosen_points = np.greater(pq_distances,0)
                                else:
                                    chosen_points = np.less(pq_distances,0)

                                first_position = np.argmin(chosen_points)
                                chosen_points = chosen_points.tolist()
                                second_position = len(chosen_points)-chosen_points[::-1].index(False)-1

                                n_erased_points = chosen_points.count(False)
                                new_polygon_1 = []
                                for n_p, point in enumerate(chosen_points):
                                    if point:
                                        new_polygon_1.append(polygon_2[n_p])
                                    elif n_p == first_position:
                                        new_polygon_1.append(tuple(intersection_segment[0]))
                                        if n_erased_points == 1:
                                           new_polygon_1.append(tuple(intersection_segment[1]))
                                    elif n_p == second_position:
                                        new_polygon_1.append(tuple(intersection_segment[1]))
                                new_polygon = sort_points_3d(new_polygon_1)

                                n_points_poly_new = len(new_polygon)
                                x_new, y_new, z_new, area_to_save = [None]*n_points_poly_new, [None]*n_points_poly_new, \
                                                                    [None]*n_points_poly_new, [None]*n_points_poly_new
                                for i in range(n_points_poly_new):
                                    x_new[i] = new_polygon[i][0]
                                    y_new[i] = new_polygon[i][1]
                                    z_new[i] = new_polygon[i][2]
                                    area_to_save[i] = [x_new[i], y_new[i], z_new[i]]

                                polygons[n_polygon] = {'xT': x_new, 'yT': y_new, 'zT': z_new, 'dip': fr['dip'],
                                                       'strike': fr['strike'], 'population': fr['population'], 'rgb': fr['rgb'],
                                                       'intersection': [], 'area_orig': fr['area_orig'],
                                                       'area_new': poly_area_3d(area_to_save)}
    except:
        pass
    return polygons


def check_intersections(polygons, intersections):

    intersections_temp = []
    for isec in intersections:
        if len(isec) > 1:
            intersections_temp.append(isec)

    intersections_reduced = []
    for isec in intersections_temp:

        p1_to_check = polygons[isec['polygon_indices'][0]]
        p2_to_check = polygons[isec['polygon_indices'][1]]

        xs1 = isec['x'][0]
        xs2 = isec['x'][1]

        x1 = p1_to_check['xT']
        x2 = p2_to_check['xT']
        y1 = p1_to_check['yT']
        y2 = p2_to_check['yT']
        z1 = p1_to_check['zT']
        z2 = p2_to_check['zT']

        check_1 = False
        check_2 = False
        check = False

        for x_test in x1:
            if x_test == xs1:
                poly_2 = [None]*len(x2)
                for j in range(len(x2)):
                    poly_2[j] = (x2[j], y2[j])
                poly_2_path = path.Path(poly_2)
                xy_intersection_1 = (isec['x'][0], isec['y'][0])
                intersection_in_poly_2 = poly_2_path.contains_points([xy_intersection_1])
                if intersection_in_poly_2:
                    check_1 = True
                    break
                else:
                    segment_1_path = path.Path([(isec['x'][0], isec['y'][0]), (isec['x'][1], isec['y'][1])])
                    for i in range(len(x2)):
                        temp_point_1 = (x2[i-1], y2[i-1])
                        temp_point_2 = (x2[i], y2[i])
                        segment_2_path = path.Path([temp_point_1, temp_point_2])
                        path_intersects = segment_1_path.intersects_path(segment_2_path)
                        if path_intersects:
                            intersection_point = line_line_intersection((isec['x'][0], isec['y'][0], isec['z'][0]),
                                                                        (isec['x'][1], isec['y'][1], isec['z'][1]),
                                                                        (x2[i-1], y2[i-1], z2[i-1]),
                                                                        (x2[i], y2[i], z2[i]))
                            isec['x'][0] = intersection_point[0]
                            isec['y'][0] = intersection_point[1]
                            isec['z'][0] = intersection_point[2]
                            check_1 = True
                            break

        if check_1:
            for x_test in x1:
                if x_test == xs2:
                    check = True
                    break
        else:
            for x_test in x2:
                if x_test == xs1:
                    poly_1 = [None]*len(x1)
                    for j in range(len(x1)):
                        poly_1[j] = (x1[j], y1[j])
                    poly_1_path = path.Path(poly_1)
                    xy_intersection_2 = (isec['x'][1], isec['y'][1])
                    intersection_in_poly_1 = poly_1_path.contains_points([xy_intersection_2])
                    if intersection_in_poly_1:
                        check_2 = True
                        break
                    else:
                        segment_1_path = path.Path([(isec['x'][0], isec['y'][0]), (isec['x'][1], isec['y'][1])])
                        for i in range(len(x1)):
                            temp_point_1 = (x1[i-1], y1[i-1])
                            temp_point_2 = (x1[i], y1[i])
                            segment_2_path = path.Path([temp_point_1, temp_point_2])
                            path_intersects = segment_1_path.intersects_path(segment_2_path)
                            if path_intersects:
                                intersection_point = line_line_intersection((isec['x'][0], isec['y'][0], isec['z'][0]),
                                                                            (isec['x'][1], isec['y'][1], isec['z'][1]),
                                                                            (x1[i-1], y1[i-1], z1[i-1]),
                                                                            (x1[i], y1[i], z1[i]))
                                isec['x'][1] = intersection_point[0]
                                isec['y'][1] = intersection_point[1]
                                isec['z'][1] = intersection_point[2]
                                check_2 = True
                                break
                        if check_2:
                            break
                if check_2:
                    break

            if check_2:
                for x_test in x2:
                    if x_test == xs2:
                        check = True
                        break

        if check:
            intersections_reduced.append(isec)
        else:
            poly_1 = [None]*len(x1)
            for j in range(len(x1)):
                poly_1[j] = (x1[j], y1[j])
            poly_1_path = path.Path(poly_1)
            poly_2 = [None]*len(x2)
            for j in range(len(x2)):
                poly_2[j] = (x2[j], y2[j])
            poly_2_path = path.Path(poly_2)
            xy_intersection_1 = (isec['x'][0], isec['y'][0])
            xy_intersection_2 = (isec['x'][1], isec['y'][1])
            if poly_1_path.contains_points([xy_intersection_1]) and poly_2_path.contains_points([xy_intersection_2]):
                intersections_reduced.append(isec)
            elif poly_1_path.contains_points([xy_intersection_2]) and poly_2_path.contains_points([xy_intersection_1]):
                intersections_reduced.append(isec)

    for i in range(len(polygons)):
        polygons[i]['intersection'] = []

    for i, isec in enumerate(intersections_reduced):
        polygons[isec['polygon_indices'][0]]['intersection'].append(i)
        polygons[isec['polygon_indices'][1]]['intersection'].append(i)

    return polygons, intersections_reduced
