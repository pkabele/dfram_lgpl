#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
import numpy as np
from output_file import write_output
from polypolyintersect import area_by_tri


# by Petr Kabele
def plane_area(verts):
    """
    Finds a plane as the least-sqare fit to a spatial polygon
    (ref. http://www.ilikebigbits.com/blog/2017/9/24/fitting-a-plane-to-noisy-points-in-3d)
    Then projects the vertices to this plane (note 3.12.2017).
    Vertices (verts) are in the form of array([[x0,y0,z0],[x1,y1,z1],...].
    Returns the vertices of the projected polygon, area, normal and sum of the
    norms of the projection vectors (gives idea how much the original polygon
    deviated from plane).
    """

    # Geometric center
    cent = np.mean(verts, axis=0)
    # Covariance matrix
    covm = np.cov(verts - cent, rowvar=False)
    # eigenvalues and eigenvectors of covariance matrix
    eigen = np.linalg.eigh(covm)
    # eigenvector corresponding to the least eigenvalue is the sought plane normal
    nvec = eigen[1][:, 0]
    # project the original vertices to this plane and calculate the total length of the projection vector
    pverts = []
    pnsum = 0
    for ivert, vert in enumerate(verts):
        p = -np.dot(vert - cent, nvec)
        pvec = p * nvec
        pvert = vert + pvec
        pverts.append(pvert)
        pnsum += abs(p)
    # Calculate the area of the projected polygon
    parea, _ = area_by_tri(pverts)

    return pverts, parea, nvec, pnsum


def recalculate_outcrops(project_path, outcrop_name, outcrop_area_old, vertices):
    new_vertices, new_area, outcrop_normal, _ = plane_area(vertices)
    write_output(project_path, '%s\t%.6f\t --> %.6f' % (outcrop_name, outcrop_area_old, new_area))

    '''
    !!! plotting does not work - necessary to set boundaries

    import matplotlib.pyplot as plt
    import mpl_toolkits.mplot3d as mp3d

    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection='3d')

    poly = mp3d.art3d.Poly3DCollection([vertices], alpha=0.8, linewidth=1)
    poly.set_facecolor('red')
    ax.add_collection3d(poly)

    poly = mp3d.art3d.Poly3DCollection([new_vertices], alpha=0.8, linewidth=1)
    poly.set_facecolor('blue')
    ax.add_collection3d(poly)

    plt.savefig('fig.png')
    '''

    return new_vertices, new_area, outcrop_normal
