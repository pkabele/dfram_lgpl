#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
from tictoc import *
import xlwt
import numpy as np
import sys
import fileinput


def initiate_output_file(path, project_name, time_of_start):
    f_out = open(path+'/calculation_log.txt', 'w')
    f_out.write('--------------------------------------------------------------------------------\n')
    f_out.write('DFN by DFRaM: Calculation report \n')
    f_out.write('Project name: %s \n' % project_name)
    f_out.write('Calculation started: %s \n' % time_of_start)
    f_out.write('--------------------------------------------------------------------------------\n')
    f_out.write('\n')
    f_out.close()
    get_start_time()


def initiate_measurements_file(path, project_name, time_of_start):
    f_out = open(path+'/stats_observations.txt', 'w')
    f_out.write('--------------------------------------------------------------------------------\n')
    f_out.write('DFN by DFRaM: Statistical evaluation of observed fractures on outcrops \n')
    f_out.write('Project name: %s \n' % project_name)
    f_out.write('Calculation started: %s \n' % time_of_start)
    f_out.write('--------------------------------------------------------------------------------\n')
    f_out.write('\n')
    f_out.close()


def write_output(path, information):
    f_out = open(path+'/calculation_log.txt', 'a')
    string_to_print = get_duration() + '\t\t' + information + '\n'
    string_to_display = get_duration() + '\t\t' + information
    f_out.write(string_to_print)
    print(string_to_display)
    f_out.close()


def write_meas_output(path, information):
    f_out = open(path+'/stats_observations.txt', 'a')
    string_to_print = information + '\n'
    string_to_display = '\t\t' + information
    f_out.write(string_to_print)
    print(string_to_display)
    f_out.close()


def write_calculation_header(path, project_name, time_of_start):
    f_out = open(path+'/stats_calculations.txt', 'w')
    f_out.write('--------------------------------------------------------------------------------\n')
    f_out.write('DFN by DFRaM: Statistical evaluation of calculated fractures on virtual outcrops\n')
    f_out.write('Project name: %s \n' % project_name)
    f_out.write('Calculation started: %s \n' % time_of_start)
    f_out.write('--------------------------------------------------------------------------------\n')
    f_out.write('\n')
    f_out.close()


def write_calculation_results(cur_path, cur_pop, dip_2d_mean, strike_2d_mean, kappa_2d, mean_length_2d,
                              length_unit, density_P21, x_2d_start, outcrop_name, density_P20):
    f_out = open(cur_path + '/stats_calculations.txt', 'a')
    f_out.write('Statistics for virtual outcrop %s and population %d: \n' % (outcrop_name, cur_pop+1))
    f_out.write('Mean length of fractures: %.3f %s \n' % (mean_length_2d, length_unit))
    f_out.write('Fracture density (P21): %.3f %s-1 \n' % (density_P21, length_unit))
    f_out.write('Fracture density (P20): %.3f %s-2 \n' % (density_P20, length_unit))
    f_out.write('Number of fractures: %d \n\n' % len(x_2d_start))
    f_out.close()

    print('\n')
    print('Statistics for virtual outcrop %s and population %d:' % (outcrop_name, cur_pop+1))
    print('Mean length of fractures: %.3f %s' % (mean_length_2d, length_unit))
    print('Fracture density (P21): %.3f %s-1' % (density_P21, length_unit))
    print('Fracture density (P20): %.3f %s-2' % (density_P20, length_unit))
    print('Number of fractures: %d \n' % len(x_2d_start))


def write_optimization_results(cur_path, fracture_densities, x_mins, alphas, strikes, dips, kappas, perc, rgbs,
                               jsd_all_pops, n_fractures):

    f_out = open(cur_path + '/DFN_unit_mean_vectors.txt', 'w')
    mus = []
    for idx, output in enumerate(strikes):
        mu = unit_vector_from_dips_and_strikes(dips[idx], strikes[idx])
        mus.append(mu)
        if idx == len(fracture_densities) - 1:
            f_out.write('%.6f, %.6f, %.6f' % (mu[0], mu[1], mu[2]))
        else:
            f_out.write('%.6f, %.6f, %.6f\n ' % (mu[0], mu[1], mu[2]))
    f_out.close()

    f_out = open(cur_path + '/DFN_fracture_densities.txt', 'w')
    for idx, output in enumerate(fracture_densities):
        if idx == len(fracture_densities)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/DFN_x_mins.txt', 'w')
    for idx, output in enumerate(x_mins):
        if idx == len(fracture_densities)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/DFN_alphas.txt', 'w')
    for idx, output in enumerate(alphas):
        if idx == len(fracture_densities)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/DFN_strikes.txt', 'w')
    for idx, output in enumerate(strikes):
        if idx == len(fracture_densities)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/DFN_dips.txt', 'w')
    for idx, output in enumerate(dips):
        if idx == len(fracture_densities)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/DFN_kappas.txt', 'w')
    for idx, output in enumerate(kappas):
        if idx == len(fracture_densities)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/percentage_fracuture_traces_populations.txt', 'w')
    for idx, output in enumerate(perc):
        if idx == len(fracture_densities)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/DFN_rgbs.txt', 'w')
    for output in rgbs:
        f_out.write('%.3f, %.3f, %.3f\n' % (output[0], output[1], output[2]))
    f_out.close()

    # export the results to an excel spreadsheet
    note = '* Jensen-Shannon divergence (JSD) is used to quantify the (dis)similarity between the probability ' \
           'distributions of observed and simulated (1 realization) trace lengths. It ranges between 0 and 1. ' \
           'JSD = 0 if the distributions are indistinguishable.'

    book = xlwt.Workbook()
    sheet = book.add_sheet('Population stats')
    sheet.write(0, 0, 'Population')
    sheet.write(0, 1, 'N fractures')
    sheet.write(0, 2, 'Percentage of traces [%]')
    sheet.write(0, 3, 'Strike')
    sheet.write(0, 4, 'Dip')
    sheet.write_merge(0, 0, 5, 7, 'Unit mean vector')
    sheet.write(0, 8, 'Kappa')
    sheet.write(0, 9, 'x_min [m]')
    sheet.write(0, 10, 'alpha')
    sheet.write(0, 11, 'P30 [m^-3]')
    if len(jsd_all_pops) > 0:
        sheet.write(0, 12, 'JSD *')

    line_counter = 0
    for i in range(len(fracture_densities)):
        line_counter += 1
        if len(jsd_all_pops) > 0:
            row = [i+1, n_fractures[i], perc[i], strikes[i], dips[i], mus[i][0], mus[i][1], mus[i][2], kappas[i], x_mins[i], alphas[i],
                   fracture_densities[i], jsd_all_pops[i]]
        else:
            row = [i+1, n_fractures[i], perc[i], strikes[i], dips[i], mus[i][0], mus[i][1], mus[i][2], kappas[i], x_mins[i], alphas[i],
                   fracture_densities[i]]
        for j in range(len(row)):
            sheet.write(i + 1, j, row[j])
    if len(jsd_all_pops) > 0:
        sheet.write_merge(line_counter+2, line_counter+3, 0, 21, note)
    book.save(cur_path + '/stats_fracture_traces.xls')


def write_optimization_results_2(cur_path, first_population, last_population, fracture_density, x_min, alpha, strike,
                                 dip, kappa):

    if first_population:
        f_out = open(cur_path + '/DFN_unit_mean_vectors.txt', 'w')
    else:
        f_out = open(cur_path + '/DFN_unit_mean_vectors.txt', 'a')
    mu = unit_vector_from_dips_and_strikes(dip, strike)
    if last_population:
        f_out.write('%.6f, %.6f, %.6f' % (mu[0], mu[1], mu[2]))
    else:
        f_out.write('%.6f, %.6f, %.6f\n ' % (mu[0], mu[1], mu[2]))
    f_out.close()

    if first_population:
        f_out = open(cur_path + '/DFN_fracture_densities.txt', 'w')
    else:
        f_out = open(cur_path + '/DFN_fracture_densities.txt', 'a')
    if last_population:
        f_out.write('%.6f' % fracture_density)
    else:
        f_out.write('%.6f, ' % fracture_density)
    f_out.close()

    if first_population:
        f_out = open(cur_path + '/DFN_x_mins.txt', 'w')
    else:
        f_out = open(cur_path + '/DFN_x_mins.txt', 'a')
    if last_population:
        f_out.write('%.6f' % x_min)
    else:
        f_out.write('%.6f, ' % x_min)
    f_out.close()

    if first_population:
        f_out = open(cur_path + '/DFN_alphas.txt', 'w')
    else:
        f_out = open(cur_path + '/DFN_alphas.txt', 'a')
    if last_population:
        f_out.write('%.6f' % alpha)
    else:
        f_out.write('%.6f, ' % alpha)
    f_out.close()

    if first_population:
        f_out = open(cur_path + '/DFN_strikes.txt', 'w')
    else:
        f_out = open(cur_path + '/DFN_strikes.txt', 'a')
    if last_population:
        f_out.write('%.6f' % strike)
    else:
        f_out.write('%.6f, ' % strike)
    f_out.close()

    if first_population:
        f_out = open(cur_path + '/DFN_dips.txt', 'w')
    else:
        f_out = open(cur_path + '/DFN_dips.txt', 'a')
    if last_population:
        f_out.write('%.6f' % dip)
    else:
        f_out.write('%.6f, ' % dip)
    f_out.close()

    if first_population:
        f_out = open(cur_path + '/DFN_kappas.txt', 'w')
    else:
        f_out = open(cur_path + '/DFN_kappas.txt', 'a')
    if last_population:
        f_out.write('%.6f' % kappa)
    else:
        f_out.write('%.6f, ' % kappa)
    f_out.close()


def write_optimization_results_dir_stats_only(cur_path, strikes, dips, kappas, n_fractures):

    f_out = open(cur_path + '/DFN_unit_mean_vectors.txt', 'w')
    mus = []
    for idx, output in enumerate(strikes):
        mu = unit_vector_from_dips_and_strikes(dips[idx], strikes[idx])
        mus.append(mu)
        if idx == len(strikes) - 1:
            f_out.write('%.6f, %.6f, %.6f' % (mu[0], mu[1], mu[2]))
        else:
            f_out.write('%.6f, %.6f, %.6f\n ' % (mu[0], mu[1], mu[2]))
    f_out.close()

    f_out = open(cur_path + '/DFN_unit_mean_vectors.txt', 'w')
    for idx, output in enumerate(strikes):
        mu = unit_vector_from_dips_and_strikes(dips[idx], strikes[idx])
        if idx == len(strikes) - 1:
            f_out.write('%.6f, %.6f, %.6f' % (mu[0], mu[1], mu[2]))
        else:
            f_out.write('%.6f, %.6f, %.6f\n ' % (mu[0], mu[1], mu[2]))
    f_out.close()

    f_out = open(cur_path + '/DFN_strikes.txt', 'w')
    for idx, output in enumerate(strikes):
        if idx == len(strikes)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/DFN_dips.txt', 'w')
    for idx, output in enumerate(dips):
        if idx == len(strikes)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    f_out = open(cur_path + '/DFN_kappas.txt', 'w')
    for idx, output in enumerate(kappas):
        if idx == len(strikes)-1:
            f_out.write('%.6f' % output)
        else:
            f_out.write('%.6f, ' % output)
    f_out.close()

    # export the data to a spreadsheet
    book = xlwt.Workbook()
    sheet = book.add_sheet('Population stats')
    sheet.write(0, 0, 'Population')
    sheet.write(0, 1, 'N fractures')
    sheet.write(0, 2, 'Strike')
    sheet.write(0, 3, 'Dip')
    sheet.write_merge(0, 0, 4, 6, 'Unit mean vector')
    sheet.write(0, 7, 'Kappa')

    for i in range(len(strikes)):
        row = [i + 1, n_fractures[i], strikes[i], dips[i], mus[i][0], mus[i][1], mus[i][2], kappas[i]]
        for j in range(len(row)):
            sheet.write(i + 1, j, row[j])
    book.save(cur_path + '/stats_fracture_traces.xls')


def write_xls_fracture_traces(out_path, simulations_or_observations, outcrops_names, pops_2d, strikes_2d, dips_2d,
                              lens_2d):
    book = xlwt.Workbook()
    sheet = book.add_sheet('Fracture traces ('+simulations_or_observations+')')
    sheet.write(0, 0, 'Outcrop')
    sheet.write(0, 1, 'Population')
    sheet.write(0, 2, 'Strike')
    sheet.write(0, 3, 'Dip')
    sheet.write(0, 4, 'Trace length')

    for i in range(len(outcrops_names)):
        row = [outcrops_names[i], float(pops_2d[i]), strikes_2d[i], dips_2d[i], lens_2d[i]]
        for j in range(5):
            sheet.write(i+1, j, row[j])

    if not os.path.exists(out_path+'/results_populations'):
        os.makedirs(out_path+'/results_populations')
    book.save(out_path+'/results_populations/'+simulations_or_observations+'-fracture_traces_on_outcrops.xls')


def unit_vector_from_dips_and_strikes(dips, strikes):

    from stats import sd_to_vec

    try:
        length_of_dips = len(dips)
    except TypeError:
        dips = np.array([dips])
        strikes = np.array([strikes])
        length_of_dips = 1

    nvecs = []
    A = np.zeros([3, 3])
    for i in range(length_of_dips):
        d = dips[i]
        s = strikes[i]

        #  Vector components of pole (on lower hemisphere)
        nvec = sd_to_vec(s, d)
        nvecs.append(nvec)
        A = A+np.outer(np.matrix(nvec), np.matrix(nvec).T)

    eigenA = np.linalg.eigh(A)
    imax = np.argmax(eigenA[0])
    evec = eigenA[1][:, imax]

    # Reverse the vector if it is it has upward orientation
    if evec[2] > 0:
       evec = np.negative(evec)

    # Calculate the resultant vector R and unit r with reorientation
    Rvec = np.array([0, 0, 0])

    # Number of vectors
    N = len(nvecs)
    # Change orientation of all n-vectors according to e-vector
    for ivec, nvec in enumerate(nvecs):
        if np.inner(evec, nvec) < 0:
            nvec = np.negative(nvec)
            nvecs[ivec] = nvec

        # Add n to R
        Rvec = Rvec+nvec

    # |R|
    Rnorm = np.linalg.norm(Rvec)

    # Unit vector r=R/|R|
    rvec = Rvec/Rnorm

    return rvec


def print_html_report(input_filename, keyword, new_text, keep_keyword=True):
    try:
        f = open(input_filename, 'r')
    except OSError:
        print('Cannot open the report file %s.' % input_filename)
        sys.exit('The DFN verification terminated.')

    text_line = []
    for temp in fileinput.input(input_filename, openhook=fileinput.hook_encoded('utf-8')):

        temp_changed = False
        if keyword in temp:
            temp_changed = True
            if keep_keyword:
                temp_orig = '\n' + temp
            temp = temp.replace(keyword, new_text)

        text_line.append(temp)
        if keep_keyword and temp_changed:
            text_line.append(temp_orig)
    f.close()

    f_out = open(input_filename, 'w', encoding='utf-8')
    for myLineTemp in text_line:
        f_out.write(myLineTemp)
    f_out.close()
