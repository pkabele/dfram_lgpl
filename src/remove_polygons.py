#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
import numpy as np


def remove_small_polygons(polygons, intersections, min_radius, save_intersections):

    for idx, poly in enumerate(polygons):
        eq_radius = np.sqrt(poly['area_new']/3.14159)
        if eq_radius < min_radius:
            polygons[idx]['area_new'] = 0
            if save_intersections:
                for isec in polygons[idx]['intersection']:
                    intersections[isec]['polygon_indices'][0] = 1e11

    poly_numbers = [None]*len(polygons)
    counter = 0
    for idx, poly in enumerate(polygons):
        if poly['area_new'] != 0:
            poly_numbers[idx] = counter
            counter += 1
        else:
            poly_numbers[idx] = 1e12

    if save_intersections:
        isec_numbers = [None]*len(intersections)
        counter = 0
        for idx, isec in enumerate(intersections):
            if isec['polygon_indices'][0] != 1e11:
                isec_numbers[idx] = counter
                counter += 1
            else:
                isec_numbers[idx] = 1e12

    poly_new = []
    for idx, poly in enumerate(polygons):
        if poly['area_new'] != 0:
            if save_intersections:
                temp_isec_in_poly = []
                for isec in poly['intersection']:
                    if isec_numbers[isec] < 1e12:
                        temp_isec_in_poly.append(isec_numbers[isec])
                poly['intersection'] = temp_isec_in_poly
            poly_new.append(poly)

    isec_new = []
    if save_intersections:
        for idx, isec in enumerate(intersections):
            if isec['polygon_indices'][0] != 1e11:
                indices_temp = [poly_numbers[isec['polygon_indices'][0]], poly_numbers[isec['polygon_indices'][1]]]
                isec['polygon_indices'] = indices_temp
                isec_new.append(isec)

    return poly_new, isec_new
