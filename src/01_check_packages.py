#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#

print("Testing necessary packages for DFraM")
#  Comments
#  N: installed by default in Miniconda or as part of another package
#  conda-forge: conda install pkg_name -c conda-forge
#  pip: pip install pkg_name
#  whl: pip install pkg_name.whl
import math  # N
import scipy
import numpy  # N
import matplotlib
import mpl_toolkits  # N
import openpyxl
import xlwt
import xlrd
import tabulate
import datetime  # N
import sys  # N
import os  # N
import tictoc  # N
import random  # N
import copy  # N
import warnings  # N
import subprocess  # N
import fileinput  # N
import powerlaw  # conda-forge
import mplstereonet  # pip
import mathutils  # whl
#		○ Download:  mathutils-2.78-cp36-cp36m-win_amd64.whl from
#		https://www.lfd.uci.edu/~gohlke/pythonlibs/ to c:\ProgramData\Miniconda3\envs\DFraM\
#		○ Install:
#		cd c:\ProgramData\Miniconda3\envs\DFraM\
#		pip install mathutils-2.78-cp36-cp36m-win_amd64.whl
