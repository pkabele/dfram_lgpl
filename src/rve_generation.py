#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
def calculate_outcrop_centroids(outcrop_geometry, x_max):

    outcrop_centroids = []
    rve_sizes = []
    for outcrop in outcrop_geometry:
        n_vert = len(outcrop['x'])
        x_mean = sum(outcrop['x'])/n_vert
        y_mean = sum(outcrop['y'])/n_vert
        z_mean = sum(outcrop['z'])/n_vert
        outcrop_centroids.append((x_mean, y_mean, z_mean))

        rve_size_temp = 0
        for i in range(n_vert):
            vert_distance = ((outcrop['x'][i]-x_mean)**2+(outcrop['y'][i]-y_mean)**2+(outcrop['z'][i]-z_mean)**2)**0.5
            rve_size_temp = max([rve_size_temp, vert_distance])
        rve_size_temp += x_max

        # rve_sizes.append(rve_size_temp * 1.2)
        rve_sizes.append(rve_size_temp*2)

    return rve_sizes, outcrop_centroids


def generate_rves(outcrop_centroids, rve_sizes):

    print_volumes = False  # print volumes for debugging

    rves = []
    total_volume = 0
    if print_volumes:
        test_volume = []

    for idx, centroid in enumerate(outcrop_centroids):

        x_cur_min = centroid[0]-rve_sizes[idx]/2
        x_cur_max = centroid[0]+rve_sizes[idx]/2
        y_cur_min = centroid[1]-rve_sizes[idx]/2
        y_cur_max = centroid[1]+rve_sizes[idx]/2
        z_cur_min = centroid[2]-rve_sizes[idx]/2
        z_cur_max = centroid[2]+rve_sizes[idx]/2

        merged = []
        delete_volume = 0

        if idx > 0:
            for irve, rve in enumerate(rves):
                inside_x, inside_y, inside_z = False, False, False
                x_rve_min = rve['x'][0]
                x_rve_max = rve['x'][1]
                y_rve_min = rve['y'][0]
                y_rve_max = rve['y'][1]
                z_rve_min = rve['z'][0]
                z_rve_max = rve['z'][1]

                if (x_rve_min < x_cur_min and x_rve_max > x_cur_min) or (x_rve_min < x_cur_max and x_rve_max > x_cur_max):
                    inside_x = True
                if (y_rve_min < y_cur_min and y_rve_max > y_cur_min) or (y_rve_min < y_cur_max and y_rve_max > y_cur_max):
                    inside_y = True
                if (z_rve_min < z_cur_min and z_rve_max > z_cur_min) or (z_rve_min < z_cur_max and z_rve_max > z_cur_max):
                    inside_z = True

                if inside_x and inside_y and inside_z:
                    merged.append(irve)
                    delete_volume += (x_rve_max-x_rve_min)*(y_rve_max-y_rve_min)*(z_rve_max-z_rve_min)
                    x_cur_min = min(rve['x'][0], x_cur_min)
                    x_cur_max = max(rve['x'][1], x_cur_max)
                    y_cur_min = min(rve['y'][0], y_cur_min)
                    y_cur_max = max(rve['y'][1], y_cur_max)
                    z_cur_min = min(rve['z'][0], z_cur_min)
                    z_cur_max = max(rve['z'][1], z_cur_max)

        rve_to_append = {'x': [x_cur_min, x_cur_max], 'y': [y_cur_min, y_cur_max], 'z': [z_cur_min, z_cur_max]}
        total_volume += (x_cur_max-x_cur_min)*(y_cur_max-y_cur_min)*(z_cur_max-z_cur_min)

        if print_volumes:
            test_volume.append((x_cur_max-x_cur_min)*(y_cur_max-y_cur_min)*(z_cur_max-z_cur_min))

        rves.append(rve_to_append)
        if delete_volume > 0:
            total_volume -= delete_volume
            for merg in merged:
                rves.pop(merg)
                if print_volumes:
                    test_volume.pop(merg)

    if print_volumes:
        print(test_volume)

    return rves, total_volume
