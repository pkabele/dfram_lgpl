#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
from matplotlib import path
import mathutils
import matplotlib.pyplot as plt
import math
import mpl_toolkits.mplot3d as mp3d
import numpy as np
from numpy.linalg import norm


def segment_polygons_intersections(segment, polygons):
    x1 = segment['x']
    y1 = segment['y']
    z1 = segment['z']
    deltas = [abs(x1[1]-x1[0]), abs(y1[1]-y1[0]), abs(z1[1]-z1[0])]
    max_value = max(deltas)
    max_index = [i for i, j in enumerate(deltas) if j == max_value]  # find position of non-maximum normal component
    max_index = max_index[0]

    # find cartesian box surrounding the segment
    x1_min = min(x1)
    x1_max = max(x1)
    y1_min = min(y1)
    y1_max = max(y1)
    z1_min = min(z1)
    z1_max = max(z1)

    seg = [(x1[0], y1[0], z1[0]), (x1[1], y1[1], z1[1])]

    # investigate polygons and find intersections with segment
    seg_intersections = []
    seg_intersected_polygons = []
    for fr in polygons:
        x2 = fr['xT']
        y2 = fr['yT']
        z2 = fr['zT']
        n_points_poly_2 = len(x2)  # polygon 2 = crack (plane q)

        # find cartesian box surrounding the polygon
        x2_min = min(x2)
        x2_max = max(x2)
        y2_min = min(y2)
        y2_max = max(y2)
        z2_min = min(z2)
        z2_max = max(z2)

        check_intersections = False
        if (x2_min < x1_max and x1_min < x2_max) and (y2_min < y1_max and y1_min < y2_max) and (z2_min < z1_max and z1_min < z2_max):
            check_intersections = True

        if check_intersections:
            polygon_2 = [None]*n_points_poly_2
            for i in range(0, n_points_poly_2):
                polygon_2[i] = (x2[i], y2[i], z2[i])

            # calculate polygon normal
            p_normal = mathutils.geometry.normal([polygon_2[0], polygon_2[1], polygon_2[2]])

            # find intersection of the polygon plane with the segment
            intersection_point = mathutils.geometry.intersect_line_plane(seg[0], seg[1], polygon_2[0], p_normal)

            if intersection_point is not None:
                # reduce polygon to 2D projection
                reduced_poly_2 = [None]*n_points_poly_2
                for idx, cur_point in enumerate(polygon_2):
                    reduced_poly_2[idx] = cur_point[:max_index] + cur_point[max_index+1:]

                # create path from polygon
                poly_2_path = path.Path(reduced_poly_2)

                # check if line-plane intersection is located within the polygon
                point_in_poly = poly_2_path.contains_points([intersection_point[:max_index] + intersection_point[max_index+1:]])
                check_intersections = point_in_poly[0]
            else:
                check_intersections = False

        if check_intersections:
            seg_intersected_polygons.append({'x': x2, 'y': y2, 'z': z2, 'strike': fr['strike'], 'dip': fr['dip'],
                                             'rgb': fr['rgb'], 'population': fr['population']})
            seg_intersections.append(intersection_point)

    return seg_intersections, seg_intersected_polygons


def plot_segment_polygons_intersections(segment, seg_intersections, n_populations, seg_intersected_polygons, title,
                                        output_path, length_unit, n_figure):

    # get the segment vertices
    p1 = [segment['x'][0], segment['y'][0], segment['z'][0]]
    p2 = [segment['x'][1], segment['y'][1], segment['z'][1]]

    # get a vector of the segment
    b = np.array([p1[0]-p2[0], p1[1]-p2[1], p1[2]-p2[2]])
    l = np.sqrt(b[0]**2+b[1]**2+b[2]**2)

    # distances of individual points points (intersections)
    n_i = len(seg_intersections)
    seg_intersections_dist, seg_intersections_popul, seg_intersections_rgb = [], [], []
    for i in range(len(seg_intersections)):
        p = seg_intersections[i]
        seg_intersections_dist.append(np.sqrt((p1[0]-p[0])**2+(p1[1]-p[1])**2+(p1[2]-p[2])**2))
        seg_intersections_popul.append(seg_intersected_polygons[i]['population'])
        seg_intersections_rgb.append(seg_intersected_polygons[i]['rgb'])

    rgbs, seg_intersections_trans_populs, labels, rgb_to_save, label_to_save = [], [], [], [], []
    for j in range(n_populations):
        x_to_save = []
        for i in range(len(seg_intersections_dist)):
            if j == seg_intersections_popul[i]-1:
                rgb_to_save = seg_intersections_rgb[i]
                x_to_save.append(seg_intersections_dist[i])
                label_to_save = 'P %d' % seg_intersections_popul[i]
        if len(x_to_save) > 0:
            seg_intersections_trans_populs.append(x_to_save)
            rgbs.append(rgb_to_save)
            labels.append(label_to_save)

    fig = plt.figure(n_figure, figsize=[17, 5])
    plt.hist(seg_intersections_trans_populs, bins=int(round(l)-1), range=[0, round(l)-1], density=False, stacked=True,
             color=rgbs, label=labels)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlabel('Depth [%s]' % length_unit)
    plt.ylabel('Number of intersected fractures')
    plt.grid(True)
    fig.canvas.set_window_title(title)
    plt.savefig('%s/%s.png' % (output_path, title))
    # fig.show()
