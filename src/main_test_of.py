#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
from fracturegen import *
from fracturepolygon import *
from polypolyintersect import *
from segpolyintersect import *
from process_csv import *
from vtk_export import *
from remove_polygons import *
from confidence import *
from process_data import *
from interpolation import *
from input_processing import *
from output_file import *
from rve_generation import *
from objective_functions import *
import copy
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)

# input file
input_filename = 'input/692_site.in'


'''
input_arguments = sys.argv
if len(input_arguments) != 2:
    sys.exit('Two arguments must be specified for python: "python main.py input/inputfilename"; main.py is the main '
             'DFraM function and inputfilename is the input file written in ASCII format.')
else:
    input_filename = input_arguments[1]
    print('DFraM launched succesfully.')
'''

# optional switch for histograms on outcrops
plot_histograms_outcrops = True

# secure that maximum density does not exceed a reasonable value
max_density = 3

# optimize fracture density or number of fractures
optimize_density = True

# run several simulations to get statistics when optimize_dfn = True and optimize = False
opt_n_sims = 20

# initial alpha
alpha_1 = 1.5
alpha_2 = 1.6

of_txt = open('of.txt', 'w')

# process input file
[aux_inputs, project_name, save_intersections, x_max, x_min_remove, export_vtk_data, plot_volume_data,
 plot_fracture_size, rock_geometry, terminations, terminate_fractures, length_unit, x_boundary, y_boundary, z_boundary,
 regular_polygons, n_vert, n_figure, n_populations, pop_order, axes_ratio, optimize_dfn] \
    = read_and_process_input_file(input_filename)

if optimize_dfn:
    optimize = aux_inputs['optimize']
    optimized_parameter = aux_inputs['optimized_parameter']
    create_rves = aux_inputs['create_rves']
    min_trace_length_recorded = aux_inputs['min_trace_length_recorded']
    req_confidence = aux_inputs['req_confidence']
    eps_max_lengths = aux_inputs['eps_max_lengths']
    eps_max_density = aux_inputs['eps_max_density']
    max_iter = aux_inputs['max_iter']
    x_min = aux_inputs['x_min']
    outcrop_geometry = aux_inputs['outcrop_geometry']
    outcrop_name = aux_inputs['outcrop_name']
    outcrop_file = aux_inputs['outcrop_file']
    outcrop_weigth = aux_inputs['outcrop_weigth']
    outcrop_area = aux_inputs['outcrop_area']
    borehole_geometry = aux_inputs['borehole_geometry']
    borehole_name = aux_inputs['borehole_name']
    input_outcrops = aux_inputs['input_outcrops']
    alpha_no_optimization = aux_inputs['alpha_no_optimization']
    fr_dens_initial = aux_inputs['fr_dens_initial']
    export_fractures_on_outcrops_data = aux_inputs['export_fractures_on_outcrops_data']
    plot_borehole_data = aux_inputs['plot_borehole_data']
    max_n_simulations = aux_inputs['max_n_simulations']
    plot_outcrop_data = True
else:
    dips_input = aux_inputs['dips_input']
    strikes_input = aux_inputs['strikes_input']
    kappas_input = aux_inputs['kappas_input']
    x_mins = aux_inputs['x_mins_input']
    alphas = aux_inputs['alphas_input']
    fr_densities = aux_inputs['fr_densities_input']
    optimize = False
    create_rves = False
    export_fractures_on_outcrops_data = False
    outcrop_area, outcrop_geometry, outcrop_name, borehole_geometry, borehole_name, rgbs_input = [], [], [], [], [], []
    plot_outcrop_data = False
    plot_borehole_data = False
    export_fractures_on_outcrops_data = False

# initiate output files
[time_of_start, out_path] = create_time_folder(project_name, optimize_dfn)
initiate_output_file(out_path, project_name, time_of_start)
write_output(out_path, 'No input errors detected in %s.' % input_filename)

# advanced initial settings
if optimize_dfn:
    if optimized_parameter == 1:
        min_alpha = alpha_1*1.0
        alpha_trial_2 = alpha_2*1.0
        alpha_trial = [min_alpha, alpha_trial_2]
    elif optimized_parameter == 2:
        x_min_trial = [x_min[0], x_min[0]]
    fr_dens_trial = [fr_dens_initial[0], fr_dens_initial[0]]

# print calculated outcrop data in tabular format - initialization
print_outcrops = False

# get center of outcrops and generate RVEs
if create_rves:
    [rve_sizes, outcrop_centroids] = calculate_outcrop_centroids(outcrop_geometry, x_max)
    [rock_geometries, rock_total_volume] = generate_rves(outcrop_centroids, rve_sizes)
    write_output(out_path, '%d RVEs around outcrops created.' % (len(rock_geometries)))
else:
    rock_geometries = []
    if optimize_dfn:
        write_output(out_path, 'Placing fractures in the entire rock volume (no RVEs around outcrops).')

# initiate input arrays to be optimized
if optimize:
    opt_n_sims = 1
    if optimized_parameter == 1:
        alphas = [alpha_trial[0]]*n_populations
        if len(x_min) > 1:
            x_mins = copy.deepcopy(x_min)
        else:
            x_mins = [x_min[0]]*n_populations
        if len(fr_dens_initial) > 1:
            fr_densities = copy.deepcopy(fr_dens_initial)
        else:
            fr_densities = [fr_dens_initial[0]]*n_populations
    elif optimized_parameter == 2:
        if len(alpha_no_optimization) > 1:
            alphas = copy.deepcopy(alpha_no_optimization)
        else:
            alphas = [alpha_no_optimization[0]]*n_populations
        if len(x_min) > 1:
            x_mins = copy.deepcopy(x_min)
        else:
            x_mins = [x_min_trial[0]]*n_populations
        if len(fr_dens_initial) > 1:
            fr_densities = copy.deepcopy(fr_dens_initial)
        else:
            fr_densities = [fr_dens_initial[0]]*n_populations
elif optimize_dfn:
    if len(alpha_no_optimization) > 1:
        alphas = copy.deepcopy(alpha_no_optimization)
    else:
        alphas = [alpha_no_optimization[0]]*n_populations
    if len(x_min) > 1:
        x_mins = copy.deepcopy(x_min)
    else:
        x_mins = [x_min[0]]*n_populations
    if len(fr_dens_initial) > 1:
        fr_densities = copy.deepcopy(fr_dens_initial)
    else:
        fr_densities = [fr_dens_initial[0]]*n_populations

# create project folders
sim_out_dir = out_path+'/images/simulations'
if not os.path.exists(sim_out_dir):
    os.makedirs(sim_out_dir)

# load and process data from CSV file
# process_csv(filename, project_name, outcrop_title, outcrop_area [m2], outcrop_weight <1; 10>, plot_stereograms,
#             plot_histograms, min_length_of_fractures_to_be_removed, length_unit, order_of_populations, n_figure)
initiate_measurements_file(out_path, project_name, time_of_start)
outcrops_names_obs_all, pops_obs_all, strikes_obs_all, dips_obs_all, lens_obs_all = [], [], [], [], []
for i in range(len(outcrop_area)):
    [observed_outcrop_data, n_figure, outcrop_names_obs_outcrop, pops_obs_outcrop, strikes_obs_outcrop,
     dips_obs_outcrop, lengths_obs_outcrop] = process_csv(outcrop_file[i], out_path, outcrop_name[i], outcrop_area[i],
                                                          outcrop_weigth[i], True, plot_histograms_outcrops,
                                                          min_trace_length_recorded, length_unit, pop_order, n_figure)
    outcrops_names_obs_all = outcrops_names_obs_all+outcrop_names_obs_outcrop
    pops_obs_all = pops_obs_all+pops_obs_outcrop
    strikes_obs_all = strikes_obs_all+strikes_obs_outcrop
    dips_obs_all = dips_obs_all+dips_obs_outcrop
    lens_obs_all = lens_obs_all+lengths_obs_outcrop
    input_outcrops = process_outcrop_data(observed_outcrop_data, input_outcrops, length_unit, out_path)

if optimize_dfn:
    strikes_input, dips_input, kappas_input = calculate_mu_and_kappa(out_path, False, pops_obs_all, strikes_obs_all,
                                                                     dips_obs_all)
    write_output(out_path, '%s/stats_observations.txt created.' % out_path)

if export_fractures_on_outcrops_data:
    write_output(out_path, 'Exporting observed fracture traces for all outcrops to Excel spreadsheet.')
    write_xls_fracture_traces(out_path, 'observations', outcrops_names_obs_all, pops_obs_all, strikes_obs_all,
                              dips_obs_all, lens_obs_all)

# get inputs for the model from outcrop data
if optimize_dfn:
    [rgbs_input, mean_lengths_input, n_fractures_input, dens_fractures_input, outcrop_w_input, mean_lengths_output,
     n_fractures_output, dens_fractures_output] = get_inputs(input_outcrops, n_populations)
else:
    for npop in range(n_populations):
        rgbs_input.append(clrs(npop))

# determine how many optimizations are necessary
if optimize:
    opt_pops = n_populations
    opt_vars = 2
    optimized = True
    if optimized_parameter == 1:
        result_at_alpha = [None]*len(alpha_trial)
    elif optimized_parameter == 2:
        result_at_x_min = [None]*len(x_min_trial)
    result_at_fr_dens = [None]*len(fr_dens_trial)
    write_output(out_path, 'DFN will be optimized, %d populations in total.' % n_populations)
else:
    opt_pops = 1
    opt_vars = 1
    optimized = False
    write_output(out_path, 'DFN will not be optimized.')
    if not optimize and opt_n_sims > 1:
        lens_all_pops = []
        strikes_all_pops = []
        dips_all_pops = []
        outcrops_for_lens_name = []
        outcrops_for_lens_area = []
        outcrops_for_lens_weight = []
        for ii in range(n_populations):
            temp_array_1, temp_array_2, temp_array_3, temp_array_4, temp_array_5, temp_array_6 = [], [], [], [], [], []
            for jj in range(opt_n_sims):
                temp_array_1.append([])
                temp_array_2.append([])
                temp_array_3.append([])
                temp_array_4.append([])
                temp_array_5.append([])
                temp_array_6.append([])
            lens_all_pops.append(temp_array_1)
            strikes_all_pops.append(temp_array_2)
            dips_all_pops.append(temp_array_3)
            outcrops_for_lens_name.append(temp_array_4)
            outcrops_for_lens_area.append(temp_array_5)
            outcrops_for_lens_weight.append(temp_array_6)

for opt_pop in range(opt_pops):  # optimize populations first; from the most dominant to less dominant ones
    for opt_var in range(opt_vars):  # for each population optimize size of fractures (alpha), followed by their density
        iteration_number = 0
        if optimize_dfn:
            eps = 1000
            errors_plot = []
            optimization_counter = 0
            optimization_plot = []
            if opt_var == 0:
                if optimized_parameter == 1 and optimize_dfn:
                    alpha_trial = [min_alpha, alpha_trial_2]
                    if optimize:
                        write_output(out_path, 'Optimizing parameter alpha, population %d.' % (opt_pop+1))
                elif optimized_parameter == 2 and optimize_dfn:
                    if len(x_min) > 1:
                        x_min_trial = [x_min[opt_pop], x_min[opt_pop]]
                    else:
                        x_min_trial = [x_min[0], x_min[0]]
                    if optimize:
                        write_output(out_path, 'Optimizing parameter x_min, population %d.' % (opt_pop+1))
            else:
                if len(fr_dens_initial) > 1:
                    fr_dens_trial = [fr_dens_initial[opt_pop], fr_dens_initial[opt_pop]]
                else:
                    fr_dens_trial = [fr_dens_initial[0], fr_dens_initial[0]]
                if optimize:
                    write_output(out_path, 'Optimizing fracture density (P30), population %d.' % (opt_pop+1))
                    if len(fr_dens_initial) > 1:
                        fr_densities[opt_pop] = fr_dens_initial[opt_pop]
                    else:
                        fr_densities[opt_pop] = fr_dens_initial[0]

        update_variable = True
        while update_variable:
            iteration_number += 1
            if opt_var == 0:
                if optimize:
                    if optimized_parameter == 1:
                        write_output(out_path, 'Optimizing parameter alpha for population %d, iteration no. %d.' %
                                     (opt_pop+1, iteration_number-1))
                    elif optimized_parameter == 2:
                        write_output(out_path, 'Optimizing parameter x_min for population %d, iteration no. %d.' %
                                     (opt_pop+1, iteration_number-1))
            else:
                if optimize:
                    write_output(out_path, 'Optimizing fracture density (P30) for population %d, iteration no. %d.' %
                                         (opt_pop+1, iteration_number-1))

            n_simulations = 0
            if optimize_dfn:
                # prepare arrays for gathering results
                [lengths_outcrop, dens_fractures_outcrop, n_fractures_outcrop] = \
                    prepare_arrays_for_results(outcrop_geometry, n_populations)

            # calculate number of fractures for all populations
            if create_rves:
                if optimize and not terminate_fractures:
                    n_fractures = calculate_n_fractures_rve([fr_densities[opt_pop]], rock_total_volume)
                else:
                    n_fractures = calculate_n_fractures_rve(fr_densities, rock_total_volume)
            else:
                n_fractures = calculate_n_fractures(fr_densities, x_boundary, y_boundary, z_boundary)

            # class Fractures(population, n_fractures, x_range, y_range, z_range, powerlaw constants: [x_min, alpha],
            # aspect_ratio, dummy_var, orientation: [strike_mean, dip_mean, kappa], in_plane_rotation_range,
            # n_poly_range, rgb, rve_geometries)
            if optimize and not terminate_fractures:
                group = [None]
                if optimized_parameter == 1:
                    group[0] = Fractures(opt_pop+1, n_fractures[0], x_boundary[opt_pop], y_boundary[opt_pop],
                                         z_boundary[opt_pop], [x_mins[opt_pop], alphas[opt_pop], x_max], axes_ratio, [0, 1],
                                         [strikes_input[opt_pop], dips_input[opt_pop], kappas_input[opt_pop]], [0, 90],
                                         n_vert, rgbs_input[opt_pop], rock_geometries)
                elif optimized_parameter == 2:
                    group[0] = Fractures(opt_pop+1, n_fractures[0], x_boundary[opt_pop], y_boundary[opt_pop],
                                         z_boundary[opt_pop], [x_mins[opt_pop], alphas[opt_pop], x_max],
                                         axes_ratio, [0, 1],
                                         [strikes_input[opt_pop], dips_input[opt_pop], kappas_input[opt_pop]], [0, 90],
                                         n_vert, rgbs_input[opt_pop], rock_geometries)
            else:
                group = [None]*n_populations
                for g in range(n_populations):
                    if not optimize_dfn:
                        try:
                            group[g] = Fractures(g+1, n_fractures[g], x_boundary[g], y_boundary[g], z_boundary[g],
                                                 [x_mins[g], alphas[g], x_max], axes_ratio, [0, 1],
                                                 [strikes_input[g], dips_input[g], kappas_input[g]], [0, 90], n_vert,
                                                 rgbs_input[g], rock_geometries)
                        except:
                            sys.exit('Not all populations are defined properly, insufficient number of dips, strikes, '
                                     'kappas, x_mins, alphas or fracture densities given!')
                    else:
                        if optimized_parameter == 1:
                            group[g] = Fractures(g+1, n_fractures[g], x_boundary[g], y_boundary[g], z_boundary[g],
                                                 [x_mins[opt_pop], alphas[g], x_max], axes_ratio, [0, 1],
                                                 [strikes_input[g], dips_input[g], kappas_input[g]], [0, 90], n_vert,
                                                 rgbs_input[g], rock_geometries)
                        else:
                            group[g] = Fractures(g+1, n_fractures[g], x_boundary[g], y_boundary[g], z_boundary[g],
                                                 [x_mins[g], alphas[opt_pop], x_max], axes_ratio, [0, 1],
                                                 [strikes_input[g], dips_input[g], kappas_input[g]], [0, 90], n_vert,
                                                 rgbs_input[g], rock_geometries)
            if optimize_dfn:
                if opt_var == 0:
                    if optimized_parameter == 1:
                        optimization_plot.append(alphas[opt_pop])
                    elif optimized_parameter == 2:
                        optimization_plot.append(x_mins[opt_pop])
                else:
                    optimization_plot.append(fr_densities[opt_pop])

            enough_simulations = False
            while not enough_simulations:
                n_simulations += 1
                if optimize:
                    print('Starting simulation number: %d.' % n_simulations)
                tic()

                # method generate_fractures() belongs to Fractures class defined in fracturegen.py
                write_output(out_path, 'Generating fractures (%d in total).' % sum(n_fractures))
                generated_fractures = []
                for g in range(len(group)):
                    generated_fractures = generated_fractures + group[g].generate_fractures()

                # generate individual polygons representing fractures
                polygons = []
                for fr in generated_fractures:
                    [xT, yT, zT, poly_area] = generate_poly(fr['a'], fr['b'], fr['n_points_poly'], fr['x'], fr['y'],
                                                            fr['z'], fr['strike'], fr['dip'], fr['in_plane_rotation'],
                                                            regular_polygons)
                    # save the polygon
                    polygons.append({'xT': xT, 'yT': yT, 'zT': zT, 'strike': fr['strike'], 'dip': fr['dip'],
                                     'rgb': fr['rgb'], 'population': fr['population'], 'intersection': [0],
                                     'area_orig': poly_area, 'area_new': poly_area})

                # termination of generated fractures
                intersections = []  # all intersections of polygons in the rock volume
                if terminate_fractures:
                    write_output(out_path, 'Terminating fractures.')
                    for idx, fr in enumerate(polygons):
                        if save_intersections:
                            [polygons, intersections] = poly_poly_intersections_2(fr, idx, polygons, terminations,
                                                                                  intersections)
                        else:
                            polygons = poly_poly_intersections_2_no_intersections(fr, idx, polygons, terminations)

                        print('Terminating fractures... %.0f%%' % (idx/len(polygons)*100), end='\r')

                    # "intersection" field in polygons contains numbers of intersections,
                    # i.e. polygons[n_polygon]['intersection'] == [0, 10] contains 1st and 11th intersection:
                    # intersections[0] and intersections[10]
                    if save_intersections:
                        write_output(out_path, 'Checking intersections.')
                        [polygons, intersections] = check_intersections(polygons, intersections)
                    write_output(out_path, 'Removing small polygons.')
                    [polygons, intersections] = remove_small_polygons(polygons, intersections, x_min_remove,
                                                                      save_intersections)

                enough_simulations = True
                if optimize_dfn:
                    for idx, outcrop in enumerate(outcrop_geometry):
                        [polygon_2d, intersections_2d,
                         virt_outcrop_area, _] = poly_poly_intersections(outcrop, polygons, min_trace_length_recorded)
                        if optimize and not terminate_fractures:
                            min_pop = opt_pop
                            max_pop = opt_pop+1
                        else:
                            min_pop = 0
                            max_pop = n_populations

                        for current_population in range(min_pop, max_pop):
                            x_2d_start, x_2d_end, y_2d_start, y_2d_end, z_2d_start, z_2d_end, dips_2d, strikes_2d, \
                            outcrop_names_2d, outcrop_areas_2d, outcrop_weights_2d = [], [], [], [], [], [], [], [], \
                                [], [], []
                            for fr in intersections_2d:
                                if fr['population'] == current_population+1:
                                    x_2d_start.append(fr['x'][0])
                                    x_2d_end.append(fr['x'][1])
                                    y_2d_start.append(fr['y'][0])
                                    y_2d_end.append(fr['y'][1])
                                    z_2d_start.append(0)
                                    z_2d_end.append(0)
                                    dips_2d.append(fr['dip'])
                                    strikes_2d.append(fr['strike'])
                                    outcrop_names_2d.append(outcrop_name[idx])
                                    outcrop_areas_2d.append(outcrop_area[idx])
                                    outcrop_weights_2d.append(outcrop_w_input[idx])


                            if len(x_2d_start) > 0:
                                [mean_length_2d, density_fractures_virt_outcrop, n_fractures_virt_outcrop] = \
                                    stats_reduced(x_2d_start, x_2d_end, y_2d_start, y_2d_end, z_2d_start, z_2d_end,
                                                  virt_outcrop_area)

                                dens_fractures_outcrop[idx][current_population].append(density_fractures_virt_outcrop)
                                lengths_outcrop[idx][current_population].append(mean_length_2d)
                                n_fractures_outcrop[idx][current_population].append(n_fractures_virt_outcrop)

                                if not optimize and opt_n_sims > 1:
                                    cur_fractures = get_lengths_of_traces(x_2d_start, x_2d_end, y_2d_start, y_2d_end,
                                                                          z_2d_start, z_2d_end)
                                    for ifr, fracture in enumerate(cur_fractures):
                                        lens_all_pops[current_population][n_simulations-1].append(fracture)
                                        strikes_all_pops[current_population][n_simulations-1].append(strikes_2d[ifr])
                                        dips_all_pops[current_population][n_simulations-1].append(dips_2d[ifr])
                                        outcrops_for_lens_name[current_population][n_simulations-1].append(outcrop_names_2d[ifr])
                                        outcrops_for_lens_area[current_population][n_simulations-1].append(outcrop_areas_2d[ifr])
                                        outcrops_for_lens_weight[current_population][n_simulations-1].append(outcrop_weights_2d[ifr])

                            else:
                                dens_fractures_outcrop[idx][current_population].append(0.0)
                                lengths_outcrop[idx][current_population].append(0.0)
                                n_fractures_outcrop[idx][current_population].append(0.0)

                    # confidence([set of values], required_confidence - 0.9 = 90%)
                    if opt_var == 0:
                        if not confidence2(out_path, 'mean length of fracture traces', current_population, lengths_outcrop,
                                           outcrop_weigth, req_confidence/100, optimize):
                            enough_simulations = False
                    else:
                        if not confidence2(out_path, 'mean density of fracture traces', current_population,
                                           dens_fractures_outcrop, outcrop_weigth, req_confidence/100, optimize):
                            enough_simulations = False

                    if n_simulations == max_n_simulations:
                        enough_simulations = True
                        write_output(out_path, 'Number of simulations exceeds the allowed limit, the optimization will '
                                               'continue regardless the reached confidence level.')

                    if enough_simulations:
                        for current_population in range(n_populations):
                            for idx in range(len(outcrop_geometry)):
                                if len(lengths_outcrop[idx][current_population]) > 0.0:
                                    mean_lengths_output[current_population][idx] = \
                                        np.mean(lengths_outcrop[idx][current_population])
                                    dens_fractures_output[current_population][idx] = \
                                        np.mean(dens_fractures_outcrop[idx][current_population])
                                    n_fractures_output[current_population][idx] = \
                                        np.mean(n_fractures_outcrop[idx][current_population])
                                else:
                                    mean_lengths_output[current_population][idx] = 0.0
                                    dens_fractures_output[current_population][idx] = 0.0
                                    n_fractures_output[current_population][idx] = 0.0
                    if not optimize:
                        if opt_n_sims == n_simulations:
                            enough_simulations = True
                        update_variable = False
                    toc()

                    # set the variable to be investigated
                    opt_var = 1

                    if enough_simulations:
                        if optimize:
                            write_output(out_path, 'Sufficient confidence in results after %d simulations.' % n_simulations)
                            if opt_var == 0:
                                if optimized_parameter == 1:

                                    optimization_counter += 1
                                    of_value, of_derivative, iter_1_value = of_lengths(mean_lengths_input[opt_pop],
                                                                                       mean_lengths_output[opt_pop],
                                                                                       outcrop_w_input)
                                    of_txt.write('%.2f\t%.6f\t%.6f\t' % (alphas[opt_pop], of_value, of_derivative))
                                    for il in range(len(outcrop_w_input)):
                                        if il < len(mean_lengths_input[opt_pop])-1:
                                            of_txt.write('%.6f\t' % (mean_lengths_input[opt_pop][il]))
                                            of_txt.write('%.6f\t' % (mean_lengths_output[opt_pop][il]))
                                        else:
                                            of_txt.write('%.6f\n' % (mean_lengths_input[opt_pop][il]))
                                            of_txt.write('%.6f\n' % (mean_lengths_output[opt_pop][il]))
                                    print('alpha, of_value, of_derivative: %.2f\t%.6f\t%.6f\n' % (alphas[opt_pop], of_value, of_derivative))
                                    alphas[opt_pop] += 0.10
                                    if optimization_counter == 40:
                                        of_txt.close()
                                        sys.exit()

                                elif optimized_parameter == 2:
                                    optimization_counter += 1
                                    of_value, of_derivative, iter_1_value = of_lengths(mean_lengths_input[opt_pop],
                                                                                       mean_lengths_output[opt_pop],
                                                                                       outcrop_w_input)
                                    of_txt.write('%.4f\t%.6f\t%.6f\t' % (x_mins[opt_pop], of_value, of_derivative))
                                    for il in range(len(outcrop_w_input)):
                                        if il < len(mean_lengths_input[opt_pop])-1:
                                            of_txt.write('%.6f\t' % (mean_lengths_input[opt_pop][il]))
                                            of_txt.write('%.6f\t' % (mean_lengths_output[opt_pop][il]))
                                        else:
                                            of_txt.write('%.6f\t' % (mean_lengths_input[opt_pop][il]))
                                            of_txt.write('%.6f\n' % (mean_lengths_output[opt_pop][il]))
                                    print('x_min, of_value, of_derivative: %.2f\t%.6f\t%.6f\n' % (x_mins[opt_pop], of_value, of_derivative))
                                    x_mins[opt_pop] += 0.010
                                    if optimization_counter == 80:
                                        of_txt.close()
                                        sys.exit()
                            else:
                                optimization_counter += 1
                                of_value, of_derivative, iter_1_value = of_densities(dens_fractures_input[opt_pop],
                                                                                     dens_fractures_output[opt_pop],
                                                                                     n_fractures_input[opt_pop],
                                                                                     n_fractures_output[opt_pop],
                                                                                     optimize_density,
                                                                                     outcrop_w_input)
                                of_txt.write('%.6f\t%.6f\t%.6f\t' % (fr_densities[opt_pop], of_value, of_derivative))
                                for il in range(len(outcrop_w_input)):
                                    if il < len(mean_lengths_input[opt_pop])-1:
                                        of_txt.write('%.6f\t' % (dens_fractures_input[opt_pop][il]))
                                        of_txt.write('%.6f\t' % (dens_fractures_output[opt_pop][il]))
                                    else:
                                        of_txt.write('%.6f\t' % (dens_fractures_input[opt_pop][il]))
                                        of_txt.write('%.6f\n' % (dens_fractures_output[opt_pop][il]))
                                print('P30, of_value, of_derivative: %.2f\t%.6f\t%.6f\n' % (x_mins[opt_pop], of_value, of_derivative))
                                fr_densities[opt_pop] += 0.05
                                if optimization_counter == 40:
                                    of_txt.close()
                                    sys.exit()

                                if fr_densities[opt_pop] > max_density:
                                    if len(fr_dens_initial) > 1:
                                        fr_densities[opt_pop] = fr_dens_initial[opt_pop]*1.3
                                    else:
                                        fr_densities[opt_pop] = fr_dens_initial[0]*1.3

                            if opt_pop == n_populations-1 and opt_var == 1:
                                if not update_variable:
                                    update_variable = True
                                    optimize = False
                                    write_output(out_path, 'Generating final network, all parameters optimized.')
                else:
                    update_variable = False

outcrop_intersections_vtk = [None]*len(outcrop_geometry)
if plot_outcrop_data:
    write_output(out_path, 'Plotting outcrop data.')
    write_calculation_header(out_path, project_name, time_of_start)

    strikes_2d_all, dips_2d_all, pops_2d_all, lens_2d_all, outcrops_names_all = [], [], [], [], []
    for idx, outcrop in enumerate(outcrop_geometry):
        if print_outcrops:
            f_outcrop = open(out_path+'/'+outcrop_name[idx]+'.txt', 'w')
            fr_counter = 0
        outcrop_intersections_vtk_temp = []
        [polygon_2d, intersections_2d, virt_outcrop_area,
         outcrop_intersections_3d] = poly_poly_intersections(outcrop, polygons, min_trace_length_recorded)
        plot_polygon_polygons_intersections(polygon_2d, intersections_2d, outcrop_name[idx], sim_out_dir, n_figure)
        n_figure += 1
        for current_population in range(n_populations):
            try:
                outcrop_intersections_vtk_temp_temp = []
                strikes_2d, dips_2d, x_2d_start, x_2d_end, y_2d_start, y_2d_end, z_2d_start, z_2d_end = \
                    [], [], [], [], [], [], [], []
                color_chosen = False
                for fridx, fr in enumerate(intersections_2d):
                    if fr['population'] == current_population+1:
                        if not color_chosen:
                            rgbs_2d = (fr['rgb'])
                            color_chosen = True
                        strikes_2d.append(fr['strike'])
                        dips_2d.append(fr['dip'])
                        x_2d_start.append(fr['x'][0])
                        x_2d_end.append(fr['x'][1])
                        y_2d_start.append(fr['y'][0])
                        y_2d_end.append(fr['y'][1])
                        z_2d_start.append(0)
                        z_2d_end.append(0)

                        x_3d = [outcrop_intersections_3d[fridx]['x'][0], outcrop_intersections_3d[fridx]['x'][1]]
                        y_3d = [outcrop_intersections_3d[fridx]['y'][0], outcrop_intersections_3d[fridx]['y'][1]]
                        z_3d = [outcrop_intersections_3d[fridx]['z'][0], outcrop_intersections_3d[fridx]['z'][1]]
                        outcrop_intersections_vtk_temp_temp.append({'x': x_3d, 'y': y_3d, 'z': z_3d})
                        if print_outcrops:
                            fr_counter += 1
                            new_line = '%d\t%d\t%d\t%d\t%d\t0\t0\t0\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\n' \
                                       % (fr_counter, fr['dip'], fr['strike']+90, fr['strike'], fr['population'],
                                          x_3d[0], y_3d[0], z_3d[0], x_3d[1], y_3d[1], z_3d[1],
                                          np.sqrt((x_3d[1]-x_3d[0])**2+(y_3d[1]-y_3d[0])**2+(z_3d[1]-z_3d[0])**2))
                            f_outcrop.write(new_line)
                outcrop_intersections_vtk_temp.append(outcrop_intersections_vtk_temp_temp)

                [strike_2d_mean, dip_2d_mean, kappa_2d, mean_length_2d, n_fractures_2d, fracture_length_2d,
                 cumulative_fracture_length_2d] = stats2(dips_2d, strikes_2d, x_2d_start, x_2d_end, y_2d_start, y_2d_end,
                                                         z_2d_start, z_2d_end, min_trace_length_recorded)
                fracture_density_P21 = cumulative_fracture_length_2d/virt_outcrop_area
                fracture_density_P20 = n_fractures_2d/virt_outcrop_area

                title = 'Simulations, fracture population no. %d' % (current_population+1)
                output_file = '%s/histogram-%s-population-%d.png' % (sim_out_dir, outcrop_name[idx], current_population+1)
                output_file_2 = '%s/logplot-%s-population-%d.png' % (sim_out_dir, outcrop_name[idx], current_population+1)
                hist_plots(fracture_length_2d, 15, rgbs_2d, title, 'Trace length', length_unit,
                           'Number of fractures [-]', output_file, n_figure)
                n_figure += 1
                log_plots(fracture_length_2d, rgbs_2d, title, length_unit, output_file_2, n_figure)
                n_figure += 1

                if not optimize:
                    write_calculation_results(out_path, current_population, dip_2d_mean, strike_2d_mean, kappa_2d,
                                              mean_length_2d, length_unit, fracture_density_P21, x_2d_start,
                                              outcrop_name[idx], fracture_density_P20)
            except:
                write_output(out_path, 'There are no traces of population %d on outcrop %s.'
                             % (current_population+1, outcrop_name[idx]))

        outcrop_intersections_vtk[idx] = outcrop_intersections_vtk_temp
        if print_outcrops:
            f_outcrop.close()

        # plot stereograms on outcrop, function stereo_plots(...) defined in stereoplots.py
        strikes_2d, dips_2d, pops_2d, lens_2d, rgbs_2d, outcrops_names = [], [], [], [], [], []
        for fr in intersections_2d:
            strikes_2d.append(fr['strike'])
            dips_2d.append(fr['dip'])
            pops_2d.append(fr['population'])
            lens_2d.append((fr['length']))
            rgbs_2d.append(fr['rgb'])
            outcrops_names.append(outcrop_name[idx])
        stereo_plots(strikes_2d, dips_2d, rgbs_2d, outcrop_name[idx], sim_out_dir, n_figure)
        n_figure += 3

        strikes_2d_all = strikes_2d_all+strikes_2d
        dips_2d_all = dips_2d_all+dips_2d
        pops_2d_all = pops_2d_all+pops_2d
        lens_2d_all = lens_2d_all+lens_2d
        outcrops_names_all = outcrops_names_all+outcrops_names

    strikes_output, dips_output, kappas_output = calculate_mu_and_kappa(out_path, True, pops_2d_all, strikes_2d_all,
                                                                        dips_2d_all)

if export_vtk_data:
    write_output(out_path, 'Exporting VTK files.')
    vtk_export(polygons, intersections, rock_geometry, outcrop_geometry, outcrop_name, borehole_geometry, borehole_name,
               outcrop_intersections_vtk, out_path+'/vtk')

if export_fractures_on_outcrops_data:
    if len(outcrop_name) > 0:
        write_output(out_path, 'Exporting simulated fracture traces for all outcrops to Excel spreadsheet, '
                               'plotting histograms of fracture trace lengths and stereograms.')
        write_xls_fracture_traces(out_path, 'simulations', outcrops_names_all, pops_2d_all, strikes_2d_all, dips_2d_all,
                                  lens_2d_all)
        fracture_traces_all_outcrops_histograms(out_path, int(n_populations), pops_obs_all, pops_2d_all, lens_obs_all,
                                                lens_2d_all, length_unit, min_trace_length_recorded, n_figure)
        n_figure += n_populations
        log_log_plots(out_path, int(n_populations), pops_obs_all, pops_2d_all, lens_obs_all, lens_2d_all, length_unit,
                      min_trace_length_recorded, n_figure)
        n_figure += n_populations
        stereo_plots_populations(out_path, int(n_populations), pops_obs_all, pops_2d_all, dips_obs_all, strikes_obs_all,
                                 dips_2d_all, strikes_2d_all, rgbs_input, n_figure)
        n_figure += 2*n_populations
        if opt_n_sims > 1:
            fracture_traces_all_outcrops_histograms_and_loglog_more_simulations(out_path, n_populations, pops_obs_all,
                                                                                lens_obs_all, lens_all_pops, 
                                                                                strikes_all_pops, dips_all_pops,
                                                                                outcrops_for_lens_name, 
                                                                                outcrops_for_lens_area,
                                                                                outcrops_for_lens_weight, length_unit,
                                                                                min_trace_length_recorded, n_figure)            
            n_figure += 2*n_populations
    else:
        write_output(out_path, 'No outcrops detected.')

if plot_fracture_size:
    write_output(out_path, 'Equivalent fracture size for individual populations, before and after intersecting:')
    for current_population in range(n_populations):
        size_of_fracture_orig, size_of_fracture_new = [], []
        color_chosen = False
        for fr in polygons:
            if fr['population'] == current_population+1:
                if not color_chosen:
                    rgbs_3d = (fr['rgb'])
                    color_chosen = True
                if fr['area_new'] > 0:
                    size_of_fracture_orig.append(np.sqrt(fr['area_orig']/3.14159))
                    size_of_fracture_new.append(np.sqrt(fr['area_new']/3.14159))

        [x_min_old, alpha_old] = get_powerlaw_constants(size_of_fracture_orig)
        [x_min_new, alpha_new] = get_powerlaw_constants(size_of_fracture_new)
        write_output(out_path, 'x_min old, population %d: %.4f' % (current_population+1, x_min_old))
        write_output(out_path, 'alpha old, population %d: %.4f' % (current_population+1, alpha_old))
        write_output(out_path, 'x_min new, population %d: %.4f' % (current_population+1, x_min_new))
        write_output(out_path, 'alpha new, population %d: %.4f' % (current_population+1, alpha_new))
        print('\n')

        title = 'Fracture length (before intersections), population no. %d' % (current_population+1)
        output_file = '%s/histograms_fracture_length-1_in_volume-population-%d-before_intersections.png' \
                      % (sim_out_dir, current_population+1)
        output_file_2 = '%s/logplot_fracture_length-1_in_volume-population-%d-before_intersections.png' \
                        % (sim_out_dir, current_population+1)
        hist_plots_with_range(size_of_fracture_orig, 20, [x_mins[current_population], x_max], rgbs_3d, title,
                              'Equivalent fracture length - before terminations, sqrt(area/pi)', length_unit,
                              'Number of fractures [-]', output_file, n_figure)

        n_figure += 1
        log_plots(size_of_fracture_orig, rgbs_3d, title, length_unit, output_file_2, n_figure)
        n_figure += 1

        title = 'Fracture length (after intersections), population no. %d' % (current_population+1)
        output_file = '%s/histograms_fracture_length-2_in_volume-population-%d-after_intersections.png' \
                      % (sim_out_dir, current_population+1)
        output_file_2 = '%s/logplot_fracture_length-2_in_volume-population-%d-after_intersections.png' \
                        % (sim_out_dir, current_population+1)
        hist_plots_with_range(size_of_fracture_new, 20, [x_mins[current_population], x_max], rgbs_3d, title,
                              'Equivalent fracture length - after terminations, sqrt(area/pi)', length_unit,
                              'Number of fractures [-]', output_file, n_figure)
        n_figure += 1
        log_plots(size_of_fracture_new, rgbs_3d, title, length_unit, output_file_2, n_figure)
        n_figure += 1

if plot_borehole_data:
    write_output(out_path, 'Plotting borehole data.')
    for idx, borehole in enumerate(borehole_geometry):
        [seg_intersections, seg_intersected_polygons] = segment_polygons_intersections(borehole, polygons)
        if len(seg_intersections) > 0:
            plot_segment_polygons_intersections(borehole, seg_intersections, n_populations, seg_intersected_polygons,
                                                borehole_name[idx], sim_out_dir, length_unit, n_figure)
            n_figure += 1

            # plot stereograms for borehole, function stereo_plots(...) defined in stereoplots.py
            strikes_1d, dips_1d, rgbs_1d = [], [], []
            for fr in seg_intersected_polygons:
                strikes_1d.append(fr['strike'])
                dips_1d.append(fr['dip'])
                rgbs_1d.append(fr['rgb'])
            stereo_plots(strikes_1d, dips_1d, rgbs_1d, borehole_name[idx], sim_out_dir, n_figure)
            n_figure += 3
        else:
            write_output(out_path, 'No borehole %s intersections.' % borehole_name[idx])

if not plot_outcrop_data:
    outcrop_geometry = []
if not plot_borehole_data:
    borehole_geometry = []

if plot_volume_data:
    write_output(out_path, 'Plotting all fractures in the rock volume.')
    plot_fracture_3d(polygons, rock_geometry, 0.7, outcrop_geometry, borehole_geometry, 'Fractures in rock volume',
                     sim_out_dir + '/Rock-volume-all_fractures.png', n_figure)
    n_figure += 1
    if optimize_dfn:
        write_output(out_path, 'Plotting volume stereograms.')
        strikes_3d, dips_3d, rgbs_3d = [], [], []
        for fr in polygons:
            strikes_3d.append(fr['strike'])
            dips_3d.append(fr['dip'])
            rgbs_3d.append(fr['rgb'])
        stereo_plots(strikes_3d, dips_3d, rgbs_3d, 'Rock volume', sim_out_dir, n_figure)
        n_figure += 3

if optimize_dfn:
    counter_fractures_in_pops = np.zeros(n_populations)
    percentages = np.zeros(n_populations)
    for pop in pops_obs_all:
        counter_fractures_in_pops[int(pop-1)] += 1
    total = np.sum(counter_fractures_in_pops)
    percentages = counter_fractures_in_pops/total*100

    write_optimization_results(out_path, fr_densities, x_mins, alphas, strikes_input, dips_input, kappas_input,
                               percentages, rgbs_input)
write_output(out_path, 'Finished!')

# plot all figures from the script
# if n_figure > 1:
#    plt.show()
