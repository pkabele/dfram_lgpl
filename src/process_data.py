#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
import numpy as np
from output_file import write_meas_output


def calculate_n_fractures(fracture_density, x_boundary, y_boundary, z_boundary):
    n_populations = len(fracture_density)
    n_fractures = [None]*n_populations
    for i in range(n_populations):
        vol = (x_boundary[i][1]-x_boundary[i][0])*(y_boundary[i][1]-y_boundary[i][0])*(z_boundary[i][1]-z_boundary[i][0])
        n_fractures[i] = int(round(fracture_density[i]*vol))
    return n_fractures


def calculate_n_fractures_rve(fracture_density, vol):
    n_populations = len(fracture_density)
    n_fractures = [None]*n_populations
    for i in range(n_populations):
        n_fractures[i] = int(round(fracture_density[i]*vol))
    return n_fractures


def prepare_arrays_for_results(outcrop_geometry, n_populations):

    lengths_outcrop, n_fractures_outcrop, density_fractures_outcrop = [], [], []
    for idx in range(len(outcrop_geometry)):
        lengths_outcrop.append([])
        n_fractures_outcrop.append([])
        density_fractures_outcrop.append([])
    for idx in range(len(outcrop_geometry)):
        for current_population in range(n_populations):
            lengths_outcrop[idx].append([])
            n_fractures_outcrop[idx].append([])
            density_fractures_outcrop[idx].append([])

    return lengths_outcrop, density_fractures_outcrop, n_fractures_outcrop


def process_outcrop_data(observed_outcrop_data, input_outcrops, length_unit, out_path):

    dips_input, strikes_input, kappas_input, rgbs_input = [], [], [], []
    for cur_pop, popul in enumerate(observed_outcrop_data):
        dips_input.append(popul['dips_mean'])
        strikes_input.append(popul['strikes_mean'])
        kappas_input.append(popul['kappa'])
        rgbs_input.append(popul['rgb'])

    n_fractures_outcrop = np.zeros((len(observed_outcrop_data), 1))
    lengths_outcrop = np.zeros((len(observed_outcrop_data), 1))
    dens_fractures_outcrop = np.zeros((len(observed_outcrop_data), 1))
    outcrop_weigth = observed_outcrop_data[0]['outcrop_weight']

    f_out = open(out_path+'/stats_observations.txt', 'a')
    for cur_pop, popul in enumerate(observed_outcrop_data):
        if popul['n_fractures'] > 0:
            write_meas_output(out_path,
                              'Statistics for field measurements on outcrop %s and '
                              'population %d:' % (popul['outcrop_title'], cur_pop+1))
            write_meas_output(out_path, 'Mean length of fractures: %.3f %s' % (popul['mean_length'], length_unit))
            write_meas_output(out_path, 'Fracture density (P21): %.3f %s-1' % (popul['fracture_density'], length_unit))
            write_meas_output(out_path, 'Fracture density (P20): %.3f %s-2' % (popul['dens_fractures'], length_unit))
            write_meas_output(out_path, 'Number of fractures: %d \n' % popul['n_fractures'])

            n_fractures_outcrop[cur_pop] = popul['n_fractures']
            dens_fractures_outcrop[cur_pop] = popul['dens_fractures']
            lengths_outcrop[cur_pop] = popul['mean_length']
        else:
            n_fractures_outcrop[cur_pop] = 0.0
            lengths_outcrop[cur_pop] = 0.0
            dens_fractures_outcrop[cur_pop] = 0.0
    f_out.close()

    input_outcrops.append({'mean_lengths': lengths_outcrop, 'n_fractures': n_fractures_outcrop,
                           'dens_fractures': dens_fractures_outcrop, 'outcrop_weight': outcrop_weigth,
                           'dips': dips_input, 'strikes': strikes_input,
                           'kappas': kappas_input, 'rgbs': rgbs_input})

    return input_outcrops


def get_inputs(input_outcrops, n_pops):

    n_outcrops = len(input_outcrops)

    # initialize output arrays
    rgbs_input = [0.0]*n_pops
    mean_lengths_input = [0.0]*n_pops
    n_fractures_input = [0.0]*n_pops
    dens_fractures_input = [0.0]*n_pops
    mean_lengths_output = [0.0]*n_pops
    n_fractures_output = [0.0]*n_pops
    dens_fractures_output = [0.0]*n_pops

    for i in range(n_pops):
        for j in range(n_outcrops):
            if len(input_outcrops[j]['dips']) > i:
                if len(input_outcrops[j]['rgbs'][i]) == 3:
                    rgbs_input[i] = input_outcrops[j]['rgbs'][i]

        mean_lengths = [None]*n_outcrops
        n_fractures = [None]*n_outcrops
        dens_fractures = [None]*n_outcrops
        mean_lengths_out = [None]*n_outcrops
        n_fractures_out = [None]*n_outcrops
        dens_fractures_out = [None]*n_outcrops
        for j in range(n_outcrops):
            if len(input_outcrops[j]['dips']) > i:
                mean_lengths[j] = input_outcrops[j]['mean_lengths'][i][0]
                n_fractures[j] = input_outcrops[j]['n_fractures'][i][0]
                dens_fractures[j] = input_outcrops[j]['dens_fractures'][i][0]
            else:
                mean_lengths[j] = 0.0
                n_fractures[j] = 0.0
                dens_fractures[j] = 0.0

        mean_lengths_input[i] = mean_lengths
        n_fractures_input[i] = n_fractures
        dens_fractures_input[i] = dens_fractures
        mean_lengths_output[i] = mean_lengths_out
        n_fractures_output[i] = n_fractures_out
        dens_fractures_output[i] = dens_fractures_out

    outcrop_w_input = [None]*n_outcrops
    for j in range(n_outcrops):
        outcrop_w_input[j] = (input_outcrops[j]['outcrop_weight'])

    return rgbs_input, mean_lengths_input, n_fractures_input, dens_fractures_input, outcrop_w_input, \
           mean_lengths_output, n_fractures_output, dens_fractures_output
