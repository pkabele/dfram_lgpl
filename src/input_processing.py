#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#

from polypolyintersect import area_by_tri
import sys
import numpy as np
from process_spreadsheets import *
import os


def get_revision_id():
# For Git (.gitattributes must be properly set up)
    revision_text = "$Format:%D$ $Format:%ci$ $Format:%aN$ ($Format:%h$)"
# The code below is for $Id$ from SVN.
#    paths = os.listdir(os.getcwd())
#    paths.sort()
#    valid_extensions = ['.py']
#    latest_revision = 0
#    revision_text = ''
#    for file in paths:
#        ext = os.path.splitext(file)[1]
#        if ext.lower() in valid_extensions:
#            try:
#                f = open(file, 'r')
#            except OSError:
#                print('Cannot open %s.' % file)
#                sys.exit('The DFN calculation terminated.')
#            else:
#                for line in f:
#                    if '@revision' in line and not 'if' in line:
#                        revision_number = [int(s) for s in line.split() if s.isdigit()]
#                        try:
#                            if revision_number[0] > latest_revision:
#                                latest_revision = revision_number[0]
#                                revision_text = line[:]
#                        except:
#                            print('test')
    print('Last revision: ', revision_text)
    return revision_text


def str2bool(v):
    return v.lower() in ('True', 'true', 'yes', '1')


def read_line(line, text_name, line_used):
    read_this_line = False
    if len(line) > 1:
        if line[0] != '#' and line[1] != '#':
            if not line_used:
                if text_name in line:
                    read_this_line = True
    return read_this_line


def get_variable(line, numeric, logic):
        line_split = line.split(':', maxsplit=1)
        value = line_split[1]
        value = value.strip()
        if numeric:
            value = float(value)
        elif logic:
            value = str2bool(value)
        return value, True


def get_array(line):
    line_split = line.split(':', maxsplit=1)
    values = line_split[1]
    values = values.split(',')
    for idx, val in enumerate(values):
        val = val.strip()
        values[idx] = float(val)
    return values, True


def get_array_or_string(line):
    line_split = line.split(':', maxsplit=1)
    values = line_split[1]
    values = values.split(',')
    for idx, val in enumerate(values):
        val = val.strip()
        try:
            values[idx] = float(val)
        except ValueError:
            values[idx] = str(val)
    return values, True


def read_and_process_input_file(input_filename):

    '''
    outcrop_geometry_x = []
    outcrop_geometry_y = []
    outcrop_geometry_z = []
    '''
    outcrop_name = []
    outcrop_file = []
    borehole_geometry_x = []
    borehole_geometry_y = []
    borehole_geometry_z = []
    borehole_name = []
    x_boundary = []
    y_boundary = []
    z_boundary = []

    try:
        f = open(input_filename, 'r')
    except OSError:
        print('Cannot open %s.' % input_filename)
        sys.exit('The DFN calculation terminated.')
    else:
        print('Reading input file: %s.' % input_filename)

    optimization_or_rve = 1.0  # default - optimization
    for idx, line in enumerate(f.readlines()):
        line_used = False

        # task
        text_name = 'Task:'
        numeric = False
        logic = False
        if read_line(line, text_name, line_used):
            [task, line_used] = get_variable(line, numeric, logic)
            if 'DFN optimization' in task:
                optimization_or_rve = 1.0
            elif 'DFN generation' in task:
                optimization_or_rve = 2.0
            elif 'DFN verification' in task:
                optimization_or_rve = 3.0
            else:
                sys.exit('Invalid input for "Task".')

        # project name
        text_name = 'Project name:'                                        # set a name of the input
        numeric = False                                                    # set whether the input is a number or string
        logic = False                                                      # set whether the input is logical or not
        if read_line(line, text_name, line_used):
            [project_name, line_used] = get_variable(line, numeric, logic)  # set the name of the input variable

        if (optimization_or_rve == 1.0) or (optimization_or_rve == 3.0):  # optimization or verification
            # outcrops
            text_name = 'Outcrop name:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [outcrop_name_temp, line_used] = get_variable(line, numeric, logic)
                outcrop_name.append(outcrop_name_temp)

            text_name = 'Outcrop field measurement data:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [outcrop_file_temp, line_used] = get_variable(line, numeric, logic)
                outcrop_file.append(outcrop_file_temp)

            text_name = 'Outcrop relevance data:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [outcrop_relevance_file, line_used] = get_variable(line, numeric, logic)
                outcrop_relevance = process_input_relevance(outcrop_relevance_file)

        if optimization_or_rve != 3.0:  # optimization or generation

            # set project unit
            text_name = 'Length unit:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [length_unit, line_used] = get_variable(line, numeric, logic)

            # saving fracture intersections
            text_name = 'Save intersections [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [save_intersections, line_used] = get_variable(line, numeric, logic)

            # terminating fractures
            text_name = 'Terminate fractures [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [terminate_fractures, line_used] = get_variable(line, numeric, logic)

            # fractures as regular polygons
            text_name = 'Fractures as regular polygons [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [regular_polygons, line_used] = get_variable(line, numeric, logic)

            # number of polygon vertices
            text_name = 'Number of polygon vertices (interval of possible values):'
            if read_line(line, text_name, line_used):
                [n_poly_vertices, line_used] = get_array(line)

            # number of polygon vertices
            text_name = 'Minor to major axis length ratio (interval of possible values):'
            if read_line(line, text_name, line_used):
                [axes_ratio, line_used] = get_array(line)

            # powerlaw x_max
            text_name = 'Largest generated fracture major axis (x_max):'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [x_max, line_used] = get_variable(line, numeric, logic)

            # removing small fractures
            text_name = 'Minimum equivalent size of fractures not to be removed from DFN:'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [x_min_remove, line_used] = get_variable(line, numeric, logic)

            # exporting vtk data
            text_name = 'Export vtk files [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [export_vtk_data, line_used] = get_variable(line, numeric, logic)

            '''
            # exporting volume data
            text_name = 'Export figure with fractures within the rock volume [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [plot_volume_data, line_used] = get_variable(line, numeric, logic)
            '''

            # exporting fracture size histograms
            text_name = 'Export size distribution histograms for all fractures [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [plot_fracture_size, line_used] = get_variable(line, numeric, logic)

            # plotting histograms for all outcrops
            text_name = 'Export histograms of trace lengths for each outcrop [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [plot_histograms_outcrops, line_used] = get_variable(line, numeric, logic)

            # rock position
            text_name = 'Rock position, west-east interval (x):'
            if read_line(line, text_name, line_used):
                [rock_x, line_used] = get_array(line)
            text_name = 'Rock position, south-north interval (y):'
            if read_line(line, text_name, line_used):
                [rock_y, line_used] = get_array(line)
            text_name = 'Rock position, depth interval (z):'
            if read_line(line, text_name, line_used):
                [rock_z, line_used] = get_array(line)

            # distribution of fracutre within the rock volume
            text_name = 'All fractures distributed uniformly in the entire rock volume [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [hom_distribution, line_used] = get_variable(line, numeric, logic)

            # populations - order and terminations
            text_name = 'Fracture terminations data:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [termination_file, line_used] = get_variable(line, numeric, logic)
                pop_order, terminations = process_input_termination(termination_file)

            # populations - transmissivities
            text_name = 'Fracture transmissivity data:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [transmissivity_file, line_used] = get_variable(line, numeric, logic)
                transmissivities = process_input_transmissivity(transmissivity_file)

            # populations - boundaries
            text_name = 'Population boundaries, west-east interval (x):'
            if read_line(line, text_name, line_used):
                [x_boundary_temp, line_used] = get_array(line)
                x_boundary.append(x_boundary_temp)
            text_name = 'Population boundaries, south-north interval (y):'
            if read_line(line, text_name, line_used):
                [y_boundary_temp, line_used] = get_array(line)
                y_boundary.append(y_boundary_temp)
            text_name = 'Population boundaries, depth interval (z):'
            if read_line(line, text_name, line_used):
                [z_boundary_temp, line_used] = get_array(line)
                z_boundary.append(z_boundary_temp)

        if optimization_or_rve == 1.0:  # optimization

            # DFN optimization
            text_name = 'Optimize DFN [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [optimize, line_used] = get_variable(line, numeric, logic)

            # optimized populations
            text_name = 'Populations to be optimized (sequence of integers or "all"):'
            if read_line(line, text_name, line_used):
                [pops_to_optimize, line_used] = get_array_or_string(line)

            # directional statistics only?
            text_name = 'Calculate only directional statistics [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [directional_stats_only, line_used] = get_variable(line, numeric, logic)

            # Number of simulations if not optimized DFN
            text_name = 'Number of simulations:'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [opt_n_sims, line_used] = get_variable(line, numeric, logic)

            # parameter to optimize
            text_name = 'Optimized parameter (1 - alpha, 2 - x_min):'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [optimized_parameter, line_used] = get_variable(line, numeric, logic)

            # Objective function for P30 estimation
            text_name = 'Selected OF for P30 (1 - based on fracture densities on outcrops (P'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [of_p30, line_used] = get_variable(line, numeric, logic)

            # create rve option
            text_name = 'Create RVEs around outcrops [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [create_rves, line_used] = get_variable(line, numeric, logic)

            # minimum recorded fracture trace size
            text_name = 'Minimum recorded size of fracture traces on virtual outcrops:'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [min_trace_length_recorded, line_used] = get_variable(line, numeric, logic)

            # minimum required confidence
            text_name = 'Required confidence in mean fracture trace size and density [%]:'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [req_confidence, line_used] = get_variable(line, numeric, logic)

            # maximum number of simulations to gain required confidence
            text_name = 'Maximum number of simulations to obtain mean parameters with required confidence:'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [max_n_simulations, line_used] = get_variable(line, numeric, logic)

            # maximum admissible error in fracture sizes
            text_name = 'Maximum size of a relative iteration step for fracture trace lengths [%]:'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [eps_max_lengths, line_used] = get_variable(line, numeric, logic)

            # maximum admissible error in fracture density
            text_name = 'Maximum size of a relative iteration step for fracture densities P20 [%]:'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [eps_max_density, line_used] = get_variable(line, numeric, logic)

            # maximum number of iterations
            text_name = 'Maximum number of iterations when optimizing size of fractures and fracture densities:'
            numeric = True
            logic = False
            if read_line(line, text_name, line_used):
                [max_iter, line_used] = get_variable(line, numeric, logic)
                if max_iter < 3:
                    max_iter = 3
                    print('The maximum number of iterations must be higher than 2, changed to %d.' % max_iter)

            # powerlaw x_min
            text_name = 'Smallest generated fracture major axis x_min (can be a single number or for each population separately):'
            if read_line(line, text_name, line_used):
                [x_min, line_used] = get_array(line)

            '''
            # exporting xls with fracture data on outcrops
            text_name = 'Export xls with data about fractures on outcrops [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [export_fractures_on_outcrops_data, line_used] = get_variable(line, numeric, logic)
            '''

            # exporting borehole data
            text_name = 'Export virtual borehole data [yes/no]:'
            numeric = False
            logic = True
            if read_line(line, text_name, line_used):
                [plot_borehole_data, line_used] = get_variable(line, numeric, logic)

            # boreholes
            text_name = 'Borehole name:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [borehole_name_temp, line_used] = get_variable(line, numeric, logic)
                borehole_name.append(borehole_name_temp)
            text_name = 'Borehole segment points, x cooridinates:'
            if read_line(line, text_name, line_used):
                [borehole_geometry_x_temp, line_used] = get_array(line)
                borehole_geometry_x.append(borehole_geometry_x_temp)
            text_name = 'Borehole segment points, y cooridinates:'
            if read_line(line, text_name, line_used):
                [borehole_geometry_y_temp, line_used] = get_array(line)
                borehole_geometry_y.append(borehole_geometry_y_temp)
            text_name = 'Borehole segment points, z cooridinates:'
            if read_line(line, text_name, line_used):
                [borehole_geometry_z_temp, line_used] = get_array(line)
                borehole_geometry_z.append(borehole_geometry_z_temp)

            # alpha settings
            text_name = 'Power-law decay parameter, starting/fixed value(s) alpha_0 (can be a single number or for each population separately):'
            if read_line(line, text_name, line_used):
                [alpha_0, line_used] = get_array(line)

            text_name = 'Power-law decay parameter, second optimization value value(s) alpha_1 (can be a single number or for each population separately):'
            if read_line(line, text_name, line_used):
                [alpha_1, line_used] = get_array(line)

            # fracture density
            text_name = 'Initial fracture density P30 (can be a single number or for each population separately):'
            if read_line(line, text_name, line_used):
                [fr_dens_initial, line_used] = get_array(line)

            if not line_used and len(line.strip()) > 0:
                if line[0] != '#' and line[1] != '#':
                    print('Unknown input on line %d: %s' % (idx+1, line))

        if optimization_or_rve == 2.0:  # generation

            # read prescribed dips
            text_name = 'Dips:'
            if read_line(line, text_name, line_used):
                [dips_input, line_used] = get_array(line)

            # read prescribed strikes
            text_name = 'Strikes:'
            if read_line(line, text_name, line_used):
                [strikes_input, line_used] = get_array(line)

            # read prescribed kappas
            text_name = 'Concentration parameters, kappa:'
            if read_line(line, text_name, line_used):
                [kappas_input, line_used] = get_array(line)

            # read prescribed x_mins
            text_name = 'Powerlaw minimum fracture sizes, x_min:'
            if read_line(line, text_name, line_used):
                [x_mins_input, line_used] = get_array(line)

            # read prescribed alphas
            text_name = 'Powerlaw decay parameters, alpha:'
            if read_line(line, text_name, line_used):
                [alphas_input, line_used] = get_array(line)

            # read prescribed fracture densities
            text_name = 'Fracture densities, P30:'
            if read_line(line, text_name, line_used):
                [fr_densities_input, line_used] = get_array(line)

        if optimization_or_rve == 3.0:  # verification

            text_name = 'Folder with results for verification:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [folder_for_verification, line_used] = get_variable(line, numeric, logic)

            text_name = 'Site name:'
            numeric = False
            logic = False
            if read_line(line, text_name, line_used):
                [site_name, line_used] = get_variable(line, numeric, logic)

        f.close()

    # create dictionaries and lists of arrays
    if optimization_or_rve != 3.0:  # optimization or generation
        rock_geometry = {'x': rock_x, 'y': rock_y, 'z': rock_z}
    outcrop_geometry = []
    borehole_geometry = []
    outcrop_area = []
    for file in outcrop_file:
        verts = process_input_outcrops(file)
        x_v, y_v, z_v = [], [], []
        for vert in verts:
            x_v.append(vert[0])
            y_v.append(vert[1])
            z_v.append(vert[2])
        outcrop_geometry.append({'x': x_v, 'y': y_v, 'z': z_v})
        outcrop_area_temp, _ = area_by_tri(verts)
        outcrop_area.append(outcrop_area_temp)
    for i in range(len(borehole_geometry_x)):
        borehole_geometry.append({'x': borehole_geometry_x[i], 'y': borehole_geometry_y[i], 'z': borehole_geometry_z[i]})

    # check
    if len(borehole_geometry_x) == 0:
        plot_borehole_data = False

    # initiate counters and variables
    n_figure = 1
    input_outcrops = []

    # distribute fractures
    if optimization_or_rve != 3.0:  # optimization or generation
        n_populations = len(terminations)
        if hom_distribution:
            for pop in range(n_populations):
                x_boundary[pop][0] = rock_geometry['x'][0]
                x_boundary[pop][1] = rock_geometry['x'][1]
                y_boundary[pop][0] = rock_geometry['y'][0]
                y_boundary[pop][1] = rock_geometry['y'][1]
                z_boundary[pop][0] = rock_geometry['z'][0]
                z_boundary[pop][1] = rock_geometry['z'][1]

    if optimization_or_rve == 1.0:  # optimization
        '''
        for i in range(len(alpha_0)):
            if alpha_1[i] <= alpha_0[i]:
                alpha_1[i] = 1.1 * alpha_0[i]
                print("Alpha_0[%d] cannot be more or equal to alpha_0[%d], alpha_0[%d] changed to %.4f."
                      % (i, i, i, alpha_1))
        '''
        if len(alpha_0) < n_populations:
            alpha_0 = [alpha_0[0]] * n_populations
        if len(alpha_1) < n_populations:
            alpha_1 = [alpha_1[0]] * n_populations

        aux_inputs = {'optimize': optimize, 'optimized_parameter': optimized_parameter, 'create_rves': create_rves,
                      'min_trace_length_recorded': min_trace_length_recorded, 'req_confidence': req_confidence,
                      'eps_max_lengths': eps_max_lengths, 'eps_max_density': eps_max_density, 'max_iter': max_iter,
                      'x_min': x_min, 'outcrop_geometry': outcrop_geometry, 'outcrop_name': outcrop_name,
                      'outcrop_file': outcrop_file, 'outcrop_relevance': outcrop_relevance,
                      'outcrop_area': outcrop_area, 'borehole_geometry': borehole_geometry,
                      'borehole_name': borehole_name, 'input_outcrops': input_outcrops,
                      'alphas_01': [alpha_0, alpha_1], 'fr_dens_initial': fr_dens_initial,
                      'plot_borehole_data': plot_borehole_data, 'max_n_simulations': max_n_simulations,
                      'OF_P30': of_p30, 'opt_n_sims': opt_n_sims, 'directional_stats_only': directional_stats_only,
                      'plot_histograms_outcrops': plot_histograms_outcrops, 'pops_to_optimize': pops_to_optimize}
        optimize_dfn = True
    elif optimization_or_rve == 2.0:  # generation
        aux_inputs = {'dips_input': dips_input, 'strikes_input': strikes_input, 'kappas_input': kappas_input,
                      'x_mins_input': x_mins_input, 'alphas_input': alphas_input,
                      'fr_densities_input': fr_densities_input}
        optimize_dfn = False
    else:  # verification
        aux_inputs = {'outcrop_name': outcrop_name, 'outcrop_file': outcrop_file, 'outcrop_relevance': outcrop_relevance,
                      'outcrop_area': outcrop_area, 'verify_dir': folder_for_verification, 'site_name': site_name}
        optimize_dfn = False
        save_intersections, terminate_fractures, regular_polygons, n_poly_vertices, axes_ratio, x_max, x_min_remove, \
        export_vtk_data, plot_fracture_size, plot_histograms_outcrops, pop_order, terminations, \
        transmissivities, n_populations = None, None, None, None, None, None, None, None, None, None, None, None, None, \
                                          None
        rock_geometry = []
        length_unit = ' '

    try:
        if optimize_dfn:
            if optimized_parameter < 1 or optimized_parameter > 2:
                sys.exit('Invalid input for "Optimized parameter", it can be equal to 1 or 2.')

        return aux_inputs, project_name, save_intersections, x_max, x_min_remove, export_vtk_data, \
               plot_fracture_size, rock_geometry, terminations, terminate_fractures, length_unit, x_boundary, \
               y_boundary, z_boundary, regular_polygons, n_poly_vertices, n_figure, n_populations, pop_order, \
               axes_ratio, optimize_dfn, transmissivities

    except NameError:
        print('Variables in input file %s are not defined correctly.' % input_filename)
        sys.exit('The DFN calculation terminated.')
