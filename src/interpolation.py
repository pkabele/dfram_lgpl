#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
from random import random

def parabolic_interp(x, y):
    a, b, c = x[1], x[0], x[2]
    fa, fb, fc = y[1], y[0], y[2]
    x_min = b-0.5*(((b-a)**2)*(fb-fc)-((b-c)**2)*(fb-fa))/((b-a)*(fb-fc)-(b-c)*(fb-fa))
    return x_min


def linear_interp(x, fx, minimum_x):
    k = (fx[1]-fx[0])/(x[1]-x[0])
    x_min = (x[0]*k-fx[0])/k
    # x_min = (x[1]-x[0])*fx[1]/(fx[0]-fx[1])+x[1]

    if x_min < minimum_x:
        x_out = minimum_x+random()*0.2*minimum_x
    else:
        x_out = x_min*1.0

    # check for NaN
    if x_out != x_out:
        x_out = minimum_x+random()*minimum_x

    return x_out
