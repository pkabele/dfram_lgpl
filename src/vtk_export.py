#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
import os


def vtk_export(polygons, intersections, rock_volume, outcrop_geometry, outcrop_name, borehole_geometry, borehole_name,
               outcrop_intersections_vtk, path_to_project):

    temp_dir = os.getcwd()
    os.chdir(path_to_project)

    ###################################### FRACTURES #################################

    output_fr = open('Fractures.vtk', 'a')

    # HEADER
    header_fr = '# vtk DataFile Version 3.1\n', 'An output file listing all fractures\n', 'ASCII', '\n', \
                'DATASET POLYDATA\n', '\n'
    output_fr.writelines(header_fr)

    # POINTDATA
    number_of_points_fr = 0
    for i in range(len(polygons)):
        number_of_points_fr += len(polygons[i]['xT'][0:-1])  # exclude the last point

    pointdata_fr = 'POINTS ', '{:d}'.format(number_of_points_fr), ' DOUBLE', '\n'
    output_fr.writelines(pointdata_fr)

    for polygon in polygons:
        x = polygon['xT'][0:-1]  # exclude the last point
        y = polygon['yT'][0:-1]  # exclude the last point
        z = polygon['zT'][0:-1]  # exclude the last point

        for j in range(len(x)):
            string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(x[j], y[j], z[j])
            output_fr.write(string_to_save)

    # CELLDATA
    celldata_fr = '\n', 'POLYGONS ', '{:d}'.format(len(polygons)), ' ', \
                  '{:d}'.format(number_of_points_fr + len(polygons)), '\n'
    output_fr.writelines(celldata_fr)

    counter_fr = 0
    for polygon in polygons:
        counter_fr += len(polygon['xT'][0:-1])  # exclude the last point
        vertices = '{:d}'.format(len(polygon['xT'][0:-1])), ' ', \
                   ' '.join(str(k) for k in list(range(counter_fr-len(polygon['xT'][0:-1]), counter_fr))), '\n'
        output_fr.writelines(vertices)

    # cell_types_fr = '\n',  'CELL_TYPES ', '{:d}'.format(len(polygons)), '\n', \
    #              ' '.join(str(l) for l in list([7]*len(polygons))), '\n'
    # output_fr.writelines(cell_types_fr)

    # CELLDATASETS
    output_fr.writelines('\nCELL_DATA %d \n' % (len(polygons)))

    # populations
    output_fr.writelines('\nSCALARS Population INT 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:d} '.format(polygon['population']))

    # transmissivity
    output_fr.writelines('\n\nSCALARS Transmissivity INT 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:d} '.format(polygon['transmissivity']))

    # area
    output_fr.writelines('\n\nSCALARS Area DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['area_new']))

    # axis 1 size
    output_fr.writelines('\n\nSCALARS Axis_1_size DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['ax1']))

    # axis 2 size
    output_fr.writelines('\n\nSCALARS Axis_2_size DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['ax2']))

    # center x-cooordinate
    output_fr.writelines('\n\nSCALARS Center_x_coords DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['xc']))

    # center y-cooordinate
    output_fr.writelines('\n\nSCALARS Center_y_coords DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['yc']))

    # center z-cooordinate
    output_fr.writelines('\n\nSCALARS Center_z_coords DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['zc']))

    output_fr.writelines('\n')
    output_fr.close()

    ###################################### ROCK VOLUME #################################

    output_rockvol = open('Rock volume.vtk', 'a')

    # HEADER
    header_rockvol = '# vtk DataFile Version 3.1\n', \
           'An output file listing a bounding rock volume\n', \
           'ASCII\n', \
           'DATASET UNSTRUCTURED_GRID\n', \
           '\n'
    output_rockvol.writelines(header_rockvol)

    # POINTDATA
    pointdata_rockvol = 'POINTS 8 DOUBLE\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][0], rock_volume['y'][0], rock_volume['z'][0]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][1], rock_volume['y'][0], rock_volume['z'][0]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][1], rock_volume['y'][0], rock_volume['z'][1]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][0], rock_volume['y'][0], rock_volume['z'][1]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][0], rock_volume['y'][1], rock_volume['z'][0]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][1], rock_volume['y'][1], rock_volume['z'][0]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][1], rock_volume['y'][1], rock_volume['z'][1]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][0], rock_volume['y'][1], rock_volume['z'][1]), '\n'
    output_rockvol.writelines(pointdata_rockvol)

    # CELLDATA
    celldata_rockvol = '\nCELLS 6 30\n', '4 0 1 2 3\n', '4 4 5 6 7\n', '4 0 1 5 4\n', '4 1 2 6 5\n', '4 2 3 7 6\n', \
                       '4 0 3 7 4\n'
    output_rockvol.writelines(celldata_rockvol)

    cell_types_rockvol = '\nCELL_TYPES 6\n', '9 9 9 9 9 9\n'
    output_rockvol.writelines(cell_types_rockvol)

    # CELLDATASETS
    celldatasets_rockvol = '\nCELL_DATA 6\n', 'SCALARS Rock_volume INT\n', 'LOOKUP_TABLE rock_vol_tab\n', '0\n', \
                           '0\n', '0\n', '0\n', '0\n', '0\n', 'LOOKUP_TABLE rock_vol_tab 1\n', '0.0 1.0 1.0 0.2'
    output_rockvol.writelines(celldatasets_rockvol)
    output_rockvol.close()

    ###################################### INTERSECTIONS #################################

    output_intersec = open('Intersections.vtk', 'a')

    # HEADER
    header_intersec = '# vtk DataFile Version 3.1\n', \
           'An output file listing all intersections\n', \
           'ASCII\n', \
           'DATASET UNSTRUCTURED_GRID\n', \
           '\n'
    output_intersec.writelines(header_intersec)

    # POINTDATA
    pointdata_intersec = 'POINTS ', '{:d}'.format(2*len(intersections)), ' DOUBLE\n'
    output_intersec.writelines(pointdata_intersec)

    for intersection in intersections:
        string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(intersection['x'][0], intersection['y'][0],intersection['z'][0]), \
                         '{:.10f} {:.10f} {:.10f} \n'.format(intersection['x'][1], intersection['y'][1],intersection['z'][1])
        output_intersec.writelines(string_to_save)

    # CELLDATA
    celldata_intersec = '\n', 'CELLS ', '{:d}'.format(len(intersections)), ' ', '{:d}'.format(3*len(intersections)), '\n'
    output_intersec.writelines(celldata_intersec)

    counter_intersec = 0
    for intersection in intersections:
        counter_intersec += 2
        vertices = '2', ' ', ' '.join(str(i) for i in list(range(counter_intersec-2, counter_intersec))), '\n'
        output_intersec.writelines(vertices)

    cell_types_intersec = '\n',  'CELL_TYPES ', '{:d}'.format(len(intersections)), '\n', \
                          ' '.join(str(j) for j in list([3]*len(intersections))), '\n'
    output_intersec.writelines(cell_types_intersec)

    # CELLDATASETS
    celldatasets_intersec = '\nCELL_DATA ', '{:d}'.format(len(intersections)), '\n', \
                   'SCALARS Intersections INT\n', \
                   'LOOKUP_TABLE intersections_tab\n'
    output_intersec.writelines(celldatasets_intersec)

    for intersection in intersections:
        inter = '0\n'
        output_intersec.writelines(inter)

    lookup_tab_intersec = 'LOOKUP_TABLE intersections_tab 1\n', \
                          '0.0 1.0 0.0 1.0'
    output_intersec.writelines(lookup_tab_intersec)

    output_intersec.close()

    ###################################### OUTCROPS ######################################

    for i in range(len(outcrop_geometry)):
        outcrop_file_name = '%s.vtk' % outcrop_name[i]
        output_outcrop = open(outcrop_file_name, 'a')

        # HEADER
        header_outcrop = '# vtk DataFile Version 3.1\n', \
               'An output file listing ', '%s' %outcrop_name[i], ' outcrop\n', \
               'ASCII\n', \
               'DATASET POLYDATA\n', \
               '\n'
        output_outcrop.writelines(header_outcrop)

        # POINTDATA
        pointdata_outcrop = 'POINTS ', '{:d}'.format(len(outcrop_geometry[i]['x'])), ' DOUBLE\n'
        output_outcrop.writelines(pointdata_outcrop)

        x = outcrop_geometry[i]['x']
        y = outcrop_geometry[i]['y']
        z = outcrop_geometry[i]['z']

        for j in range(len(x)):
            string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(x[j], y[j], z[j])
            output_outcrop.write(string_to_save)

        # CELLDATA
        celldata_outcrops = '\n', 'POLYGONS 1 ', '{:d}'.format(len(outcrop_geometry[i]['x']) + 1), '\n'
        output_outcrop.writelines(celldata_outcrops)

        vertices = '{:d}'.format(len(outcrop_geometry[i]['x'])), ' ', \
                   ' '.join(str(k) for k in list(range(0, len(outcrop_geometry[i]['x'])))), '\n'
        output_outcrop.writelines(vertices)

        # cell_types_outcrop = '\n',  'CELL_TYPES 1\n', '7\n'
        # output_outcrop.writelines(cell_types_outcrop)

        # CELLDATASETS
        celldatasets_outcrop = '\nCELL_DATA 1', '\n', 'SCALARS Outcrop INT\n', 'LOOKUP_TABLE outcrops_tab\n', '0\n'
        output_outcrop.writelines(celldatasets_outcrop)

        lookup_tab_outcrop = 'LOOKUP_TABLE outcrops_tab 1\n', '1.0 1.0 1.0 1.0'
        output_outcrop.writelines(lookup_tab_outcrop)
        output_outcrop.close()

    ######################### FRACTURE-OUTCROP INTERSECTIONS #########################

    for i in range(len(outcrop_intersections_vtk)):
        frac_out_file_name = '%s_intersections.vtk' % outcrop_name[i]
        output_frac_out = open(frac_out_file_name, 'a')

        # HEADER
        header_frac_out = '# vtk DataFile Version 3.1\n', \
               'An output file listing fracture intersections with', '%s' %outcrop_name[i], ' outcrop\n', \
               'ASCII\n', \
               'DATASET UNSTRUCTURED_GRID\n', \
               '\n'
        output_frac_out.writelines(header_frac_out)

        # POINTDATA
        number_of_fracs_frac_out = 0
        for j in range(len(outcrop_intersections_vtk[i])):
            number_of_fracs_frac_out += len(outcrop_intersections_vtk[i][j])

        pointdata_frac_out = 'POINTS ', '{:d}'.format(number_of_fracs_frac_out*2), ' DOUBLE\n'
        output_frac_out.writelines(pointdata_frac_out)

        for population in outcrop_intersections_vtk[i]:
            for j in range(len(population)):
                string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(population[j]['x'][0], population[j]['y'][0],population[j]['z'][0]), \
                                 '{:.10f} {:.10f} {:.10f} \n'.format(population[j]['x'][1], population[j]['y'][1],population[j]['z'][1])
                output_frac_out.writelines(string_to_save)

        # CELLDATA
        celldata_frac_out = '\n', 'CELLS ', '{:d}'.format(number_of_fracs_frac_out), ' ', \
                            '{:d}'.format(number_of_fracs_frac_out*3), '\n'
        output_frac_out.writelines(celldata_frac_out)

        counter_frac_out = 0
        for j in range(number_of_fracs_frac_out):
            counter_frac_out += 2
            vertices = '2 ', ' '.join(str(k) for k in list(range(counter_frac_out-2, counter_frac_out))), '\n'
            output_frac_out.writelines(vertices)

        cell_types_frac_out = '\n',  'CELL_TYPES ', '{:d}'.format(number_of_fracs_frac_out), '\n', \
                              ' '.join(str(j) for j in list([3]*(number_of_fracs_frac_out))), '\n'
        output_frac_out.writelines(cell_types_frac_out)

        # CELLDATASETS
        celldatasets_frac_out = '\nCELL_DATA ', '{:d}'.format(number_of_fracs_frac_out), '\n', \
                       'SCALARS Outcrop_intersections INT\n', \
                       'LOOKUP_TABLE default'
        output_frac_out.writelines(celldatasets_frac_out)

        #counter_population = 0
        #for population in outcrop_intersections_vtk[i]:
        #    counter_population += 1
        for counter_population, population in enumerate(outcrop_intersections_vtk[i]):
            celldata_to_save = '\n', '\n'.join(str(k) for k in list([counter_population]*(len(population))))
            output_frac_out.writelines(celldata_to_save)

        output_frac_out.close()

    ###################################### BOREHOLES #################################

    for i in range(len(borehole_geometry)):
        borehole_file_name = '%s.vtk' % borehole_name[i]
        output_borehole = open(borehole_file_name, 'a')

        # HEADER
        header_borehole = '# vtk DataFile Version 3.1\n', \
               'An output file listing a borehole\n', \
               'ASCII\n', \
               'DATASET UNSTRUCTURED_GRID\n', \
               '\n'
        output_borehole.writelines(header_borehole)

        # POINTDATA
        pointdata_borehole = 'POINTS ', '{:d}'.format(len(borehole_geometry[i]['x'])), ' DOUBLE\n'
        output_borehole.writelines(pointdata_borehole)

        x = borehole_geometry[i]['x']
        y = borehole_geometry[i]['y']
        z = borehole_geometry[i]['z']

        for j in range(len(x)):
            string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(x[j], y[j], z[j])
            output_borehole.write(string_to_save)

        # CELLDATA
        celldata_borehole = '\n', 'CELLS 1 3\n'
        output_borehole.writelines(celldata_borehole)

        vertices = '2 0 1\n'
        output_borehole.writelines(vertices)

        cell_types_borehole = '\n',  'CELL_TYPES 1\n', '3\n'
        output_borehole.writelines(cell_types_borehole)

        # CELLDATASETS
        celldatasets_borehole = '\nCELL_DATA 1\n', \
                       'SCALARS Borehole INT\n', \
                       'LOOKUP_TABLE borehole_tab\n', \
                       '0\n'
        output_borehole.writelines(celldatasets_borehole)

        lookup_tab_borehole = 'LOOKUP_TABLE borehole_tab 1\n', \
                              '0.0 1.0 1.0 1.0'
        output_borehole.writelines(lookup_tab_borehole)

        output_borehole.close()

    os.chdir(temp_dir)


def vtk_export_testing(polygons, intersections, rock_volume, outcrop_geometry, outcrop_name, borehole_geometry, borehole_name,
                       outcrop_intersections_vtk, path_to_project, name_suffix):

    temp_dir = os.getcwd()
    os.chdir(path_to_project)

    ###################################### FRACTURES #################################

    output_fr = open('Fractures_%s.vtk' % name_suffix, 'a')

    # HEADER
    header_fr = '# vtk DataFile Version 3.1\n', 'An output file listing all fractures\n', 'ASCII', '\n', \
                'DATASET POLYDATA\n', '\n'
    output_fr.writelines(header_fr)

    # POINTDATA
    number_of_points_fr = 0
    for i in range(len(polygons)):
        number_of_points_fr += len(polygons[i]['xT'][0:-1])  # exclude the last point

    pointdata_fr = 'POINTS ', '{:d}'.format(number_of_points_fr), ' DOUBLE', '\n'
    output_fr.writelines(pointdata_fr)

    for polygon in polygons:
        x = polygon['xT'][0:-1]  # exclude the last point
        y = polygon['yT'][0:-1]  # exclude the last point
        z = polygon['zT'][0:-1]  # exclude the last point

        for j in range(len(x)):
            string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(x[j], y[j], z[j])
            output_fr.write(string_to_save)

    # CELLDATA
    celldata_fr = '\n', 'POLYGONS ', '{:d}'.format(len(polygons)), ' ', \
                  '{:d}'.format(number_of_points_fr + len(polygons)), '\n'
    output_fr.writelines(celldata_fr)

    counter_fr = 0
    for polygon in polygons:
        counter_fr += len(polygon['xT'][0:-1])  # exclude the last point
        vertices = '{:d}'.format(len(polygon['xT'][0:-1])), ' ', \
                   ' '.join(str(k) for k in list(range(counter_fr-len(polygon['xT'][0:-1]), counter_fr))), '\n'
        output_fr.writelines(vertices)

    # cell_types_fr = '\n',  'CELL_TYPES ', '{:d}'.format(len(polygons)), '\n', \
    #              ' '.join(str(l) for l in list([7]*len(polygons))), '\n'
    # output_fr.writelines(cell_types_fr)

    # CELLDATASETS
    output_fr.writelines('\nCELL_DATA %d \n' % (len(polygons)))

    # populations
    output_fr.writelines('\nSCALARS Population INT 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:d} '.format(polygon['population']))

    # transmissivity
    output_fr.writelines('\n\nSCALARS Transmissivity INT 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:d} '.format(polygon['transmissivity']))

    # area
    output_fr.writelines('\n\nSCALARS Area DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['area_new']))

    # axis 1 size
    output_fr.writelines('\n\nSCALARS Axis_1_size DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['ax1']))

    # axis 2 size
    output_fr.writelines('\n\nSCALARS Axis_2_size DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['ax2']))

    # center x-cooordinate
    output_fr.writelines('\n\nSCALARS Center_x_coords DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['xc']))

    # center y-cooordinate
    output_fr.writelines('\n\nSCALARS Center_y_coords DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['yc']))

    # center z-cooordinate
    output_fr.writelines('\n\nSCALARS Center_z_coords DOUBLE 1\nLOOKUP_TABLE default\n')
    for polygon in polygons:
        output_fr.writelines('{:f} '.format(polygon['zc']))

    output_fr.writelines('\n')
    output_fr.close()

    ###################################### ROCK VOLUME #################################

    output_rockvol = open('Rock volume.vtk', 'a')

    # HEADER
    header_rockvol = '# vtk DataFile Version 3.1\n', \
           'An output file listing a bounding rock volume\n', \
           'ASCII\n', \
           'DATASET UNSTRUCTURED_GRID\n', \
           '\n'
    output_rockvol.writelines(header_rockvol)

    # POINTDATA
    pointdata_rockvol = 'POINTS 8 DOUBLE\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][0], rock_volume['y'][0], rock_volume['z'][0]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][1], rock_volume['y'][0], rock_volume['z'][0]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][1], rock_volume['y'][0], rock_volume['z'][1]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][0], rock_volume['y'][0], rock_volume['z'][1]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][0], rock_volume['y'][1], rock_volume['z'][0]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][1], rock_volume['y'][1], rock_volume['z'][0]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][1], rock_volume['y'][1], rock_volume['z'][1]), '\n', \
                '{:.10f} {:.10f} {:.10f}'.format(rock_volume['x'][0], rock_volume['y'][1], rock_volume['z'][1]), '\n'
    output_rockvol.writelines(pointdata_rockvol)

    # CELLDATA
    celldata_rockvol = '\nCELLS 6 30\n', '4 0 1 2 3\n', '4 4 5 6 7\n', '4 0 1 5 4\n', '4 1 2 6 5\n', '4 2 3 7 6\n', \
                       '4 0 3 7 4\n'
    output_rockvol.writelines(celldata_rockvol)

    cell_types_rockvol = '\nCELL_TYPES 6\n', '9 9 9 9 9 9\n'
    output_rockvol.writelines(cell_types_rockvol)

    # CELLDATASETS
    celldatasets_rockvol = '\nCELL_DATA 6\n', 'SCALARS Rock_volume INT\n', 'LOOKUP_TABLE rock_vol_tab\n', '0\n', \
                           '0\n', '0\n', '0\n', '0\n', '0\n', 'LOOKUP_TABLE rock_vol_tab 1\n', '0.0 1.0 1.0 0.2'
    output_rockvol.writelines(celldatasets_rockvol)
    output_rockvol.close()

    ###################################### INTERSECTIONS #################################

    output_intersec = open('Intersections_%s.vtk' % name_suffix, 'a')

    # HEADER
    header_intersec = '# vtk DataFile Version 3.1\n', \
           'An output file listing all intersections\n', \
           'ASCII\n', \
           'DATASET UNSTRUCTURED_GRID\n', \
           '\n'
    output_intersec.writelines(header_intersec)

    # POINTDATA
    pointdata_intersec = 'POINTS ', '{:d}'.format(2*len(intersections)), ' DOUBLE\n'
    output_intersec.writelines(pointdata_intersec)

    for intersection in intersections:
        string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(intersection['x'][0], intersection['y'][0],intersection['z'][0]), \
                         '{:.10f} {:.10f} {:.10f} \n'.format(intersection['x'][1], intersection['y'][1],intersection['z'][1])
        output_intersec.writelines(string_to_save)

    # CELLDATA
    celldata_intersec = '\n', 'CELLS ', '{:d}'.format(len(intersections)), ' ', '{:d}'.format(3*len(intersections)), '\n'
    output_intersec.writelines(celldata_intersec)

    counter_intersec = 0
    for intersection in intersections:
        counter_intersec += 2
        vertices = '2', ' ', ' '.join(str(i) for i in list(range(counter_intersec-2, counter_intersec))), '\n'
        output_intersec.writelines(vertices)

    cell_types_intersec = '\n',  'CELL_TYPES ', '{:d}'.format(len(intersections)), '\n', \
                          ' '.join(str(j) for j in list([3]*len(intersections))), '\n'
    output_intersec.writelines(cell_types_intersec)

    # CELLDATASETS
    celldatasets_intersec = '\nCELL_DATA ', '{:d}'.format(len(intersections)), '\n', \
                   'SCALARS Intersections INT\n', \
                   'LOOKUP_TABLE intersections_tab\n'
    output_intersec.writelines(celldatasets_intersec)

    for intersection in intersections:
        inter = '0\n'
        output_intersec.writelines(inter)

    lookup_tab_intersec = 'LOOKUP_TABLE intersections_tab 1\n', \
                          '0.0 1.0 0.0 1.0'
    output_intersec.writelines(lookup_tab_intersec)

    output_intersec.close()

    ###################################### OUTCROPS ######################################

    for i in range(len(outcrop_geometry)):
        outcrop_file_name = '%s.vtk' % outcrop_name[i]
        output_outcrop = open(outcrop_file_name, 'a')

        # HEADER
        header_outcrop = '# vtk DataFile Version 3.1\n', \
               'An output file listing ', '%s' %outcrop_name[i], ' outcrop\n', \
               'ASCII\n', \
               'DATASET POLYDATA\n', \
               '\n'
        output_outcrop.writelines(header_outcrop)

        # POINTDATA
        pointdata_outcrop = 'POINTS ', '{:d}'.format(len(outcrop_geometry[i]['x'])), ' DOUBLE\n'
        output_outcrop.writelines(pointdata_outcrop)

        x = outcrop_geometry[i]['x']
        y = outcrop_geometry[i]['y']
        z = outcrop_geometry[i]['z']

        for j in range(len(x)):
            string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(x[j], y[j], z[j])
            output_outcrop.write(string_to_save)

        # CELLDATA
        celldata_outcrops = '\n', 'POLYGONS 1 ', '{:d}'.format(len(outcrop_geometry[i]['x']) + 1), '\n'
        output_outcrop.writelines(celldata_outcrops)

        vertices = '{:d}'.format(len(outcrop_geometry[i]['x'])), ' ', \
                   ' '.join(str(k) for k in list(range(0, len(outcrop_geometry[i]['x'])))), '\n'
        output_outcrop.writelines(vertices)

        # cell_types_outcrop = '\n',  'CELL_TYPES 1\n', '7\n'
        # output_outcrop.writelines(cell_types_outcrop)

        # CELLDATASETS
        celldatasets_outcrop = '\nCELL_DATA 1', '\n', 'SCALARS Outcrop INT\n', 'LOOKUP_TABLE outcrops_tab\n', '0\n'
        output_outcrop.writelines(celldatasets_outcrop)

        lookup_tab_outcrop = 'LOOKUP_TABLE outcrops_tab 1\n', '1.0 1.0 1.0 1.0'
        output_outcrop.writelines(lookup_tab_outcrop)
        output_outcrop.close()

    ######################### FRACTURE-OUTCROP INTERSECTIONS #########################

    for i in range(len(outcrop_intersections_vtk)):
        frac_out_file_name = '%s_intersections_%s.vtk' % (outcrop_name[i], name_suffix)
        output_frac_out = open(frac_out_file_name, 'a')

        # HEADER
        header_frac_out = '# vtk DataFile Version 3.1\n', \
               'An output file listing fracture intersections with', '%s' %outcrop_name[i], ' outcrop\n', \
               'ASCII\n', \
               'DATASET UNSTRUCTURED_GRID\n', \
               '\n'
        output_frac_out.writelines(header_frac_out)

        # POINTDATA
        number_of_fracs_frac_out = 0
        for j in range(len(outcrop_intersections_vtk[i])):
            number_of_fracs_frac_out += len(outcrop_intersections_vtk[i][j])

        pointdata_frac_out = 'POINTS ', '{:d}'.format(number_of_fracs_frac_out*2), ' DOUBLE\n'
        output_frac_out.writelines(pointdata_frac_out)

        for population in outcrop_intersections_vtk[i]:
            for j in range(len(population)):
                string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(population[j]['x'][0], population[j]['y'][0],population[j]['z'][0]), \
                                 '{:.10f} {:.10f} {:.10f} \n'.format(population[j]['x'][1], population[j]['y'][1],population[j]['z'][1])
                output_frac_out.writelines(string_to_save)

        # CELLDATA
        celldata_frac_out = '\n', 'CELLS ', '{:d}'.format(number_of_fracs_frac_out), ' ', \
                            '{:d}'.format(number_of_fracs_frac_out*3), '\n'
        output_frac_out.writelines(celldata_frac_out)

        counter_frac_out = 0
        for j in range(number_of_fracs_frac_out):
            counter_frac_out += 2
            vertices = '2 ', ' '.join(str(k) for k in list(range(counter_frac_out-2, counter_frac_out))), '\n'
            output_frac_out.writelines(vertices)

        cell_types_frac_out = '\n',  'CELL_TYPES ', '{:d}'.format(number_of_fracs_frac_out), '\n', \
                              ' '.join(str(j) for j in list([3]*(number_of_fracs_frac_out))), '\n'
        output_frac_out.writelines(cell_types_frac_out)

        # CELLDATASETS
        celldatasets_frac_out = '\nCELL_DATA ', '{:d}'.format(number_of_fracs_frac_out), '\n', \
                       'SCALARS Outcrop_intersections INT\n', \
                       'LOOKUP_TABLE default'
        output_frac_out.writelines(celldatasets_frac_out)

        #counter_population = 0
        #for population in outcrop_intersections_vtk[i]:
        #    counter_population += 1
        for counter_population, population in enumerate(outcrop_intersections_vtk[i]):
            celldata_to_save = '\n', '\n'.join(str(k) for k in list([counter_population]*(len(population))))
            output_frac_out.writelines(celldata_to_save)

        output_frac_out.close()

    ###################################### BOREHOLES #################################

    for i in range(len(borehole_geometry)):
        borehole_file_name = '%s.vtk' % borehole_name[i]
        output_borehole = open(borehole_file_name, 'a')

        # HEADER
        header_borehole = '# vtk DataFile Version 3.1\n', \
               'An output file listing a borehole\n', \
               'ASCII\n', \
               'DATASET UNSTRUCTURED_GRID\n', \
               '\n'
        output_borehole.writelines(header_borehole)

        # POINTDATA
        pointdata_borehole = 'POINTS ', '{:d}'.format(len(borehole_geometry[i]['x'])), ' DOUBLE\n'
        output_borehole.writelines(pointdata_borehole)

        x = borehole_geometry[i]['x']
        y = borehole_geometry[i]['y']
        z = borehole_geometry[i]['z']

        for j in range(len(x)):
            string_to_save = '{:.10f} {:.10f} {:.10f} \n'.format(x[j], y[j], z[j])
            output_borehole.write(string_to_save)

        # CELLDATA
        celldata_borehole = '\n', 'CELLS 1 3\n'
        output_borehole.writelines(celldata_borehole)

        vertices = '2 0 1\n'
        output_borehole.writelines(vertices)

        cell_types_borehole = '\n',  'CELL_TYPES 1\n', '3\n'
        output_borehole.writelines(cell_types_borehole)

        # CELLDATASETS
        celldatasets_borehole = '\nCELL_DATA 1\n', \
                       'SCALARS Borehole INT\n', \
                       'LOOKUP_TABLE borehole_tab\n', \
                       '0\n'
        output_borehole.writelines(celldatasets_borehole)

        lookup_tab_borehole = 'LOOKUP_TABLE borehole_tab 1\n', \
                              '0.0 1.0 1.0 1.0'
        output_borehole.writelines(lookup_tab_borehole)

        output_borehole.close()

    os.chdir(temp_dir)
