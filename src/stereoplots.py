#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
# Import the needed modules
import matplotlib.pyplot as plt
import mplstereonet


def stereo_plots(strikes, dips, rgbs, title, output_path, n_figure):
    # plotting: documentation of the mplstereonet plotting package: https://pypi.python.org/pypi/mplstereonet

    fig = plt.figure(n_figure)
    ax = fig.add_subplot(111, projection='stereonet')
    for i in range(0, len(strikes)):
        ax.pole(strikes[i], dips[i], 'o', color=rgbs[i], markersize=5)
    ax.grid(True)
    fig.canvas.set_window_title(title)
    plt.savefig('%s/%s-poles.png' % (output_path, title))
    plt.close(fig)
    # fig.show()

    fig = plt.figure(n_figure+1)
    ax = fig.add_subplot(111, projection='stereonet')
    for i in range(0, len(strikes)):
        ax.plane(strikes[i], dips[i], color=rgbs[i], linewidth=1)
    ax.grid(True)
    fig.canvas.set_window_title(title)
    plt.savefig('%s/%s-planes.png' % (output_path, title))
    plt.close(fig)
    # fig.show()

    fig = plt.figure(n_figure+2)
    ax = fig.add_subplot(111, projection='stereonet')
    cax = ax.density_contourf(strikes, dips, measurement='poles')
    ax.grid(True)
    fig.colorbar(cax)
    fig.canvas.set_window_title(title)
    plt.savefig('%s/%s-density-of-poles.png' % (output_path, title))
    plt.close(fig)
    # fig.show()


def stereo_plots_populations(out_path, n_pops, pops_obs_all, pops_2d_all, dips_obs_all, strikes_obs_all,
                             dips_2d_all, strikes_2d_all, rgbs, n_figure):
    for i in range(n_pops):
        dips_obs, dips_calc, strikes_obs, strikes_calc = [], [], [], []
        pop_num = i+1
        for j, pop in enumerate(pops_obs_all):
            if pop_num == pop:
                dips_obs.append(dips_obs_all[j])
                strikes_obs.append(strikes_obs_all[j])
        for j, pop in enumerate(pops_2d_all):
            if pop_num == pop:
                dips_calc.append(dips_2d_all[j])
                strikes_calc.append(strikes_2d_all[j])

        fig = plt.figure(n_figure)
        n_figure += 1
        ax = fig.add_subplot(111, projection='stereonet')
        for ii in range(0, len(strikes_obs)):
            ax.pole(strikes_obs[ii], dips_obs[ii], 'o', color=rgbs[i], markersize=5)
        ax.grid(True)
        fig.canvas.set_window_title('Stereogram from fracture traces on all outcrops (observations), '
                                    'population %d' % pop_num)
        plt.savefig(out_path+'/results_populations/stereogram-population_'+str(pop_num)+
                    '-observations_on_all_outcrops.png')

        fig = plt.figure(n_figure)
        n_figure += 1
        ax = fig.add_subplot(111, projection='stereonet')
        for ii in range(0, len(strikes_calc)):
            ax.pole(strikes_calc[ii], dips_calc[ii], 'o', color=rgbs[i], markersize=5)
        ax.grid(True)
        fig.canvas.set_window_title('Stereogram from fracture traces on all outcrops (simulation), '
                                    'population %d' % pop_num)
        plt.savefig(out_path+'/results_populations/stereogram-population_'+str(pop_num)+
                    '-simulation_results_on_all_outcrops.png')
        plt.close(fig)
