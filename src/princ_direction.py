#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Calculation of the principal direction vector for a population
of fractures given by dip and strike
Assumptions:
- RHR rule for dip
- Carthesian system: east-north-up
- see Waldron, Formula Sheet for Structural Geology and Tectonics
"""
from math import *
import numpy as np
import scipy as sp


def sd_to_vec(s, d):
    "Transforms strike and dip to pole vector (on lower hemispehere)."
    nx = -cos(radians(s))*sin(radians(d))
    ny = sin(radians(s))*sin(radians(d))
    nz = -cos(radians(d))
    return np.array([nx, ny, nz])

    
def vec_to_tp(vec):
    "Transforms pole vector (on lower hemispehere) to trend and plunge."
    ex = vec[0]
    ey = vec[1]
    ez = vec[2]
    if ey > 0:
        trend = degrees(atan(ex/ey))
    else:
        if ey < 0:
            trend = degrees(atan(ex/ey))+180
        else:
            trend="NaN"
    plunge = -degrees(asin(ez))
    return [trend, plunge]


'''
Clean up.
Function vec_to_sd corrected by PK, 26.08.2019
def vec_to_sd(vec):    
    "Transforms pole vector (on lower hemispehere) to strike and dip."
    ex = vec[0]
    ey = vec[1]
    ez = vec[2]
    if ey > 0:
        strike = degrees(atan(ex / ey)) + 90
    else:
        if ey < 0:
            strike = degrees(atan(ex / ey)) + 270
        else:
            strike = "NaN"
    dip = degrees(acos(-ez))  # Error in Waldron ??
    return [strike, dip]
'''
def vec_to_sd(vec):    
    """
    Adopted from DFN_tools_fcns.py (PK, 26.08.2019)
    Transforms lower hemispehere pole vector (vec) to strike and dip.
    Note:
    The transformation relations were derived for normal vector (upper hemisphere),
    See OneNote 14.04.19 16:08. So, (nx,ny,nz) are components of normal vector.
    """
    tol = 1.0e-6
# Convert vec to normal vector (nx,ny,nz). Generally, normal is just pole reversed.
# However, if pole vector is oriented upward, the normal vector is not reversed.
# (If nz is negative, it would result in dip > 90 deg). 
    if vec[2] > 0.0:
        nx = vec[0]
        ny = vec[1]
        nz = vec[2]
    else:
        nx = -vec[0]
        ny = -vec[1]
        nz = -vec[2]

    dip=degrees(acos(nz))
    if np.abs(nx) <= tol:
        if ny > 0:
            strike = 270.0
        else:
            strike = 90.0
        return(strike,dip)
    s0 = degrees(atan(-ny/nx))
    if nx < 0.0:
        strike = s0+180.0
    else:
        if ny <= 0:
            strike = s0
        else:
            strike = s0+360.0
    return [strike,dip]   
'''
Clean up (PK, 26.08.2019)
This function is needed for calculation of kappa by solutvin of transcendental
equation. Not used in DFraM.

def Admrb(kappa, N, R):
    admrb_result = sp.special.iv(3/2, kappa)/sp.special.iv(3/2-1, kappa)-R/N
    return admrb_result
'''    


def cal_kappa(N, R):
    # Find kappa of Fisher distribution by solving nonlinear eq."
    # Ref. Fisher (1993), Mardia and Jupp (1999) or Dhillon and Sra (2003)
    if N == R:
        kappa = 1.e6
    if N < 16:
        # kappa = np.power((N/(N-R))*(1-(1/N)), 2)
        kappa = (N / (N - R)) * np.power((1 - (1 / N)), 2)
    else:
        kappa = (N - 1) / (N - R)
    return kappa


def cal_Terzaghi(fstrike, fdip, ocnvec, tcmax=10):
    """
    Calculate Terzaghi correction factor.
    tcmax ... max. value of correction factor, e.g 10 (Priest 1993)
    """
    fnvec = sd_to_vec(fstrike, fdip)
    crvec = np.cross(fnvec, ocnvec)
    cr = np.linalg.norm(crvec)
    fn = np.linalg.norm(fnvec)
    on = np.linalg.norm(ocnvec)
    sinbeta = abs(cr/(fn*on))
    tcfact = min([1.0/sinbeta, tcmax])
    return tcfact

