#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
from scipy.stats import vonmises
from output_file import *
from vmf_sampling import *
from princ_direction import *
from scipy import optimize
import powerlaw


def calculate_fisher(dips, strikes, outcrop_numbers, outcrop_normals, Terzaghi_corr=True):
    nvecs = []
    A = np.zeros([3, 3])
    for i in range(len(dips)):
        d = dips[i]
        s = strikes[i]

        #  Vector components of pole (on lower hemisphere)
        nvec = sd_to_vec(s, d)
        nvecs.append(nvec)
        A = A + np.outer(np.matrix(nvec), np.matrix(nvec).T)

    eigenA = np.linalg.eigh(A)
    imax = np.argmax(eigenA[0])
    evec = eigenA[1][:, imax]

    # Reverse the vector if it is it has upward orientation
    if evec[2] > 0:
       evec = np.negative(evec)

    # Calculate the resultant vector R and unit r with reorientation
    Rvec = np.array([0, 0, 0])

    # Number of vectors
    N = len(nvecs)
    Nw = 0
    tcfs = []
    # Change orientation of all n-vectors according to e-vector
    for ivec, nvec in enumerate(nvecs):
        if np.inner(evec, nvec) < 0:
            nvec = np.negative(nvec)
            nvecs[ivec] = nvec*1.0

        # Add n to R, apply Tarzaghi correction factor
        tcf = cal_Terzaghi(strikes[ivec], dips[ivec], outcrop_normals[int(outcrop_numbers[ivec])])
        # print('[%d, %d, %.16f]' % (dips[ivec], strikes[ivec], tcf))
        tcfs.append(tcf)
        Nw += tcf  # adopted from DFN_verify

    for ivec, nvec in enumerate(nvecs):
        if Terzaghi_corr:
            Rvec = Rvec + nvec * tcfs[ivec] * N/Nw
        else:
            Rvec = Rvec + nvec

    # |R|
    Rnorm = np.linalg.norm(Rvec)

    # Unit vector r=R/|R|
    rvec = Rvec / Rnorm

    # Strike and dip of plane associated with pole
    [mean_strike, mean_dip] = vec_to_sd(rvec)
    '''
    Corrected in princ_direction.py/def vec_to_sd (PK, 26.08.2019)
    if mean_dip > 90:
        mean_dip = 180 - mean_dip
    '''

    # Estimate kappa
    kappa = cal_kappa(N, Rnorm)

    return mean_dip, mean_strike, kappa



'''
Clean up (PK, 26.08.2019)
This function is needed for calculation of kappa by solutvin of transcendental
equation. Not used in DFraM.

def cal_kappa_tr(N, R):
    # Find kappa of Fisher distribution by solving transcendental eq.
    # Ref. Fisher (1993), Mardia and Jupp (1999) or Dhillon and Sra (2003)
    try:
        kappa = optimize.newton(Admrb, 0.01, args=(N, R,))
    except:
        kappa = cal_kappa(N,R)
        print("Warning: solution of transcendental equation for kappa failed, using simplified formula instead.")
        print("N = ", N, "R = ", R, "kappa = ", kappa)
        pass
    return kappa
'''

def stats(temp_dips, temp_strikes, temp_x_starts, temp_x_ends, temp_y_starts, temp_y_ends, temp_z_starts, temp_z_ends):

    if len(temp_dips) > 1:
        dip_temp_mean, strike_temp_mean, kappa = calculate_fisher(temp_dips, temp_strikes, outcrop_normals=[[1, 0, 0]],
                                                                  outcrop_numbers=0, Terzaghi_corr=False)
    elif len(temp_dips) == 1:
        dip_temp_mean = temp_dips[0]*1.0
        strike_temp_mean = temp_strikes[0]*1.0
        kappa = 1e3
    else:
        dip_temp_mean = 0.0
        strike_temp_mean = 0.0
        kappa = 1e3

    # fracture length
    fracture_length_temp = [None]*len(temp_x_starts)
    for k in range(len(temp_x_starts)):
        fracture_length_temp[k] = np.sqrt((temp_x_ends[k] - temp_x_starts[k])**2 + (temp_y_ends[k]
                                                        - temp_y_starts[k])**2 + (temp_z_ends[k] - temp_z_starts[k])**2)

    cumulative_fracture_length = 0
    for k in range(len(temp_x_starts)):
        cumulative_fracture_length += fracture_length_temp[k]

    # powerlaw constants
    [x_min, alpha] = get_powerlaw_constants(fracture_length_temp)

    # number of fractures
    n_fractures_in_popul = len(temp_dips)

    return strike_temp_mean, dip_temp_mean, kappa, x_min, alpha, n_fractures_in_popul, fracture_length_temp, \
           cumulative_fracture_length


def calculate_mu_and_kappa(out_path, calculation_results, pops, strikes, dips, outcrop_numbers, outcrop_normals):

    try:
        n_pops = int(max(pops))

        strikes_mean, dips_mean, kappas = [], [], []
        for ii in range(n_pops):
            pop_num = ii+1
            strikes_pop, dips_pop, outcrop_numbers_pop = [], [], []

            '''
            for iii in range(len(outcrop_normals)):
                print('Outcrop: %d' % (iii+1))
                print('[%.8f, %.8f, %.8f]' % (outcrop_normals[iii][0], outcrop_normals[iii][1], outcrop_normals[iii][2]))
            '''

            for j, pop in enumerate(pops):
                if pop_num == pop:
                    strikes_pop.append(strikes[j])
                    dips_pop.append(dips[j])
                    outcrop_numbers_pop.append(outcrop_numbers[j])

            if len(dips_pop) > 1:
                dip_mean, strike_mean, kappa_temp = calculate_fisher(dips_pop, strikes_pop, outcrop_numbers_pop,
                                                                     outcrop_normals, Terzaghi_corr=True)
            elif len(dips_pop) == 1:
                dip_mean = dips_pop[0]*1.0
                strike_mean = strikes_pop[0]*1.0
                kappa_temp = 1000
            else:
                dip_mean = 0.0
                strike_mean = 0.0
                kappa_temp = 1000
                write_output(out_path, 'There are no fracture traces on any outcrop representing population %d.' % pop_num)

            if len(dips_pop) > 5:
                kappa = kappa_temp
            elif len(dips_pop) != 0:
                write_output(out_path, 'Less than 5 fracture traces for population %d to determine parameter '
                                       'kappa for Von Mises - Fisher distribution, result may be inaccurate.' % pop_num)
                kappa = kappa_temp*1.0
            else:
                write_output(out_path, 'Only one fracture trace for population %d to determine parameter '
                                       'kappa for Von Mises - Fisher distribution, kappa cannot be determined -> '
                                       'established as kappa = 1000.' % pop_num)
                kappa = 1e3

            strikes_mean.append(strike_mean)
            dips_mean.append(dip_mean)
            kappas.append(kappa)

            if not calculation_results:
                write_meas_output(out_path, 'Statistics for the calculation, population %d:' % pop_num)
                write_meas_output(out_path, 'Mean dip: %.3f°' % dip_mean)
                write_meas_output(out_path, 'Mean strike: %.3f°' % strike_mean)
                write_meas_output(out_path, 'Concentration parameter, kappa: %.4f \n' % kappa)
            else:
                write_output(out_path, 'Statistics for the calculation, population %d:' % pop_num)
                write_output(out_path, 'Mean dip: %.3f°' % dip_mean)
                write_output(out_path, 'Mean strike: %.3f°' % strike_mean)
                write_output(out_path, 'Concentration parameter, kappa: %.4f \n' % kappa)

        return strikes_mean, dips_mean, kappas
    except:
        pass


def stats2(temp_dips, temp_strikes, temp_x_starts, temp_x_ends, temp_y_starts, temp_y_ends, temp_z_starts, temp_z_ends,
           min_length, outcrop_numbers, outcrop_normals):

    if len(temp_dips) > 1:
        dip_temp_mean, strike_temp_mean, kappa_temp = calculate_fisher(temp_dips, temp_strikes, outcrop_numbers,
                                                                       outcrop_normals, Terzaghi_corr=False)
    elif len(temp_dips) == 1:
        dip_temp_mean = temp_dips[0]*1.0
        strike_temp_mean = temp_strikes[0]*1.0
    else:
        dip_temp_mean = 0.0
        strike_temp_mean = 0.0

    if len(temp_dips) > 1:
        kappa = kappa_temp
    else:
        kappa = 1e3

    # fracture length
    fracture_length_temp = [None]*len(temp_x_starts)
    for k in range(len(temp_x_starts)):
        fracture_length_temp[k] = np.sqrt((temp_x_ends[k] - temp_x_starts[k])**2 + (temp_y_ends[k]
                                                        - temp_y_starts[k])**2 + (temp_z_ends[k] - temp_z_starts[k])**2)

    cumulative_fracture_length = 0
    n_fractures_in_popul = 0
    for k in range(len(temp_x_starts)):
        if fracture_length_temp[k] > min_length:
            cumulative_fracture_length += fracture_length_temp[k]
            n_fractures_in_popul += 1

    # mean fracture length
    mean_length = cumulative_fracture_length/n_fractures_in_popul

    return strike_temp_mean, dip_temp_mean, kappa, mean_length, n_fractures_in_popul, fracture_length_temp, \
           cumulative_fracture_length


def stats_reduced(temp_x_starts, temp_x_ends, temp_y_starts, temp_y_ends, temp_z_starts, temp_z_ends, area):

    # fracture length
    fracture_length_temp = [None]*len(temp_x_starts)
    cumulative_fracture_length = 0
    for k in range(len(temp_x_starts)):
        fracture_length_temp[k] = np.sqrt((temp_x_ends[k]-temp_x_starts[k])**2+(temp_y_ends[k]-temp_y_starts[k])**2+
                                          (temp_z_ends[k]-temp_z_starts[k])**2)
        cumulative_fracture_length += fracture_length_temp[k]

    # number of fractures
    n_fractures_in_popul = len(temp_x_starts)

    # mean length of fractures
    if n_fractures_in_popul > 0:
        mean_length = cumulative_fracture_length/n_fractures_in_popul
    else:
        mean_length = 0

    density_fracutures_in_popul = n_fractures_in_popul/area

    return mean_length, density_fracutures_in_popul, n_fractures_in_popul


def stats_reduced_2(temp_x_starts, temp_x_ends, temp_y_starts, temp_y_ends, temp_z_starts, temp_z_ends, area):

    # fracture length
    fracture_length_temp = [None]*len(temp_x_starts)
    cumulative_fracture_length = 0
    for k in range(len(temp_x_starts)):
        fracture_length_temp[k] = np.sqrt((temp_x_ends[k]-temp_x_starts[k])**2+(temp_y_ends[k]-temp_y_starts[k])**2+
                                          (temp_z_ends[k]-temp_z_starts[k])**2)
        cumulative_fracture_length += fracture_length_temp[k]

    # number of fractures
    n_fractures_in_popul = len(temp_x_starts)
    density_fracutures_in_popul = n_fractures_in_popul/area

    return cumulative_fracture_length, density_fracutures_in_popul, n_fractures_in_popul


def stats_reduced_all_fractures(temp_x_starts, temp_x_ends, temp_y_starts, temp_y_ends, temp_z_starts, temp_z_ends, area):

    # fracture length
    fracture_length_temp = [None]*len(temp_x_starts)
    cumulative_fracture_length = 0
    for k in range(len(temp_x_starts)):
        fracture_length_temp[k] = np.sqrt((temp_x_ends[k]-temp_x_starts[k])**2+(temp_y_ends[k]-temp_y_starts[k])**2+
                                          (temp_z_ends[k]-temp_z_starts[k])**2)
        cumulative_fracture_length += fracture_length_temp[k]

    # number of fractures
    n_fractures_in_popul = len(temp_x_starts)

    # mean length of fractures
    if n_fractures_in_popul > 0:
        mean_length = cumulative_fracture_length/n_fractures_in_popul
    else:
        mean_length = 0

    density_fracutures_in_popul = n_fractures_in_popul/area

    return mean_length, density_fracutures_in_popul, n_fractures_in_popul, fracture_length_temp


def get_lengths_of_traces(temp_x_starts, temp_x_ends, temp_y_starts, temp_y_ends, temp_z_starts, temp_z_ends):
    lengths = [0.0]*len(temp_x_starts)
    for k in range(len(temp_x_starts)):
        lengths[k] = np.sqrt((temp_x_ends[k]-temp_x_starts[k])**2+(temp_y_ends[k]-temp_y_starts[k])**2+
                             (temp_z_ends[k]-temp_z_starts[k])**2)
    return lengths


def generate_powerlaw_dist(n_samples, x_min, alpha, x_max, max_iter=100):
    if n_samples != 1:
        x_powerlaw = powerlaw.Power_Law(xmin=x_min, parameters=[alpha]).generate_random(n_samples)

        for i in range(max_iter):
            mask = np.where(x_powerlaw > x_max)
            n_vals_larger_x_max = len(mask[0].tolist())
            if n_vals_larger_x_max == 0:
                break
            else:
                retained_vals = x_powerlaw[np.where(x_powerlaw <= x_max)]
                new_vals = powerlaw.Power_Law(xmin=x_min, parameters=[alpha]).generate_random(n_vals_larger_x_max)
                x_powerlaw = np.concatenate((retained_vals, new_vals), axis=None)
    else:
        x_powerlaw = x_min

    return x_powerlaw


def get_powerlaw_constants(x):
    if len(x) > 1:
        fit = powerlaw.Fit(x)
        alpha = fit.power_law.alpha
        x_min = fit.power_law.xmin
        '''
        x_min = np.min(x)
        x_for_derivation = x-x_min+1
        n_samples = len(x)
        alpha = 1+n_samples/np.sum(np.log(x_for_derivation))
        '''
    elif len(x) == 1:
        x_min = x[0]/10
        alpha = 1.000
    else:
        x_min = float('NaN')
        alpha = float('NaN')
    return x_min, alpha


def generate_vonmises_dist(n_samples, mu, kp):
    x_vonmises = vonmises.rvs(kappa=kp, loc=mu, size=n_samples)
    # x_vonmises = np.random.vonmises(mu, kp, n_samples)
    return x_vonmises


def get_strike_dip(n, e, u):
    r2d = 180 / np.pi
    if u < 0:
        n = -n*1.0
        e = -e*1.0
        u = -u*1.0
    strike = np.arctan2(e, n)*r2d
    strike -= 90
    while strike >= 360:
        strike -= 360
    while strike < 0:
        strike += 360
    x = np.sqrt(np.power(n, 2)+np.power(e, 2))
    dip = np.arctan2(x, u)*r2d
    return strike, dip


def generate_fisher_dist(n_samples, dip_mean, strike_mean, kappa):

    n_n = np.sin(np.deg2rad(dip_mean))*np.sin(np.deg2rad(strike_mean))
    n_e = -np.sin(np.deg2rad(dip_mean))*np.cos(np.deg2rad(strike_mean))
    n_d = np.cos(np.deg2rad(dip_mean))

    norm_dirs = [n_n, n_e, n_d]/np.linalg.norm([n_n, n_e, n_d])
    vectors = sample_vMF(norm_dirs, kappa, n_samples)

    generated_strikes, generated_dips = [None]*n_samples, [None]*n_samples
    for i in range(n_samples):
        [generated_strikes[i], generated_dips[i]] = get_strike_dip(vectors[i][0], vectors[i][1], -vectors[i][2])

    return generated_dips, generated_strikes


def jsd(x, y):  # Jensen-shannon divergence
    try:
        normed_x = [float(i)/max(x) for i in x]
        normed_y = [float(i)/max(y) for i in y]
        x = np.array(normed_x)
        y = np.array(normed_y)
        d1 = x * np.log2(2 * x / (x + y))
        d2 = y * np.log2(2 * y / (x + y))
        d1[np.isnan(d1)] = 0
        d2[np.isnan(d2)] = 0
        d = 0.5 * np.sum(d1 + d2)
    except ZeroDivisionError:
        d = 1.0
    return d
