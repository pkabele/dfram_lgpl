#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
from stats import *


class Fractures:

    def __init__(self, population, n_fractures, x_range, y_range, z_range, powerlaw_constants, aspect_ratio_range,
                 c_range, orientation, in_plane_rotation_range, n_poly_range, rgb, rock_geometries, transmissivity):
        self.population = population
        self.n_fractures = n_fractures
        self.x_range = x_range
        self.y_range = y_range
        self.z_range = z_range
        self.x_min = powerlaw_constants[0]
        self.alpha = powerlaw_constants[1]
        self.x_max = powerlaw_constants[2]
        self.aspect_ratio_range = aspect_ratio_range
        self.c_range = c_range
        self.strike_mean = orientation[0]
        self.dip_mean = orientation[1]
        self.kappa = orientation[2]
        self.in_plane_rotation_range = in_plane_rotation_range
        self.n_poly_range = n_poly_range
        self.rgb = rgb
        self.rock_geometries = rock_geometries
        self.transmissivity = transmissivity

    def generate_fractures(self):
        generated_fractures = [None]*self.n_fractures
        generated_lengths = generate_powerlaw_dist(self.n_fractures, self.x_min, self.alpha, self.x_max)
        [generated_dips, generated_strikes] = generate_fisher_dist(self.n_fractures, self.dip_mean, self.strike_mean,
                                                                   min(self.kappa, 10000))

        # handling the transmissivity parameter
        self.transmissivity = np.array(self.transmissivity) / sum(self.transmissivity)
        transmissivity_n_fractures = [round(self.transmissivity[0]*self.n_fractures),
                                      round(self.transmissivity[1]*self.n_fractures),
                                      round(self.transmissivity[2]*self.n_fractures)]

        if len(self.rock_geometries) > 0:
            for i in range(self.n_fractures):
                if i < transmissivity_n_fractures[0]:
                    cur_transmissivity = 0
                elif i >= transmissivity_n_fractures[0] and i < transmissivity_n_fractures[0] + transmissivity_n_fractures[1]:
                    cur_transmissivity = 1
                else:
                    cur_transmissivity = 2

                rve_idx = np.random.randint(0, len(self.rock_geometries))
                x_range = self.rock_geometries[rve_idx]['x']
                y_range = self.rock_geometries[rve_idx]['y']
                z_range = self.rock_geometries[rve_idx]['z']
                generated_fractures[i] = {'population': self.population,
                                          'rgb': self.rgb,
                                          'n_points_poly': round(np.random.rand()*(self.n_poly_range[1]-self.n_poly_range[0])+self.n_poly_range[0]),
                                          'x': np.random.rand()*(x_range[1]-x_range[0])+x_range[0],
                                          'y': np.random.rand()*(y_range[1]-y_range[0])+y_range[0],
                                          'z': np.random.rand()*(z_range[1]-z_range[0])+z_range[0],
                                          'a': generated_lengths[i],
                                          'b': generated_lengths[i]*(np.random.rand()*(self.aspect_ratio_range[1]-self.aspect_ratio_range[0])+self.aspect_ratio_range[0]),
                                          'transmissivity': int(cur_transmissivity),
                                          'strike': generated_strikes[i],
                                          'dip': generated_dips[i],
                                          'in_plane_rotation': np.random.rand()*(self.in_plane_rotation_range[1]-self.in_plane_rotation_range[0])+self.in_plane_rotation_range[0]
                                          }
        else:
            for i in range(self.n_fractures):
                if i < transmissivity_n_fractures[0]:
                    cur_transmissivity = 0
                elif i >= transmissivity_n_fractures[0] and i < transmissivity_n_fractures[0] + transmissivity_n_fractures[1]:
                    cur_transmissivity = 1
                else:
                    cur_transmissivity = 2

                generated_fractures[i] = {'population': self.population,
                                          'rgb': self.rgb,
                                          'n_points_poly': round(np.random.rand()*(self.n_poly_range[1]-self.n_poly_range[0])+self.n_poly_range[0]),
                                          'x': np.random.rand()*(self.x_range[1]-self.x_range[0])+self.x_range[0],
                                          'y': np.random.rand()*(self.y_range[1]-self.y_range[0])+self.y_range[0],
                                          'z': np.random.rand()*(self.z_range[1]-self.z_range[0])+self.z_range[0],
                                          'a': generated_lengths[i],
                                          'b': generated_lengths[i]*(np.random.rand()*(self.aspect_ratio_range[1]-self.aspect_ratio_range[0])+self.aspect_ratio_range[0]),
                                          'transmissivity': int(cur_transmissivity),
                                          'strike': generated_strikes[i],
                                          'dip': generated_dips[i],
                                          'in_plane_rotation': np.random.rand()*(self.in_plane_rotation_range[1]-self.in_plane_rotation_range[0])+self.in_plane_rotation_range[0]
                                          }
        return generated_fractures


    def get_n_fractures_in_popul(self):
        n_fractures = self.n_fractures
        return n_fractures
