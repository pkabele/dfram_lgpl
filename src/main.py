#
#    Copyright 2016-2021 Václav Nežerka
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#

from fracturegen import *
from fracturepolygon import *
from polypolyintersect import *
from segpolyintersect import *
from process_spreadsheets import *
from vtk_export import *
from remove_polygons import *
from confidence import *
from process_data import *
from interpolation import *
from input_processing import *
from output_file import *
from rve_generation import *
from objective_functions import *
from recalculate_outcrops import *
import copy
import warnings
import sys
import subprocess
warnings.filterwarnings("ignore", category=RuntimeWarning)


# input file (uncomment as needed or provide as argument)
# input_filename = '../data/input/692_DFN_optimization_pop1.in'  # optimization
# input_filename = '../data/input/692_DFN_verification_pop1.in'  # verification
input_filename = '../data/input/Model1d_20_gen.in'  # generation


'''
input_arguments = sys.argv
if len(input_arguments) != 2:
    sys.exit('Two arguments must be specified for python: "python main.py ../data/input/inputfilename"; main.py is the main '
             'DFraM function and inputfilename is the input file written in ASCII format.')
else:
    input_filename = input_arguments[1]
    print('DFraM launched succesfully.')
'''

# advanced settings for verifications
rseed = 1  # number or 'Null', sets seed to generate the same random parameters
if rseed != 'Null':
    np.random.seed(rseed)
else:
    np.random.seed()
export_vtk_for_each_simulation = False  # saves vtk for each fracture network, generates huge amount of data


# secure that maximum density does not exceed a reasonable value
max_density = 3


# get revision id
revision_id = get_revision_id()

# process input file
[aux_inputs, project_name, save_intersections, x_max, x_min_remove, export_vtk_data,
 plot_fracture_size, rock_geometry, terminations, terminate_fractures, length_unit,
 x_boundary, y_boundary, z_boundary, regular_polygons, n_vert, n_figure, n_populations, pop_order, axes_ratio,
 optimization_mode, transmissivities] = read_and_process_input_file(input_filename)

plot_volume_data = False  # not recommended to switch to True, plots all fractures using pyplot, memory demanding
export_transmissivities_histograms_in_volume = True

#%% Verification mode
if len(rock_geometry) == 0:  # verification mode
    print('\nDFN verification mode is not yet implemented. Exiting.')
    sys.exit()
#  TODO
# =============================================================================
#     np.savetxt('_temp_project_name.txt', [project_name + '-DFN_verification'], fmt='%s')
#     np.savetxt('_temp_site_name.txt', [aux_inputs['site_name']], fmt='%s')
#     np.savetxt('_temp_outcrop_name.txt', aux_inputs['outcrop_name'], fmt='%s')
#     np.savetxt('_temp_outcrop_file.txt', aux_inputs['outcrop_file'], fmt='%s')
#     np.savetxt('_temp_outcrop_relevance.txt', aux_inputs['outcrop_relevance'], fmt='%s')
#     np.savetxt('_temp_outcrop_area.txt', aux_inputs['outcrop_area'], fmt='%s')
#     np.savetxt('_temp_verify_dir.txt', [aux_inputs['verify_dir']], fmt='%s')
#     try:
#         subprocess.call(["python3", "DFN_verify_main.py"])
#         print("Calling DFN_verify_main.py with python3")
#     except FileNotFoundError:
#         subprocess.call(["python", "DFN_verify_main.py"])
#         print("Calling DFN_verify_main.py with python")
#
#     os.remove('_temp_project_name.txt')
#     os.remove('_temp_site_name.txt')
#     os.remove('_temp_outcrop_name.txt')
#     os.remove('_temp_outcrop_file.txt')
#     os.remove('_temp_outcrop_relevance.txt')
#     os.remove('_temp_outcrop_area.txt')
#     os.remove('_temp_verify_dir.txt')
#
#     print('\nDFN verification finished.')
#     sys.exit()
# =============================================================================

#%% Optimization and generation mode
if optimization_mode:  # optimization mode
    optimize = aux_inputs['optimize']
    pops_to_optimize = aux_inputs['pops_to_optimize']
    optimized_parameter = aux_inputs['optimized_parameter']
    create_rves = aux_inputs['create_rves']
    min_trace_length_recorded = aux_inputs['min_trace_length_recorded']
    req_confidence = aux_inputs['req_confidence']
    eps_max_lengths = aux_inputs['eps_max_lengths']
    eps_max_density = aux_inputs['eps_max_density']
    max_iter = aux_inputs['max_iter']
    x_min = aux_inputs['x_min']
    outcrop_geometry = aux_inputs['outcrop_geometry']
    outcrop_name = aux_inputs['outcrop_name']
    outcrop_file = aux_inputs['outcrop_file']
    outcrop_relevance = aux_inputs['outcrop_relevance']
    outcrop_area = aux_inputs['outcrop_area']
    borehole_geometry = aux_inputs['borehole_geometry']
    borehole_name = aux_inputs['borehole_name']
    input_outcrops = aux_inputs['input_outcrops']
    alphas_01 = aux_inputs['alphas_01']
    alpha_0 = alphas_01[0]
    alpha_1 = alphas_01[1]
    fr_dens_initial = aux_inputs['fr_dens_initial']
    plot_borehole_data = aux_inputs['plot_borehole_data']
    max_n_simulations = aux_inputs['max_n_simulations']
    plot_outcrop_data = True
    of_p30 = aux_inputs['OF_P30']
    opt_n_sims = int(aux_inputs['opt_n_sims'])
    directional_stats_only = aux_inputs['directional_stats_only']
    plot_histograms_outcrops = aux_inputs['plot_histograms_outcrops']

    # check whether the OF for P30 optimization is based on number of traces or density on outcrops (P20)
    if of_p30 == 1.0:
        optimize_density = True
    else:
        optimize_density = False
    export_fractures_on_outcrops_data = True  # spreadsheet export of fracture traces

else:  # generation mode
    dips_input = aux_inputs['dips_input']
    strikes_input = aux_inputs['strikes_input']
    kappas_input = aux_inputs['kappas_input']
    x_mins = aux_inputs['x_mins_input']
    alphas = aux_inputs['alphas_input']
    fr_densities = aux_inputs['fr_densities_input']
    optimize = False
    create_rves = False
    export_fractures_on_outcrops_data = False
    outcrop_area, outcrop_geometry, outcrop_name, borehole_geometry, borehole_name, rgbs_input = [], [], [], [], [], []
    plot_outcrop_data = False
    plot_borehole_data = False
    directional_stats_only = False
    opt_n_sims = 1


# initiate output files
[time_of_start, out_path] = create_time_folder(project_name, optimization_mode)
initiate_output_file(out_path, project_name, time_of_start)
write_output(out_path, 'DFraM revision: %s.' % (revision_id))
write_output(out_path, 'No input errors detected in %s.' % input_filename)
if rseed != 'Null':
    write_output(out_path, 'Random seed: %f.' % (rseed))
else:
    write_output(out_path, 'Random seed: %s.' % (rseed))
if optimization_mode:
    if not directional_stats_only:
        iteration_log = open(out_path+'/iteration_log.txt', 'w')
        iteration_log.write('Population\tVariable\tIteration\tOptimized parameter\tOF value\tOF derivative value\t'
                            'Iteration step [%]\tNo. of simulations\n')
        iteration_log.close()


# correction of outcrops to make them planar
if optimization_mode:
    write_output(out_path, 'Recalculation of outcrops: least-square plane fit to make them planar.')
    write_output(out_path, 'Outcrop\tOriginal area\t --> New area')
    outcrop_normals = []
    for idx, outcrop in enumerate(outcrop_name):
        n_vertices = len(outcrop_geometry[idx]['x'])
        vertices = []
        for vi in range(n_vertices):
            vertices.append([outcrop_geometry[idx]['x'][vi], outcrop_geometry[idx]['y'][vi], outcrop_geometry[idx]['z'][vi]])
        new_vertices, new_area, outcrop_normal = recalculate_outcrops(out_path, outcrop, outcrop_area[idx], vertices)
        for vi in range(n_vertices):
            outcrop_geometry[idx]['x'][vi] = new_vertices[vi][0]*1.0
            outcrop_geometry[idx]['y'][vi] = new_vertices[vi][1]*1.0
            outcrop_geometry[idx]['z'][vi] = new_vertices[vi][2]*1.0
        outcrop_area[idx] = new_area*1.0
        outcrop_normals.append(outcrop_normal)


# gathering initial values for optimization
if optimization_mode:
    if optimized_parameter == 1:
        min_alpha = alpha_0[0] * 1.0
        alpha_trial_2 = alpha_1[0] * 1.0
        alpha_trial = [min_alpha, alpha_trial_2]
    elif optimized_parameter == 2:
        x_min_trial = [x_min[0], x_min[0]]
    fr_dens_trial = [fr_dens_initial[0], fr_dens_initial[0]]


# print calculated outcrop data in tabular format - just auxiliary initialization
print_outcrops = False


# get center of outcrops and generate RVE(s)
if create_rves:
    [rve_sizes, outcrop_centroids] = calculate_outcrop_centroids(outcrop_geometry, x_max)
    [rock_geometries, rock_total_volume] = generate_rves(outcrop_centroids, rve_sizes)
    write_output(out_path, '%d RVEs around outcrops created.' % (len(rock_geometries)))
else:
    rock_geometries = []
    if optimization_mode:
        write_output(out_path, 'Placing fractures in the entire rock volume (no RVEs around outcrops).')


# initiate input arrays to be optimized
if optimize:
    opt_n_sims = 1
    if optimized_parameter == 1:
        alphas = [alpha_0[0]]*n_populations
        if len(x_min) > 1:
            x_mins = copy.deepcopy(x_min)
        else:
            x_mins = [x_min[0]]*n_populations
        if len(fr_dens_initial) > 1:
            fr_densities = copy.deepcopy(fr_dens_initial)
        else:
            fr_densities = [fr_dens_initial[0]]*n_populations
    elif optimized_parameter == 2:
        if len(alpha_0) > 1:
            alphas = copy.deepcopy(alpha_0)
        else:
            alphas = [alpha_0[0]]*n_populations
        if len(x_min) > 1:
            x_mins = copy.deepcopy(x_min)
        else:
            x_mins = [x_min_trial[0]]*n_populations
        if len(fr_dens_initial) > 1:
            fr_densities = copy.deepcopy(fr_dens_initial)
        else:
            fr_densities = [fr_dens_initial[0]]*n_populations
elif optimization_mode:
    if len(alpha_0) > 1:
        alphas = copy.deepcopy(alpha_0)
    else:
        alphas = [alpha_0[0]]*n_populations
    if len(x_min) > 1:
        x_mins = copy.deepcopy(x_min)
    else:
        x_mins = [x_min[0]]*n_populations
    if len(fr_dens_initial) > 1:
        fr_densities = copy.deepcopy(fr_dens_initial)
    else:
        fr_densities = [fr_dens_initial[0]]*n_populations


# create project folders
if not directional_stats_only:
    sim_out_dir = out_path+'/images/simulations'
    if not os.path.exists(sim_out_dir):
        os.makedirs(sim_out_dir)


# load and process data from CSV files
initiate_measurements_file(out_path, project_name, time_of_start)
outcrops_names_obs_all, pops_obs_all, strikes_obs_all, dips_obs_all, lens_obs_all, fracture_outcrop_all = [], [], [], \
                                                                                                          [], [], []
for i in range(len(outcrop_area)):
    [observed_outcrop_data, n_figure, outcrop_names_obs_outcrop, pops_obs_outcrop, strikes_obs_outcrop,
     dips_obs_outcrop, lengths_obs_outcrop] = process_input_fractures(outcrop_file[i], out_path, outcrop_name[i], outcrop_area[i],
                                                                      outcrop_relevance[i], True, plot_histograms_outcrops,
                                                                      min_trace_length_recorded, length_unit, pop_order, n_figure,
                                                                      i, outcrop_normals)
    fracture_outcrop_all = fracture_outcrop_all+list(np.ones_like(strikes_obs_outcrop)*i)
    outcrops_names_obs_all = outcrops_names_obs_all+outcrop_names_obs_outcrop
    pops_obs_all = pops_obs_all+pops_obs_outcrop
    strikes_obs_all = strikes_obs_all+strikes_obs_outcrop
    dips_obs_all = dips_obs_all+dips_obs_outcrop
    lens_obs_all = lens_obs_all+lengths_obs_outcrop
    input_outcrops = process_outcrop_data(observed_outcrop_data, input_outcrops, length_unit, out_path)


# exporting outputs, creating output files
if optimization_mode:
    strikes_input, dips_input, kappas_input = calculate_mu_and_kappa(out_path, False, pops_obs_all, strikes_obs_all,
                                                                     dips_obs_all, fracture_outcrop_all, outcrop_normals)
    write_output(out_path, '%s/stats_observations.txt created.' % out_path)
    if directional_stats_only:
        write_output(out_path, 'Calculating only directional statistics.')
        # calculate fractures in each population
        counter_fractures_in_pops = np.zeros(n_populations)
        percentages = np.zeros(n_populations)
        for pop in pops_obs_all:
            counter_fractures_in_pops[int(pop - 1)] += 1
        write_optimization_results_dir_stats_only(out_path, strikes_input, dips_input, kappas_input,
                                                  counter_fractures_in_pops)
        write_output(out_path, 'Directional statistics calculated and exported.')
        sys.exit()

if export_fractures_on_outcrops_data:
    write_output(out_path, 'Exporting observed fracture traces for all outcrops to Excel spreadsheet.')
    write_xls_fracture_traces(out_path, 'observations', outcrops_names_obs_all, pops_obs_all, strikes_obs_all,
                              dips_obs_all, lens_obs_all)


# get inputs for the model from outcrop data
if optimization_mode:
    [rgbs_input, mean_lengths_input, n_fractures_input, dens_fractures_input, outcrop_w_input, mean_lengths_output,
     n_fractures_output, dens_fractures_output] = get_inputs(input_outcrops, n_populations)
else:
    for npop in range(n_populations):
        rgbs_input.append(clrs(npop))


# limiting the calculation to directional statistics only
if directional_stats_only:
    optimize = False
    opt_n_sims = 1


# determine how many optimizations are necessary
if optimize:  # optimization
    if pops_to_optimize[0] == 'all' or pops_to_optimize[0] == '"all"':
        opt_pops = range(n_populations)
        print_pops = ''
        for p in opt_pops:
            print_pops += '%d\t' % (p + 1)
        write_output(out_path, 'The following populations will be optimized: %s' % print_pops)
    else:
        opt_pops = []
        print_pops = ''
        for p in pops_to_optimize:
            opt_pops.append(int(p - 1))
            print_pops += '%d\t' % p
        write_output(out_path, 'The following populations will be optimized: %s' % print_pops)
    opt_vars = 2
    optimized = True
    if optimized_parameter == 1:
        result_at_alpha = [None]*len(alpha_trial)
    elif optimized_parameter == 2:
        result_at_x_min = [None]*len(x_min_trial)
    result_at_fr_dens = [None]*len(fr_dens_trial)
else:  # generation
    opt_pops = [0]
    opt_vars = 1
    optimized = False
    write_output(out_path, 'No optimization, only a DFN realization with the given input parameters.')
    if not optimize and opt_n_sims > 1:
        lens_all_pops = []
        strikes_all_pops = []
        dips_all_pops = []
        outcrops_for_lens_name = []
        outcrops_for_lens_area = []
        outcrops_for_lens_relevance = []
        for ii in range(n_populations):
            temp_array_1, temp_array_2, temp_array_3, temp_array_4, temp_array_5, temp_array_6 = [], [], [], [], [], []
            for jj in range(opt_n_sims):
                temp_array_1.append([])
                temp_array_2.append([])
                temp_array_3.append([])
                temp_array_4.append([])
                temp_array_5.append([])
                temp_array_6.append([])
            lens_all_pops.append(temp_array_1)
            strikes_all_pops.append(temp_array_2)
            dips_all_pops.append(temp_array_3)
            outcrops_for_lens_name.append(temp_array_4)
            outcrops_for_lens_area.append(temp_array_5)
            outcrops_for_lens_relevance.append(temp_array_6)

#%% Following code is executed for both generation and optimization mode
transmissivities_3d = []
for ii in range(n_populations):
    transmissivities_3d.append([])

for opt_pop in opt_pops:  # optimize populations first; from the most dominant to less dominant ones
    for opt_var in range(opt_vars):  # for each population optimize size of fractures (alpha), followed by their density
        iteration_number = 0
        if optimization_mode:
            eps = 1000
            errors_plot = []
            optimization_counter = 0
            optimization_plot = []
            of_values = []
            if opt_var == 0:
                if optimized_parameter == 1 and optimization_mode:
                    alpha_trial = [alpha_0[opt_pop] * 1.0, alpha_1[opt_pop] * 1.0]
                    if optimize:
                        write_output(out_path, 'Optimizing parameter alpha, population %d.' % (opt_pop+1))
                elif optimized_parameter == 2 and optimization_mode:
                    if len(x_min) > 1:
                        x_min_trial = [x_min[opt_pop], x_min[opt_pop]]
                    else:
                        x_min_trial = [x_min[0], x_min[0]]
                    if optimize:
                        write_output(out_path, 'Optimizing parameter x_min, population %d.' % (opt_pop+1))
            else:
                if len(fr_dens_initial) > 1:
                    fr_dens_trial = [fr_dens_initial[opt_pop], fr_dens_initial[opt_pop]]
                else:
                    fr_dens_trial = [fr_dens_initial[0], fr_dens_initial[0]]
                if optimize:
                    write_output(out_path, 'Optimizing fracture density (P30), population %d.' % (opt_pop+1))
                    if len(fr_dens_initial) > 1:
                        fr_densities[opt_pop] = fr_dens_initial[opt_pop]
                    else:
                        fr_densities[opt_pop] = fr_dens_initial[0]

        update_variable = True
        while update_variable:
            iteration_number += 1
            if opt_var == 0:
                if optimize:
                    if optimized_parameter == 1:
                        write_output(out_path, 'Optimizing parameter alpha for population %d, iteration no. %d.' %
                                     (opt_pop+1, iteration_number-1))
                        otp_var_name = 'alpha'
                    elif optimized_parameter == 2:
                        write_output(out_path, 'Optimizing parameter x_min for population %d, iteration no. %d.' %
                                     (opt_pop+1, iteration_number-1))
                        otp_var_name = 'x_min'
            else:
                if optimize:
                    write_output(out_path, 'Optimizing fracture density (P30) for population %d, iteration no. %d.' %
                                 (opt_pop+1, iteration_number-1))
                    otp_var_name = 'P30'

            n_simulations = 0
            if optimization_mode:
                # prepare arrays for gathering results
                [cum_length_outcrop, dens_fractures_outcrop, n_fractures_outcrop] = \
                    prepare_arrays_for_results(outcrop_geometry, n_populations)
                mean_length_outcrop = copy.deepcopy(cum_length_outcrop)

            # calculate number of fractures for all populations
            if create_rves:
                if optimize and not terminate_fractures:
                    n_fractures = calculate_n_fractures_rve([fr_densities[opt_pop]], rock_total_volume)
                else:
                    n_fractures = calculate_n_fractures_rve(fr_densities, rock_total_volume)
            else:
                n_fractures = calculate_n_fractures(fr_densities, x_boundary, y_boundary, z_boundary)

            # class Fractures(population, n_fractures, x_range, y_range, z_range, powerlaw constants: [x_min, alpha],
            # aspect_ratio, dummy_var, orientation: [strike_mean, dip_mean, kappa], in_plane_rotation_range,
            # n_poly_range, rgb, rve_geometries)
            if optimize and not terminate_fractures:
                group = [None]
                write_output(out_path, 'Generating list of fractures, population %d.' % (opt_pop + 1))
                if len(n_fractures) > 1:
                    n_fractures_cur = n_fractures[opt_pop]
                else:
                    n_fractures_cur = n_fractures[0]
                if optimized_parameter == 1:
                    group[0] = Fractures(opt_pop+1, n_fractures_cur, x_boundary[opt_pop], y_boundary[opt_pop],
                                         z_boundary[opt_pop], [x_mins[opt_pop], alphas[opt_pop], x_max], axes_ratio, [0, 1],
                                         [strikes_input[opt_pop], dips_input[opt_pop], kappas_input[opt_pop]], [0, 90],
                                         n_vert, rgbs_input[opt_pop], rock_geometries, transmissivities[opt_pop])
                elif optimized_parameter == 2:
                    group[0] = Fractures(opt_pop+1, n_fractures_cur, x_boundary[opt_pop], y_boundary[opt_pop],
                                         z_boundary[opt_pop], [x_mins[opt_pop], alphas[opt_pop], x_max],
                                         axes_ratio, [0, 1],
                                         [strikes_input[opt_pop], dips_input[opt_pop], kappas_input[opt_pop]], [0, 90],
                                         n_vert, rgbs_input[opt_pop], rock_geometries, transmissivities[opt_pop])
            else:
                # !!! construct Fractures class objects (both for generation and optimization mode)
                group = [None]*n_populations
                for g in range(n_populations):
                    write_output(out_path, 'Generating list of fractures, population %d.' % (g + 1))
                    if not optimization_mode:
                        try:
                            group[g] = Fractures(g+1, n_fractures[g], x_boundary[g], y_boundary[g], z_boundary[g],
                                                 [x_mins[g], alphas[g], x_max], axes_ratio, [0, 1],
                                                 [strikes_input[g], dips_input[g], kappas_input[g]], [0, 90], n_vert,
                                                 rgbs_input[g], rock_geometries, transmissivities[g])
                        except:
                            sys.exit('Not all populations are defined properly, insufficient number of dips, strikes, '
                                     'kappas, x_mins, alphas or fracture densities given!')
                    else:
                        if optimized_parameter == 1:
                            group[g] = Fractures(g+1, n_fractures[g], x_boundary[g], y_boundary[g], z_boundary[g],
                                                 [x_mins[opt_pop], alphas[g], x_max], axes_ratio, [0, 1],
                                                 [strikes_input[g], dips_input[g], kappas_input[g]], [0, 90], n_vert,
                                                 rgbs_input[g], rock_geometries, transmissivities[g])
                        else:
                            group[g] = Fractures(g+1, n_fractures[g], x_boundary[g], y_boundary[g], z_boundary[g],
                                                 [x_mins[g], alphas[opt_pop], x_max], axes_ratio, [0, 1],
                                                 [strikes_input[g], dips_input[g], kappas_input[g]], [0, 90], n_vert,
                                                 rgbs_input[g], rock_geometries, transmissivities[g])

            if optimization_mode:
                if opt_var == 0:
                    if optimized_parameter == 1:
                        optimization_plot.append(alphas[opt_pop])
                    elif optimized_parameter == 2:
                        optimization_plot.append(x_mins[opt_pop])
                else:
                    optimization_plot.append(fr_densities[opt_pop])

            enough_simulations = False
            while not enough_simulations:
                n_simulations += 1
                if optimize:
                    print('Starting simulation number: %d.' % n_simulations)
                tic()
                # !!! generate fractures' polygons
                # method generate_fractures() belongs to Fractures class defined in fracturegen.py
                generated_fractures = []
                for g in range(len(group)):
                    generated_fractures = generated_fractures + group[g].generate_fractures()
                write_output(out_path, 'Generating polygons representing the fractures (%d in total).'
                             % len(generated_fractures))

                # generate individual polygons representing fractures
                polygons = []
                n_generated_fractures = len(generated_fractures)
                prev_value = 101  # only auxiliary for the counter
                for idx, fr in enumerate(generated_fractures):
                    [xT, yT, zT, poly_area] = generate_poly(fr['a'], fr['b'], fr['n_points_poly'], fr['x'], fr['y'],
                                                            fr['z'], fr['strike'], fr['dip'], fr['in_plane_rotation'],
                                                            regular_polygons)
                    # save the polygon
                    polygons.append({'xT': xT, 'yT': yT, 'zT': zT, 'strike': fr['strike'], 'dip': fr['dip'],
                                     'rgb': fr['rgb'], 'population': fr['population'], 'intersection': [0],
                                     'area_orig': poly_area, 'area_new': poly_area,
                                     'transmissivity': fr['transmissivity'], 'ax1': fr['a'], 'ax2': fr['b'],
                                     'xc': fr['x'], 'yc': fr['y'], 'zc': fr['z']})

                    if export_transmissivities_histograms_in_volume and not optimize:
                        transmissivities_3d[fr['population'] - 1].append(fr['transmissivity'])

                    if (idx / n_generated_fractures) * 100 % 10 <= (1 / n_generated_fractures * 100) and idx != 0:
                        cur_value = int(idx / n_generated_fractures * 100)
                        msg = 'Generating fracture polygons, %.0f%% completed.' % (idx / n_generated_fractures * 100)
                        if prev_value != cur_value:
                            prev_value = cur_value * 1
                            write_output(out_path, msg)

                # termination of generated fractures
                intersections = []  # all intersections of polygons in the rock volume
                n_polygons = len(polygons)
                prev_value = 101
                if terminate_fractures:
                    write_output(out_path, 'Terminating fractures.')
                    for idx, fr in enumerate(polygons):
                        if save_intersections:
                            [polygons, intersections] = poly_poly_intersections_2(fr, idx, polygons, terminations,
                                                                                  intersections)
                        else:
                            polygons = poly_poly_intersections_2_no_intersections(fr, idx, polygons, terminations)

                        if (idx / n_polygons) * 100 % 10 <= (1 / n_polygons * 100) and idx != 0:
                            cur_value = int(idx / n_polygons * 100)
                            msg = 'Terminating fractures, %.0f%% completed.' % (idx / n_polygons * 100)
                            if prev_value != cur_value:
                                prev_value = cur_value * 1
                                write_output(out_path, msg)

                    # "intersection" field in polygons contains numbers of intersections,
                    # i.e. polygons[n_polygon]['intersection'] == [0, 10] contains 1st and 11th intersection:
                    # intersections[0] and intersections[10]
                    if save_intersections:
                        write_output(out_path, 'Checking intersections.')
                        [polygons, intersections] = check_intersections(polygons, intersections)
                    write_output(out_path, 'Removing small polygons.')
                    [polygons, intersections] = remove_small_polygons(polygons, intersections, x_min_remove,
                                                                      save_intersections)

                enough_simulations = True
                if optimization_mode:
                    for idx, outcrop in enumerate(outcrop_geometry):
                        [polygon_2d, intersections_2d,
                         virt_outcrop_area, _] = poly_poly_intersections(outcrop, polygons, min_trace_length_recorded)
                        if optimize and not terminate_fractures:
                            min_pop = opt_pop
                            max_pop = opt_pop+1
                        else:
                            min_pop = 0
                            max_pop = n_populations

                        for current_population in range(min_pop, max_pop):
                            x_2d_start, x_2d_end, y_2d_start, y_2d_end, z_2d_start, z_2d_end, dips_2d, strikes_2d, \
                            outcrop_names_2d, outcrop_areas_2d, outcrop_relevances_2d = [], [], [], [], [], [], [], [], \
                                [], [], []
                            for fr in intersections_2d:
                                if fr['population'] == current_population+1:
                                    x_2d_start.append(fr['x'][0])
                                    x_2d_end.append(fr['x'][1])
                                    y_2d_start.append(fr['y'][0])
                                    y_2d_end.append(fr['y'][1])
                                    z_2d_start.append(0)
                                    z_2d_end.append(0)
                                    dips_2d.append(fr['dip'])
                                    strikes_2d.append(fr['strike'])
                                    outcrop_names_2d.append(outcrop_name[idx])
                                    outcrop_areas_2d.append(outcrop_area[idx])
                                    outcrop_relevances_2d.append(outcrop_w_input[idx])

                            if len(x_2d_start) > 0:
                                [cum_length_2d, density_fractures_virt_outcrop, n_fractures_virt_outcrop] = \
                                    stats_reduced_2(x_2d_start, x_2d_end, y_2d_start, y_2d_end, z_2d_start, z_2d_end,
                                                    virt_outcrop_area)

                                dens_fractures_outcrop[idx][current_population].append(density_fractures_virt_outcrop)
                                cum_length_outcrop[idx][current_population].append(cum_length_2d)
                                n_fractures_outcrop[idx][current_population].append(n_fractures_virt_outcrop)

                                if not optimize and opt_n_sims > 1:
                                    cur_fractures = get_lengths_of_traces(x_2d_start, x_2d_end, y_2d_start, y_2d_end,
                                                                          z_2d_start, z_2d_end)
                                    for ifr, fracture in enumerate(cur_fractures):
                                        lens_all_pops[current_population][n_simulations-1].append(fracture)
                                        strikes_all_pops[current_population][n_simulations-1].append(strikes_2d[ifr])
                                        dips_all_pops[current_population][n_simulations-1].append(dips_2d[ifr])
                                        outcrops_for_lens_name[current_population][n_simulations-1].append(outcrop_names_2d[ifr])
                                        outcrops_for_lens_area[current_population][n_simulations-1].append(outcrop_areas_2d[ifr])
                                        outcrops_for_lens_relevance[current_population][n_simulations-1].append(outcrop_relevances_2d[ifr])

                            else:
                                dens_fractures_outcrop[idx][current_population].append(0.0)
                                cum_length_outcrop[idx][current_population].append(0.0)
                                n_fractures_outcrop[idx][current_population].append(0.0)

                    # confidence([set of values], required_confidence - 0.9 = 90%)
                    if opt_var == 0:
                        enough_simulations, mean_length_outcrop = confidence3(out_path, 'mean length of fracture traces',
                                                                              current_population, mean_length_outcrop,
                                                                              cum_length_outcrop, n_fractures_outcrop,
                                                                              outcrop_relevance, req_confidence/100,
                                                                              optimize)
                    else:
                        if not confidence2(out_path, 'mean density of fracture traces', current_population,
                                           dens_fractures_outcrop, outcrop_relevance, req_confidence/100, optimize):
                            enough_simulations = False

                    if export_vtk_for_each_simulation and optimize:
                        aux_name = 'pop_%d-%s-iter_%d-simul_%d' % (opt_pop + 1, otp_var_name, optimization_counter,
                                                                   n_simulations)
                        outcrop_intersections_vtk = [None] * len(outcrop_geometry)
                        write_calculation_header(out_path, project_name, time_of_start)

                        strikes_2d_all, dips_2d_all, pops_2d_all, lens_2d_all, outcrops_names_all, \
                        transmissivities_all = [], [], [], [], [], []
                        for idx, outcrop in enumerate(outcrop_geometry):
                            outcrop_intersections_vtk_temp = []
                            [polygon_2d, intersections_2d, virt_outcrop_area,
                             outcrop_intersections_3d] = poly_poly_intersections(outcrop, polygons,
                                                                                 min_trace_length_recorded)
                            for current_population in range(n_populations):
                                try:
                                    outcrop_intersections_vtk_temp_temp = []
                                    strikes_2d, dips_2d, x_2d_start, x_2d_end, y_2d_start, y_2d_end, z_2d_start, \
                                    z_2d_end = [], [], [], [], [], [], [], []
                                    color_chosen = False
                                    for fridx, fr in enumerate(intersections_2d):
                                        if fr['population'] == current_population + 1:
                                            x_3d = [outcrop_intersections_3d[fridx]['x'][0],
                                                    outcrop_intersections_3d[fridx]['x'][1]]
                                            y_3d = [outcrop_intersections_3d[fridx]['y'][0],
                                                    outcrop_intersections_3d[fridx]['y'][1]]
                                            z_3d = [outcrop_intersections_3d[fridx]['z'][0],
                                                    outcrop_intersections_3d[fridx]['z'][1]]
                                            outcrop_intersections_vtk_temp_temp.append(
                                                {'x': x_3d, 'y': y_3d, 'z': z_3d})
                                    outcrop_intersections_vtk_temp.append(outcrop_intersections_vtk_temp_temp)
                                except TypeError:
                                    pass
                            outcrop_intersections_vtk[idx] = outcrop_intersections_vtk_temp
                        write_output(out_path, 'Exporting VTK files.')
                        vtk_export_testing(polygons, intersections, rock_geometry, outcrop_geometry, outcrop_name,
                                           borehole_geometry, borehole_name, outcrop_intersections_vtk,
                                           out_path + '/vtk', aux_name)

                    if n_simulations == max_n_simulations:
                        enough_simulations = True
                        write_output(out_path, 'Number of simulations exceeds the allowed limit, the optimization will '
                                               'continue regardless the reached confidence level.')

                    if enough_simulations:
                        for current_population in range(n_populations):
                            for idx in range(len(outcrop_geometry)):
                                if len(cum_length_outcrop[idx][current_population]) > 0.0:
                                    mean_lengths_output[current_population][idx] = \
                                        np.array(mean_length_outcrop[idx][current_population]) * 1.0
                                    dens_fractures_output[current_population][idx] = \
                                        np.mean(dens_fractures_outcrop[idx][current_population])
                                    n_fractures_output[current_population][idx] = \
                                        np.mean(n_fractures_outcrop[idx][current_population])
                                else:
                                    mean_lengths_output[current_population][idx] = 0.0
                                    dens_fractures_output[current_population][idx] = 0.0
                                    n_fractures_output[current_population][idx] = 0.0
                    if not optimize:
                        if opt_n_sims == n_simulations:
                            enough_simulations = True
                        update_variable = False
                    toc()

                    if enough_simulations:
                        if optimize:
                            write_output(out_path, 'Sufficient confidence in results after %d simulations.' % n_simulations)
                            if opt_var == 0:
                                if optimized_parameter == 1:
                                    if optimization_counter < len(alpha_trial)-1:
                                        of_value, of_derivative, iter_1_value = of_lengths(mean_lengths_input[opt_pop],
                                                                                           mean_lengths_output[opt_pop],
                                                                                           outcrop_w_input)
                                        iteration_log = open(out_path+'/iteration_log.txt', 'a')
                                        iteration_log.write('%d\tAlpha\t%d\t%.6f\t%.6f\t%.6f\t x \t%d\n' % (opt_pop+1,
                                                                                                            optimization_counter,
                                                                                                            alpha_trial[optimization_counter],
                                                                                                            of_value,
                                                                                                            of_derivative,
                                                                                                            n_simulations))
                                        iteration_log.close()
                                        result_at_alpha[optimization_counter] = of_derivative * 1.0
                                        write_output(out_path, 'Initial calculation with alpha = %.3f done' % (alpha_trial[optimization_counter]))
                                        optimization_counter += 1
                                        alphas[opt_pop] = alpha_trial[optimization_counter] * 1.0
                                        alpha_previous = alpha_0[opt_pop] * 1.0
                                        errors_plot.append(1.0)
                                        of_values.append(of_value)
                                    else:
                                        eps = abs((alpha_previous - alphas[opt_pop]) / alpha_previous)
                                        of_value, of_derivative, iter_1_value = of_lengths(mean_lengths_input[opt_pop],
                                                                                           mean_lengths_output[opt_pop],
                                                                                           outcrop_w_input)
                                        iteration_log = open(out_path+'/iteration_log.txt', 'a')
                                        iteration_log.write('%d\tAlpha\t%d\t%.6f\t%.6f\t%.6f\t%.6f\t%d\n' % (opt_pop+1,
                                                                                                             optimization_counter,
                                                                                                             alphas[opt_pop],
                                                                                                             of_value,
                                                                                                             of_derivative,
                                                                                                             eps * 100,
                                                                                                             n_simulations))
                                        iteration_log.close()
                                        alpha_previous = alphas[opt_pop] * 1.0
                                        if optimization_counter == len(alpha_trial)-1:
                                            result_at_alpha[optimization_counter] = of_derivative * 1.0
                                            alphas[opt_pop] = min(alphas[opt_pop]*3, linear_interp(alpha_trial, result_at_alpha, 1.0))
                                        else:
                                            idx_max = result_at_alpha.index(max(result_at_alpha))
                                            result_at_alpha[idx_max] = of_derivative*1.0
                                            alpha_trial[idx_max] = alphas[opt_pop]*1.0
                                            alphas[opt_pop] = min(alphas[opt_pop]*3, linear_interp(alpha_trial, result_at_alpha, 1.0))
                                        errors_plot.append(eps)
                                        of_values.append(of_value)
                                        write_output(out_path, 'Change in alpha = %.3f%% (alpha = %.3f).' % (eps*100, alpha_previous))
                                        if (eps < eps_max_lengths/100 or iteration_number == max_iter) and iteration_number > 2:
                                            if iteration_number == max_iter:
                                                write_output(out_path, 'Number of iterations exceeded the limit set by the user.')
                                            _, min_idx = min((val, min_idx) for (min_idx, val) in enumerate(of_values))
                                            alphas[opt_pop] = optimization_plot[min_idx]*1.0
                                            optimization_counter = 0
                                            write_output(out_path, 'Optimum parameter alpha for population %d found, %.3f. \n'
                                                         % (opt_pop+1, alphas[opt_pop]))
                                            output_file = '%s/alpha_iteration-population_%s.png' % (sim_out_dir, opt_pop + 1)
                                            plot_iteration_alpha(optimization_plot, errors_plot, opt_pop+1, rgbs_input[opt_pop],
                                                                 min_idx, output_file, n_figure)
                                            n_figure += 1
                                            output_file_of = '%s/OF_alpha_iteration-population_%s.png' % (sim_out_dir, opt_pop + 1)
                                            plot_iteration_of('alpha', optimization_plot, of_values,
                                                              rgbs_input[opt_pop], min_idx, output_file_of, n_figure)
                                            n_figure += 1
                                            result_at_alpha = [None]*len(alpha_trial)
                                            update_variable = False
                                        else:
                                            if optimization_counter > len(alpha_trial)-1:
                                                if of_derivative == 0:
                                                    fr_densities[opt_pop] *= 1.2
                                                    alphas[opt_pop] = 2.6
                                            optimization_counter += 1
                                elif optimized_parameter == 2:
                                    if optimization_counter < len(x_min_trial)-1:
                                        of_value, of_derivative, iter_1_value = of_lengths(mean_lengths_input[opt_pop],
                                                                                           mean_lengths_output[opt_pop],
                                                                                           outcrop_w_input)
                                        iteration_log = open(out_path+'/iteration_log.txt', 'a')
                                        iteration_log.write('%d\tx_min\t%d\t%.6f\t%.6f\t%.6f\t x \t%d\n' % (opt_pop+1,
                                                                                                            optimization_counter,
                                                                                                            x_min_trial[optimization_counter],
                                                                                                            of_value,
                                                                                                            of_derivative,
                                                                                                            n_simulations))
                                        iteration_log.close()
                                        result_at_x_min[optimization_counter] = of_derivative
                                        write_output(out_path, 'Initial calculation with x_min = %.3f done' % (x_min_trial[optimization_counter]))
                                        x_min_previous = x_min_trial[optimization_counter] * 1.0
                                        optimization_counter += 1
                                        x_min_trial[optimization_counter] = x_min_trial[optimization_counter-1]*(iter_1_value)
                                        x_mins[opt_pop] = min(x_mins[opt_pop]*3, x_min_trial[optimization_counter])
                                        errors_plot.append(1.0)
                                        of_values.append(of_value)
                                    else:
                                        eps = abs((x_min_previous - x_mins[opt_pop]) / x_min_previous)
                                        of_value, of_derivative, iter_1_value = of_lengths(mean_lengths_input[opt_pop],
                                                                                           mean_lengths_output[opt_pop],
                                                                                           outcrop_w_input)
                                        iteration_log = open(out_path+'/iteration_log.txt', 'a')
                                        iteration_log.write('%d\tx_min\t%d\t%.6f\t%.6f\t%.6f\t%.6f\t%d\n' % (opt_pop+1,
                                                                                                             optimization_counter,
                                                                                                             x_mins[opt_pop],
                                                                                                             of_value,
                                                                                                             of_derivative,
                                                                                                             eps * 100,
                                                                                                             n_simulations))
                                        iteration_log.close()
                                        x_min_previous = x_mins[opt_pop]
                                        if optimization_counter == len(x_min_trial)-1:
                                            result_at_x_min[optimization_counter] = of_derivative
                                            x_mins[opt_pop] = min(x_mins[opt_pop]*5, linear_interp(x_min_trial, result_at_x_min, 0.00001))
                                        else:
                                            idx_max = result_at_x_min.index(max(result_at_x_min))
                                            result_at_x_min[idx_max] = of_derivative
                                            x_min_trial[idx_max] = x_mins[opt_pop]
                                            x_mins[opt_pop] = min(x_mins[opt_pop]*5, linear_interp(x_min_trial, result_at_x_min, 0.00001))
                                        errors_plot.append(eps)
                                        of_values.append(of_value)
                                        write_output(out_path, 'Change in x_min = %.3f%% (x_min = %.3f).' % (eps*100, x_min_previous))
                                        if eps < eps_max_lengths/100 or iteration_number == max_iter:
                                            if iteration_number == max_iter:
                                                write_output(out_path, 'Number of iterations exceeded the limit set by the user.')
                                            _, min_idx = min((val, min_idx) for (min_idx, val) in enumerate(of_values))
                                            x_mins[opt_pop] = optimization_plot[min_idx]*1.0
                                            optimization_counter = 0
                                            write_output(out_path, 'Optimum parameter x_min for population %d found, %.6f. \n'
                                                         % (opt_pop+1, x_mins[opt_pop]))
                                            output_file = '%s/x_min_iteration-population_%s.png' % (sim_out_dir, opt_pop+1)
                                            plot_iteration_x_min(optimization_plot, errors_plot, opt_pop+1, rgbs_input[opt_pop],
                                                                 min_idx, output_file, n_figure)
                                            n_figure += 1
                                            output_file_of = '%s/OF_x_min_iteration-population_%s.png' % (sim_out_dir, opt_pop + 1)
                                            plot_iteration_of('x_min', optimization_plot, of_values,
                                                              rgbs_input[opt_pop], min_idx, output_file_of, n_figure)
                                            n_figure += 1
                                            result_at_x_min = [None]*len(x_min_trial)
                                            update_variable = False
                                        else:
                                            if optimization_counter > len(x_min_trial)-1:
                                                if of_derivative == 0:
                                                    fr_densities[opt_pop] *= 1.2
                                                    x_mins[opt_pop] = x_min_trial[1]
                                            optimization_counter += 1
                            else:
                                if optimization_counter < len(fr_dens_trial)-1:
                                    of_value, of_derivative, iter_1_value = of_densities(dens_fractures_input[opt_pop],
                                                                                         dens_fractures_output[opt_pop],
                                                                                         n_fractures_input[opt_pop],
                                                                                         n_fractures_output[opt_pop],
                                                                                         optimize_density,
                                                                                         outcrop_w_input)
                                    iteration_log = open(out_path+'/iteration_log.txt', 'a')
                                    iteration_log.write('%d\tP30\t%d\t%.6f\t%.6f\t%.6f\t x \t%d\n' % (opt_pop+1,
                                                                                                      optimization_counter,
                                                                                                      fr_densities[opt_pop],
                                                                                                      of_value,
                                                                                                      of_derivative,
                                                                                                      n_simulations))
                                    iteration_log.close()
                                    result_at_fr_dens[optimization_counter] = of_derivative * 1.0
                                    write_output(out_path, 'Initial calculation with P30 = %.3f done' % (fr_densities[opt_pop]))
                                    fr_dens_previous = fr_densities[opt_pop] * 1.0
                                    optimization_counter += 1
                                    fr_dens_trial[optimization_counter] = fr_dens_trial[optimization_counter-1]*(iter_1_value)
                                    fr_densities[opt_pop] = fr_dens_trial[optimization_counter]
                                    errors_plot.append(1.0)
                                    of_values.append(of_value)
                                else:
                                    eps = abs((fr_dens_previous - fr_densities[opt_pop]) / fr_dens_previous)
                                    of_value, of_derivative, iter_1_value = of_densities(dens_fractures_input[opt_pop],
                                                                                         dens_fractures_output[opt_pop],
                                                                                         n_fractures_input[opt_pop],
                                                                                         n_fractures_output[opt_pop],
                                                                                         optimize_density,
                                                                                         outcrop_w_input)
                                    iteration_log = open(out_path+'/iteration_log.txt', 'a')
                                    iteration_log.write('%d\tP30\t%d\t%.6f\t%.6f\t%.6f\t%.6f\t%d\n' % (opt_pop+1,
                                                                                                       optimization_counter,
                                                                                                       fr_densities[opt_pop],
                                                                                                       of_value,
                                                                                                       of_derivative,
                                                                                                       eps * 100,
                                                                                                       n_simulations))
                                    iteration_log.close()
                                    fr_dens_previous = fr_densities[opt_pop] * 1.0
                                    if optimization_counter == len(fr_dens_trial)-1:
                                        result_at_fr_dens[optimization_counter] = of_derivative * 1.0
                                        fr_densities[opt_pop] = min(fr_densities[opt_pop]*3,
                                                                    linear_interp(fr_dens_trial, result_at_fr_dens, 0.00001))
                                    else:
                                        idx_max = result_at_fr_dens.index(max(result_at_fr_dens))
                                        result_at_fr_dens[idx_max] = of_derivative * 1.0
                                        fr_dens_trial[idx_max] = fr_densities[opt_pop]
                                        fr_densities[opt_pop] = min(fr_densities[opt_pop]*3,
                                                                    linear_interp(fr_dens_trial, result_at_fr_dens,
                                                                                  0.001))
                                    errors_plot.append(eps)
                                    of_values.append(of_value)
                                    if optimize_density:
                                        write_output(out_path, 'Change in fracture density P30 = %.3f%% (P30 = %.3f).'
                                                     % (eps*100, fr_dens_previous))
                                    if eps < eps_max_density/100 or iteration_number == max_iter:
                                        optimization_counter = 0
                                        if iteration_number == max_iter:
                                            write_output(out_path, 'Number of iterations exceeded the limit set by the user.')
                                        _, min_idx = min((val, min_idx) for (min_idx, val) in enumerate(of_values))
                                        fr_densities[opt_pop] = optimization_plot[min_idx]*1.0
                                        write_output(out_path, 'Optimum fracture density P30 for population %d found, %.6f. \n'
                                                     % (opt_pop+1, fr_densities[opt_pop]))
                                        output_file = '%s/fr_dens_P30_iteration-population_%s.png' % (sim_out_dir, opt_pop+1)
                                        plot_iteration_fr_dens(optimization_plot, optimize_density, errors_plot, opt_pop+1,
                                                               rgbs_input[opt_pop], min_idx, output_file, n_figure)
                                        n_figure += 1
                                        output_file_of = '%s/OF_fr_dens_P30_iteration-population_%s.png' % (sim_out_dir, opt_pop + 1)
                                        plot_iteration_of('P30', optimization_plot, of_values,
                                                          rgbs_input[opt_pop], min_idx, output_file_of, n_figure)
                                        n_figure += 1
                                        result_at_fr_dens = [None]*len(fr_dens_trial)
                                        update_variable = False
                                    else:
                                        if optimization_counter > len(fr_dens_trial)-1:
                                            if of_derivative == 0:
                                                fr_densities[opt_pop] *= 1.2
                                        optimization_counter += 1
                                if fr_densities[opt_pop] > max_density:
                                    if len(fr_dens_initial) > 1:
                                        fr_densities[opt_pop] = fr_dens_initial[opt_pop]*1.3
                                    else:
                                        fr_densities[opt_pop] = fr_dens_initial[0]*1.3

                            if opt_pop == n_populations-1 and opt_var == 1:
                                if not update_variable:
                                    update_variable = True
                                    optimize = False
                                    write_output(out_path, 'Generating final network, all parameters optimized.')
                                    write_optimization_results_2(out_path, False, True, fr_densities[opt_pop],
                                                                 x_mins[opt_pop], alphas[opt_pop],
                                                                 strikes_input[opt_pop], dips_input[opt_pop],
                                                                 kappas_input[opt_pop])
                            else:
                                if opt_var == 1:
                                    if not update_variable:
                                        if opt_pop == 0:
                                            write_optimization_results_2(out_path, True, False, fr_densities[opt_pop],
                                                                         x_mins[opt_pop], alphas[opt_pop],
                                                                         strikes_input[opt_pop], dips_input[opt_pop],
                                                                         kappas_input[opt_pop])
                                        else:
                                            write_optimization_results_2(out_path, False, False, fr_densities[opt_pop],
                                                                         x_mins[opt_pop], alphas[opt_pop],
                                                                         strikes_input[opt_pop], dips_input[opt_pop],
                                                                         kappas_input[opt_pop])
                else:
                    update_variable = False

#%% Output plots and vtk files
outcrop_intersections_vtk = [None]*len(outcrop_geometry)
if plot_outcrop_data:
    write_output(out_path, 'Plotting outcrop data.')
    write_calculation_header(out_path, project_name, time_of_start)

    strikes_2d_all, dips_2d_all, pops_2d_all, lens_2d_all, outcrops_names_all, transmissivities_all = [], [], [], [], \
                                                                                                      [], []
    for idx, outcrop in enumerate(outcrop_geometry):
        if print_outcrops:
            f_outcrop = open(out_path+'/'+outcrop_name[idx]+'.txt', 'w')
            fr_counter = 0
        outcrop_intersections_vtk_temp = []
        [polygon_2d, intersections_2d, virt_outcrop_area,
         outcrop_intersections_3d] = poly_poly_intersections(outcrop, polygons, min_trace_length_recorded)
        plot_polygon_polygons_intersections(polygon_2d, intersections_2d, outcrop_name[idx], sim_out_dir, n_figure)
        n_figure += 1
        for current_population in range(n_populations):
            try:
                outcrop_intersections_vtk_temp_temp = []
                strikes_2d, dips_2d, x_2d_start, x_2d_end, y_2d_start, y_2d_end, z_2d_start, z_2d_end = \
                    [], [], [], [], [], [], [], []
                color_chosen = False
                for fridx, fr in enumerate(intersections_2d):
                    if fr['population'] == current_population+1:
                        if not color_chosen:
                            rgbs_2d = (fr['rgb'])
                            color_chosen = True
                        strikes_2d.append(fr['strike'])
                        dips_2d.append(fr['dip'])
                        x_2d_start.append(fr['x'][0])
                        x_2d_end.append(fr['x'][1])
                        y_2d_start.append(fr['y'][0])
                        y_2d_end.append(fr['y'][1])
                        z_2d_start.append(0)
                        z_2d_end.append(0)

                        x_3d = [outcrop_intersections_3d[fridx]['x'][0], outcrop_intersections_3d[fridx]['x'][1]]
                        y_3d = [outcrop_intersections_3d[fridx]['y'][0], outcrop_intersections_3d[fridx]['y'][1]]
                        z_3d = [outcrop_intersections_3d[fridx]['z'][0], outcrop_intersections_3d[fridx]['z'][1]]
                        outcrop_intersections_vtk_temp_temp.append({'x': x_3d, 'y': y_3d, 'z': z_3d})
                        if print_outcrops:
                            fr_counter += 1
                            new_line = '%d\t%d\t%d\t%d\t%d\t0\t0\t0\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\n' \
                                       % (fr_counter, fr['dip'], fr['strike']+90, fr['strike'], fr['population'],
                                          x_3d[0], y_3d[0], z_3d[0], x_3d[1], y_3d[1], z_3d[1],
                                          np.sqrt((x_3d[1]-x_3d[0])**2+(y_3d[1]-y_3d[0])**2+(z_3d[1]-z_3d[0])**2))
                            f_outcrop.write(new_line)
                outcrop_intersections_vtk_temp.append(outcrop_intersections_vtk_temp_temp)

                [strike_2d_mean, dip_2d_mean, kappa_2d, mean_length_2d, n_fractures_2d, fracture_length_2d,
                 cumulative_fracture_length_2d] = stats2(dips_2d, strikes_2d, x_2d_start, x_2d_end, y_2d_start, y_2d_end,
                                                         z_2d_start, z_2d_end, min_trace_length_recorded)
                fracture_density_P21 = cumulative_fracture_length_2d/virt_outcrop_area
                fracture_density_P20 = n_fractures_2d/virt_outcrop_area

                title = 'Simulations, fracture population no. %d' % (current_population+1)
                output_file = '%s/histogram-%s-population-%d.png' % (sim_out_dir, outcrop_name[idx], current_population+1)
                output_file_2 = '%s/logplot-%s-population-%d.png' % (sim_out_dir, outcrop_name[idx], current_population+1)
                hist_plots(fracture_length_2d, 15, rgbs_2d, title, 'Trace length', length_unit,
                           'Number of fractures [-]', output_file, n_figure)
                n_figure += 1
                log_plots(fracture_length_2d, 15, rgbs_2d, title, length_unit, output_file_2, n_figure)
                n_figure += 1

                if not optimize:
                    write_calculation_results(out_path, current_population, dip_2d_mean, strike_2d_mean, kappa_2d,
                                              mean_length_2d, length_unit, fracture_density_P21, x_2d_start,
                                              outcrop_name[idx], fracture_density_P20)
            except:
                write_output(out_path, 'There are no traces of population %d on outcrop %s.'
                             % (current_population+1, outcrop_name[idx]))

        outcrop_intersections_vtk[idx] = outcrop_intersections_vtk_temp
        if print_outcrops:
            f_outcrop.close()

        # plot stereograms on outcrop, function stereo_plots(...) defined in stereoplots.py
        strikes_2d, dips_2d, pops_2d, lens_2d, rgbs_2d, transmissivities_2d, outcrops_names = [], [], [], [], [], [], []
        for fr in intersections_2d:
            strikes_2d.append(fr['strike'])
            dips_2d.append(fr['dip'])
            pops_2d.append(fr['population'])
            lens_2d.append((fr['length']))
            rgbs_2d.append(fr['rgb'])
            transmissivities_2d.append(fr['transmissivity'])
            outcrops_names.append(outcrop_name[idx])
        stereo_plots(strikes_2d, dips_2d, rgbs_2d, outcrop_name[idx], sim_out_dir, n_figure)
        n_figure += 3

        strikes_2d_all = strikes_2d_all+strikes_2d
        dips_2d_all = dips_2d_all+dips_2d
        pops_2d_all = pops_2d_all+pops_2d
        lens_2d_all = lens_2d_all+lens_2d
        transmissivities_all = transmissivities_all+transmissivities_2d
        outcrops_names_all = outcrops_names_all+outcrops_names

        try:
            strikes_output, dips_output, kappas_output = calculate_mu_and_kappa(out_path, True, pops_2d_all, strikes_2d_all,
                                                                                dips_2d_all, fracture_outcrop_all,
                                                                                outcrop_normals)
        except TypeError:
            pass

if export_vtk_data:
    write_output(out_path, 'Exporting VTK files.')
    vtk_export(polygons, intersections, rock_geometry, outcrop_geometry, outcrop_name, borehole_geometry, borehole_name,
               outcrop_intersections_vtk, out_path+'/vtk')

if export_transmissivities_histograms_in_volume and not optimize:
    write_output(out_path, 'Exporting histograms showing distribution of transmissivities in entire RVE.')
    transmissivities_histograms(out_path, transmissivities_3d, n_figure)

if export_fractures_on_outcrops_data:
    if len(outcrop_name) > 0:
        write_output(out_path, 'Exporting simulated fracture traces for all outcrops to Excel spreadsheet, '
                               'plotting histograms of fracture trace lengths and stereograms.')
        write_xls_fracture_traces(out_path, 'simulations', outcrops_names_all, pops_2d_all, strikes_2d_all, dips_2d_all,
                                  lens_2d_all)
        fracture_traces_all_outcrops_histograms(out_path, int(n_populations), pops_obs_all, pops_2d_all, lens_obs_all,
                                                lens_2d_all, length_unit, min_trace_length_recorded, n_figure)
        transmissivity_all_outcrops_histograms(out_path, int(n_populations), pops_obs_all, pops_2d_all, transmissivities,
                                               transmissivities_all, n_figure)
        n_figure += n_populations
        jsd_all_pops = log_log_plots(out_path, int(n_populations), pops_obs_all, pops_2d_all, lens_obs_all, lens_2d_all,
                                     length_unit, min_trace_length_recorded, n_figure)
        n_figure += n_populations
        stereo_plots_populations(out_path, int(n_populations), pops_obs_all, pops_2d_all, dips_obs_all, strikes_obs_all,
                                 dips_2d_all, strikes_2d_all, rgbs_input, n_figure)
        n_figure += 2*n_populations
        if opt_n_sims > 1:
            fracture_traces_all_outcrops_histograms_and_loglog_more_simulations(out_path, n_populations, pops_obs_all,
                                                                                lens_obs_all, lens_all_pops,
                                                                                strikes_all_pops, dips_all_pops,
                                                                                outcrops_for_lens_name,
                                                                                outcrops_for_lens_area,
                                                                                outcrops_for_lens_relevance, length_unit,
                                                                                min_trace_length_recorded, n_figure)
            n_figure += 2*n_populations
    else:
        write_output(out_path, 'No outcrops detected.')

if plot_fracture_size:
    write_output(out_path, 'Equivalent fracture size for individual populations, before and after intersecting:')
    for current_population in range(n_populations):
        size_of_fracture_orig, size_of_fracture_new = [], []
        color_chosen = False
        for fr in polygons:
            if fr['population'] == current_population+1:
                if not color_chosen:
                    rgbs_3d = (fr['rgb'])
                    color_chosen = True
                if fr['area_new'] > 0:
                    size_of_fracture_orig.append(np.sqrt(fr['area_orig']/3.14159))
                    size_of_fracture_new.append(np.sqrt(fr['area_new']/3.14159))

        [x_min_old, alpha_old] = get_powerlaw_constants(size_of_fracture_orig)
        [x_min_new, alpha_new] = get_powerlaw_constants(size_of_fracture_new)
        write_output(out_path, 'x_min old, population %d: %.4f' % (current_population+1, x_min_old))
        write_output(out_path, 'alpha old, population %d: %.4f' % (current_population+1, alpha_old))
        write_output(out_path, 'x_min new, population %d: %.4f' % (current_population+1, x_min_new))
        write_output(out_path, 'alpha new, population %d: %.4f' % (current_population+1, alpha_new))
        print('\n')

        if len(size_of_fracture_orig) > 0:
            title = 'Fracture length (before intersections), population no. %d' % (current_population+1)
            output_file = '%s/histograms_fracture_length-1_in_volume-population-%d-before_intersections.png' \
                          % (sim_out_dir, current_population+1)
            output_file_2 = '%s/logplot_fracture_length-1_in_volume-population-%d-before_intersections.png' \
                            % (sim_out_dir, current_population+1)
            hist_plots_with_range(size_of_fracture_orig, 20, [x_mins[current_population], x_max], rgbs_3d, title,
                                  'Equivalent fracture length - before terminations, sqrt(area/pi)', length_unit,
                                  'Number of fractures [-]', output_file, n_figure)

            n_figure += 1
            log_plots(size_of_fracture_orig, rgbs_3d, title, length_unit, output_file_2, n_figure)
            n_figure += 1
        if len(size_of_fracture_new) > 0:
            title = 'Fracture length (after intersections), population no. %d' % (current_population+1)
            output_file = '%s/histograms_fracture_length-2_in_volume-population-%d-after_intersections.png' \
                          % (sim_out_dir, current_population+1)
            output_file_2 = '%s/logplot_fracture_length-2_in_volume-population-%d-after_intersections.png' \
                            % (sim_out_dir, current_population+1)
            hist_plots_with_range(size_of_fracture_new, 20, [x_mins[current_population], x_max], rgbs_3d, title,
                                  'Equivalent fracture length - after terminations, sqrt(area/pi)', length_unit,
                                  'Number of fractures [-]', output_file, n_figure)
            n_figure += 1
            log_plots(size_of_fracture_new, rgbs_3d, title, length_unit, output_file_2, n_figure)
            n_figure += 1

if plot_borehole_data:
    write_output(out_path, 'Plotting borehole data.')
    for idx, borehole in enumerate(borehole_geometry):
        [seg_intersections, seg_intersected_polygons] = segment_polygons_intersections(borehole, polygons)
        if len(seg_intersections) > 0:
            plot_segment_polygons_intersections(borehole, seg_intersections, n_populations, seg_intersected_polygons,
                                                borehole_name[idx], sim_out_dir, length_unit, n_figure)
            n_figure += 1

            # plot stereograms for borehole, function stereo_plots(...) defined in stereoplots.py
            strikes_1d, dips_1d, rgbs_1d = [], [], []
            for fr in seg_intersected_polygons:
                strikes_1d.append(fr['strike'])
                dips_1d.append(fr['dip'])
                rgbs_1d.append(fr['rgb'])
            stereo_plots(strikes_1d, dips_1d, rgbs_1d, borehole_name[idx], sim_out_dir, n_figure)
            n_figure += 3
        else:
            write_output(out_path, 'No borehole %s intersections.' % borehole_name[idx])

if not plot_outcrop_data:
    outcrop_geometry = []
if not plot_borehole_data:
    borehole_geometry = []

if plot_volume_data:
    write_output(out_path, 'Plotting all fractures in the rock volume.')
    plot_fracture_3d(polygons, rock_geometry, 0.7, outcrop_geometry, borehole_geometry, 'Fractures in rock volume',
                     sim_out_dir + '/Rock-volume-all_fractures.png', n_figure)
    n_figure += 1
    if optimization_mode:
        write_output(out_path, 'Plotting volume stereograms.')
        strikes_3d, dips_3d, rgbs_3d = [], [], []
        for fr in polygons:
            strikes_3d.append(fr['strike'])
            dips_3d.append(fr['dip'])
            rgbs_3d.append(fr['rgb'])
        stereo_plots(strikes_3d, dips_3d, rgbs_3d, 'Rock volume', sim_out_dir, n_figure)
        n_figure += 3

if optimization_mode:
    counter_fractures_in_pops = np.zeros(n_populations)
    percentages = np.zeros(n_populations)
    for pop in pops_obs_all:
        counter_fractures_in_pops[int(pop-1)] += 1
    total = np.sum(counter_fractures_in_pops)
    percentages = counter_fractures_in_pops/total*100
    try:
        jsd_all_pops
    except NameError:
        jsd_all_pops = []
    write_optimization_results(out_path, fr_densities, x_mins, alphas, strikes_input, dips_input, kappas_input,
                               percentages, rgbs_input, jsd_all_pops, counter_fractures_in_pops)
write_output(out_path, 'Finished!')
