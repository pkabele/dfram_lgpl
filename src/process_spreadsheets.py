#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
# import sys
from stereoplots import *
from stats import *
from clrs import *
from histplots import *
from output_file import write_output
from openpyxl import load_workbook
import os
from tabulate import tabulate


# auxiliary function for constructing list of cells
def iter_rows(wsx):
    for row in wsx.iter_rows():
        yield [cell.value for cell in row]


# auxiliary function for sorting based on key
def get_key3(item):
    return item[3]


def process_input_fractures(filename, project_path, outcrop_title, outcrop_area, outcrop_weight, plot_stereograms,
                            plot_histograms, min_length, length_unit, pop_order, n_figure, outcrop_number,
                            outcrop_normals):

    obs_out_dir = project_path+'/images/observations'
    if not os.path.exists(obs_out_dir):
        os.makedirs(obs_out_dir)

    fractures = []
    if os.path.splitext(filename)[1] == 'csv':
        sys.exit("CSV files are not supported in DFraM, please use xls/xlsx format.")
    else:
        # book = open_workbook(filename, 'r')
        # sheets = book.sheet_names()
        # sheet = book.sheet_by_name(sheets[0])
        # try:
        #     for row_index in range(1, sheet.nrows):
        #         x_start, x_end = sheet.cell(row_index, 10).value, sheet.cell(row_index, 13).value
        #         y_start, y_end = sheet.cell(row_index, 11).value, sheet.cell(row_index, 14).value
        #         z_start, z_end = sheet.cell(row_index, 12).value, sheet.cell(row_index, 15).value
        #         length_fr = np.sqrt((x_end-x_start)**2+(y_end-y_start)**2+(z_end-z_start)**2)
        #         fractures.append({'dip': sheet.cell(row_index, 1).value, 'strike': sheet.cell(row_index, 3).value,
        #                           'population': pop_order[int(sheet.cell(row_index, 4).value-1)],
        #                           'x_start': x_start, 'x_end': x_end, 'y_start': y_start, 'y_end': y_end,
        #                           'z_start': z_start, 'z_end': z_end, 'length': length_fr})
        # TODO - do rigorous check for "optimize"
        try:
            book = load_workbook(filename, read_only=True, data_only=True)
            try:
                sheets = book.sheetnames
                sheet = book[sheets[0]]
                rows = list(iter_rows(sheet))
                book.close()
                # Remove empty rows and the header line (with some redundancy to account for different keywords)
                headerkw = set(['Name', 'NAME', 'Dip', 'dip', 'Azimuth', 'azimuth',
                                'Strike', 'strike', 'Population', 'Ending on'])
                rows[:] = [item for item in rows if not None in item[0:4]]  # drop row if there is empty cell in col 1-4
                rows[:] = [item for item in rows if not bool(headerkw.intersection(item[0:15]))]  # remove header
                # Read traces' data
                for row in rows:
                    x_start, x_end = row[10], row[13]
                    y_start, y_end = row[11], row[14]
                    z_start, z_end = row[12], row[15]
                    length_fr = np.sqrt((x_end-x_start)**2+(y_end-y_start)**2+(z_end-z_start)**2)
                    fractures.append({'dip': row[1], 'strike': row[3],
                                'population': pop_order[int(row[4]-1)],
                                'x_start': x_start, 'x_end': x_end, 'y_start': y_start, 'y_end': y_end,
                                'z_start': z_start, 'z_end': z_end, 'length': length_fr})
            except:
                sys.exit("Error in file '%s', look for population numbers or format of data." % filename)
        except:
            sys.exit("Could not read '%s'." % filename)

    n_fractures = len(fractures)
    dips = [None]*n_fractures
    outcrop_names = [None]*n_fractures
    strikes = [None]*n_fractures
    lengths = [None]*n_fractures
    populations = [None]*n_fractures
    x_starts = [None]*n_fractures
    x_ends = [None]*n_fractures
    y_starts = [None]*n_fractures
    y_ends = [None]*n_fractures
    z_starts = [None]*n_fractures
    z_ends = [None]*n_fractures
    for idx, fracture in enumerate(fractures):
        dips[idx] = fracture['dip']
        strikes[idx] = fracture['strike']
        populations[idx] = fracture['population']
        lengths[idx] = fracture['length']
        outcrop_names[idx] = outcrop_title
        x_starts[idx] = fracture['x_start']
        x_ends[idx] = fracture['x_end']
        y_starts[idx] = fracture['y_start']
        y_ends[idx] = fracture['y_end']
        z_starts[idx] = fracture['z_start']
        z_ends[idx] = fracture['z_end']

    rgbs = []
    for population in populations:
        rgbs.append(clrs(population-1))

    n_populations = int(max(populations))
    observed_outcrop_data = [None]*n_populations
    for i in range(1, n_populations+1):
        temp_dips = []
        temp_strikes = []
        temp_x_starts = []
        temp_x_ends = []
        temp_y_starts = []
        temp_y_ends = []
        temp_z_starts = []
        temp_z_ends = []
        for j in range(n_fractures):
            if i == populations[j]:
                temp_dips.append(dips[j])
                temp_strikes.append(strikes[j])
                temp_x_starts.append(x_starts[j])
                temp_x_ends.append(x_ends[j])
                temp_y_starts.append(y_starts[j])
                temp_y_ends.append(y_ends[j])
                temp_z_starts.append(z_starts[j])
                temp_z_ends.append(z_ends[j])

        if len(temp_dips) > 0:
            [strike_temp_mean, dip_temp_mean, kappa, mean_length, n_fractures_in_popul, fracture_length_temp,
             cumulative_fracture_length] = stats2(temp_dips, temp_strikes, temp_x_starts, temp_x_ends, temp_y_starts,
                                                  temp_y_ends, temp_z_starts, temp_z_ends, min_length,
                                                  np.ones_like(temp_dips)*outcrop_number, outcrop_normals)
            fracture_density = cumulative_fracture_length / outcrop_area
            dens_fractures = n_fractures_in_popul / outcrop_area

            observed_outcrop_data[i-1] = {'strikes_mean': strike_temp_mean, 'dips_mean': dip_temp_mean, 'kappa': kappa,
                                          'mean_length': mean_length, 'rgb': clrs(i-1), 'dens_fractures': dens_fractures,
                                          'n_fractures': n_fractures_in_popul, 'fracture_density': fracture_density,
                                          'outcrop_weight': outcrop_weight, 'outcrop_title': outcrop_title}

            if plot_histograms:
                title = 'Field observations, fracture population no. %d' % i
                output_file = '%s/histograms-%s-population-%d.png' % (obs_out_dir, outcrop_title, i)
                hist_plots(fracture_length_temp, 30, clrs(i-1), title, 'Trace length', length_unit,
                           'Number of fractures [-]', output_file, n_figure)
                n_figure += 1

        else:
            observed_outcrop_data[i-1] = {'strikes_mean': [], 'dips_mean': [], 'kappa': [],
                                          'mean_length': [], 'rgb': [], 'dens_fractures': [],
                                          'n_fractures': 0.0, 'fracture_density': [],
                                          'outcrop_weight': outcrop_weight, 'outcrop_title': outcrop_title}

    if plot_stereograms:
        title = 'Field observations, %s' % outcrop_title
        stereo_plots(strikes, dips, rgbs, title, obs_out_dir, n_figure)
        n_figure += 3

    # sort the output arrays
    populations_sorted = populations  # [x for y, x in sorted(zip(populations, populations))]
    outcrop_names_sorted = outcrop_names  # [x for y, x in sorted(zip(populations, outcrop_names))]
    strikes_sorted = strikes  # [x for y, x in sorted(zip(populations, strikes))]
    dips_sorted = dips  # [x for y, x in sorted(zip(populations, dips))]
    lengths_sorted = lengths  # [x for y, x in sorted(zip(populations, lengths))]

    write_output(project_path,
                 'Field measurement data from file "%s" for outcrop "%s" processed successfully.'
                 % (filename, outcrop_title))
    print("Field measurement data from file '%s' for outcrop '%s' processed successfully."
                 % (filename, outcrop_title))
    return observed_outcrop_data, n_figure, outcrop_names_sorted, populations_sorted, strikes_sorted, dips_sorted, \
           lengths_sorted


def process_input_outcrops(outcrop_filename):
    try:
        filename, file_extension = os.path.splitext(outcrop_filename)
        outcrop_bb_filename = filename + '_BB' + file_extension
        book = load_workbook(filename=outcrop_bb_filename, read_only=True, data_only=True)
        sheets = book.sheetnames
        sheet = book[sheets[0]]
        rows = (list(iter_rows(sheet)))
        book.close()
        # Remove empty cells and the header line
        headerkw = set(['X(East)', 'x'])
        rows[:] = [item for item in rows if not None in item[0:3]]
        rows[:] = [item for item in rows if not bool(headerkw.intersection(item[0:3]))]
        # Sort vertices according to index in 4th column
        rows.sort(key=lambda x: x[3])
        # Store vertices as array
        verts = np.zeros([len(rows), 3])
        for i in range(len(rows)):
            verts[i] = np.asarray(rows[i][:3])

        print("\nOutcrop vertices extracted from '%s'." % outcrop_bb_filename)
        print("Returning 'verts':")
        print(tabulate(verts, floatfmt=(".2f"), tablefmt="fancy_grid", numalign="center"))
    except:
        sys.exit("Could not read '%s'." % outcrop_bb_filename)

    return verts


def process_input_relevance(filename):
    try:
        # book = open_workbook(filename, 'r')
        # sheets = book.sheet_names()
        # sheet = book.sheet_by_name(sheets[0])
        # outcrop_relevances = []
        # for row_index in range(sheet.nrows):
        #     outcrop_relevances.append(sheet.cell(row_index, 1).value)
# TODO - do rigorous check for "optimize"
        book = load_workbook(filename, read_only=True, data_only=True)
        sheets = book.sheetnames
        sheet = book[sheets[0]]
        rows = list(iter_rows(sheet))
        book.close()
        # Remove empty rows
        rows[:] = [item for item in rows if not None in item[0:1]]
        outcrop_relevances = []
        for row in rows:
            outcrop_relevances.append(row[1])

        print("\nOutcrop relevances extracted from '%s'." % filename)
        print("Returning 'outcrop_relevances':", outcrop_relevances)

    except:
        sys.exit("Could not read '%s'." % filename)

    return outcrop_relevances


def process_input_termination(filename):
    try:
        # book = open_workbook(filename, 'r')
        # sheets = book.sheet_names()
        # sheet = book.sheet_by_name(sheets[0])
        # order_of_populations = []
        # terminations = []
        # for row_index in range(1, sheet.nrows):
        #     terminations_temp = []
        #     for col_index in range(1, sheet.nrows):
        #         value = sheet.cell(row_index, col_index).value
        #         terminations_temp.append(1.0 - value/100)
        #     terminations.append(terminations_temp)
        #     order_of_populations.append(int(sheet.cell(row_index, 0).value))
# TODO - do rigorous check for "optimize" - this may still be NG by VN !!!
        book = load_workbook(filename, read_only=True, data_only=True)
        sheets = book.sheetnames
        sheet = book[sheets[0]]
        rows = list(iter_rows(sheet))
        book.close()
        # Remove empty cells and the header line
        headerkw = set(["Population", "population", "POPULATION"])
        headrow = [item for item in rows if bool(headerkw.intersection(item[0:1]))]
        if headrow == []:
            sys.exit("Could not identify header row in '%s'." % filename)
        rows[:] = [item for item in rows if not None in item[0:1]]
        rows[:] = [item for item in rows if not bool(headerkw.intersection(item[0:1]))]
        order_of_populations = []
        terminations = []
        npops = len(rows)  # assume that each row represents one population
        for row in rows:
            terminations_temp = []
            for value in row[1:npops + 1]:  # 1st row is population id
                if value is None:  # assume empty cell as 0
                    value = 0
                terminations_temp.append(1.0 - value / 100)
            terminations.append(terminations_temp)
            order_of_populations.append(int(row[0]))

        print("\nTermination of fractures extracted from '%s'." % filename)
        if headrow[0][1:npops + 1] != order_of_populations:
            print("Order of populations in 1st row and 1st column are not same:")
            print("1st row:",headrow[0][1:npops+1])
            print("1st column:",order_of_populations)
            sys.exit("Error in '%s'." % filename)
        print("Returning 'order_of_populations': ", order_of_populations)
        print("Returning 'terminations':")
        print(tabulate(terminations, floatfmt=(".2f"), tablefmt="fancy_grid", numalign="center"))
    except:
        sys.exit("Could not read '%s'." % filename)

    return order_of_populations, terminations


def process_input_transmissivity(filename):
    try:
        # book = open_workbook(filename, 'r')
        # sheets = book.sheet_names()
        # sheet = book.sheet_by_name(sheets[0])
        # transmissivities = []
        # for row_index in range(1, sheet.nrows):
        #     transmissivity = []
        #     for col_index in range(1, 4):
        #         transmissivity.append(sheet.cell(row_index, col_index).value)
        #     if sum(transmissivity) <= 0.0:
        #         transmissivities.append(np.array([100, 0, 0]))
        #     else:
        #         transmissivities.append(np.array(transmissivity) / sum(transmissivity) * 100)
# TODO - do rigorous check for "optimize"
        book = load_workbook(filename, read_only=True, data_only=True)
        sheets = book.sheetnames
        sheet = book[sheets[0]]
        rows = list(iter_rows(sheet))
        book.close()
        rows.pop(0)  # remove header line
        rows[:] = [item for item in rows if item[0]]  # remove rows with 1st cell empty
        transmissivities = []
        for row in rows:
            transmissivity = row[1:4]
            transmissivity = [0 if item is None else item for item in transmissivity]  # replace empty cells by 0
            if sum(transmissivity) <= 0.0:
                transmissivities.append(np.array([100, 0, 0]))
            else:
                transmissivities.append(np.array(transmissivity) / sum(transmissivity) * 100)

        print("\nTransmissivity of fractures extracted from '%s'." % filename)
        print("Returning 'transmissivities':")
        print(tabulate(transmissivities, floatfmt=(".2f"), tablefmt="fancy_grid", numalign="center"))

    except:
        sys.exit("Could not read '%s'." % filename)

    return transmissivities
