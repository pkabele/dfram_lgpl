#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as mp3d
from polypolyintersect import poly_area_3d
from princ_direction import *


def generate_poly(semi_major_axis, semi_minor_axis, n_poly, dx, dy, dz, strike, dip, in_plane_rotation,
                  regular_polygons):

    deg2rad = np.pi/180

    # generate vertices of a polygon
    if regular_polygons:
        t = np.linspace(0.0, 1.0, num=n_poly, endpoint=False)*2*np.pi  # generate linearly spaced vector
    else:
        t = np.random.random(n_poly)*2*np.pi        # generates angles in radians in a range 0, 2pi to a 1D array
        t.sort()                                    # ascending sort of t array
    cos_t = np.cos(t)
    sin_t = np.sin(t)

    # parametric equations of an ellipse
    x = semi_major_axis*cos_t
    y = semi_minor_axis*sin_t
    z = np.zeros(n_poly, dtype=np.float64)  # necessary to specify that it is float, otherwise is evaluated as int

    # rotate the ellipse in its plane
    cz = np.cos(in_plane_rotation*deg2rad)
    sz = np.sin(in_plane_rotation*deg2rad)
    Rz = np.array([[cz, -sz, 0], [sz, cz, 0], [0, 0, 1]])

    for i in range(0, n_poly):
        current_point = np.array([[x[i]],
                                  [y[i]],
                                  [z[i]]])
        rotated_point = np.matmul(Rz, current_point)

        x[i] = rotated_point[0]
        y[i] = rotated_point[1]
        z[i] = rotated_point[2]

    # get cosines of normal from dip and strike
    ''' 
    incorrect !!!
    n_n = np.sin(dip*deg2rad)*np.sin(strike*deg2rad)
    n_e = -np.sin(dip*deg2rad)*np.cos(strike*deg2rad)
    n_d = np.cos(dip*deg2rad)
    '''
    '''
    [n_e, n_n, n_d] = sd_to_vec(strike, dip)
    Vasku I'm not sure about this as sd_to_vec was originally writen for pole vector
    of a point on lower hemispehere, which is opposite to (outward) normal vector
    of a fracture. I have tested the folllowing. (PK)
    '''
    n_e = np.sin(dip*deg2rad)*np.cos(strike*deg2rad)
    n_n = -np.sin(dip*deg2rad)*np.sin(strike*deg2rad)
    n_d = np.cos(dip*deg2rad)

    a = np.array([0.0, 0.0, 1.0])
    b = np.array([n_e, n_n, n_d])
    v = np.cross(a, b)
    s = norm(v)
    c = np.vdot(a, b)
    V = np.array([[0, -v[2], v[1]],
                  [v[2], 0, -v[0]],
                  [-v[1], v[0], 0]])
    V2 = np.matrix(V)**2
    R = np.identity(3) + V + V2*((1-c)/s**2)

    # point transformation
    xT = np.zeros(n_poly+1)
    yT = np.zeros(n_poly+1)
    zT = np.zeros(n_poly+1)

    polygon = [None]*n_poly
    for i in range(0, n_poly):
        current_point = np.array([[x[i]],
                                  [y[i]],
                                  [z[i]]])
        rotated_point = np.matmul(R, current_point)

        xT[i] = rotated_point[0]+dx
        yT[i] = rotated_point[1]+dy                 # translation added to the rotated coordinates
        zT[i] = rotated_point[2]+dz

        if i == 0:                                  # copies first point to n-th position to close the ellipse plot
            xT[n_poly] = xT[i]
            yT[n_poly] = yT[i]
            zT[n_poly] = zT[i]

        polygon[i] = [xT[i], yT[i], zT[i]]

    return [xT, yT, zT, poly_area_3d(polygon)]


def plot_fracture_3d(polygons, domain, alpha, outcrops, boreholes, title, output_filename, n_figure):

    # find max and min for individual axes to establish their scale
    x_max = domain['x'][1]
    y_max = domain['y'][1]
    z_max = domain['z'][1]
    x_min = domain['x'][0]
    y_min = domain['y'][0]
    z_min = domain['z'][0]

    fig = plt.figure(n_figure)
    ax = fig.add_subplot(111, projection='3d')

    for fr in polygons:
        rgb = fr['rgb']
        x = fr['xT']
        y = fr['yT']
        z = fr['zT']

        verts = [None]*(len(x))
        for i in range(0, len(x)):
            verts[i] = (x[i], y[i], z[i])

        poly = mp3d.art3d.Poly3DCollection([verts], alpha=alpha, linewidth=1)
        poly.set_facecolor(rgb)
        ax.add_collection3d(poly)

    for outcrop in outcrops:
        x = outcrop['x']
        y = outcrop['y']
        z = outcrop['z']

        verts = [None]*(len(x))
        for i in range(0, len(x)):
            verts[i] = (x[i], y[i], z[i])

        poly = mp3d.art3d.Poly3DCollection([verts], alpha=0.45, linewidth=2)
        poly.set_facecolor([0.4, 0.4, 0.4])
        ax.add_collection3d(poly)

    for borehole in boreholes:
        ax.plot(borehole['x'], borehole['y'], borehole['z'], linewidth=2, color=[0,0,0])

    '''
    for intersection in intersections:
        ax.plot(intersection['x'], intersection['y'], intersection['z'], linewidth=3, color=[1,1,0])
    '''

    # Create cubic bounding box to simulate equal aspect ratio
    '''max_range = np.array([x_max-x_min, y_max-y_min, z_max-z_min]).max()
    Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(x_max+x_min)
    Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(y_max+y_min)
    Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(z_max+z_min)
    for xb, yb, zb in zip(Xb, Yb, Zb):
        ax.plot([xb], [yb], [zb], 'w')'''
    ax.set_xlim3d([x_min, x_max])
    ax.set_ylim3d([y_min, y_max])
    ax.set_zlim3d([z_min, z_max])

    ax.set_xlabel('West-East')
    ax.set_ylabel('South-North')
    ax.set_zlabel('Depth')

    fig.canvas.set_window_title(title)
    plt.savefig(output_filename)
    # fig.show()
