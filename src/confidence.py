#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
import numpy as np
import scipy.stats as st
from output_file import write_output


# required confidence = confidence level

def confidence(path, variable, population, outcrop, set_of_values, required_confidence, print_output):
    b = st.t.interval(required_confidence, len(set_of_values)-1, loc=np.mean(set_of_values), scale=st.sem(set_of_values))
    halfinterval_size = (b[1]-b[0])/2
    confidence_in_results = halfinterval_size/np.mean(set_of_values)

    if confidence_in_results < (1-required_confidence)/2:
        enough_confidence = True
        '''
        print('Sufficient number of realizations, confidence in %s of population %d on outcrop %d = %.1f%%.'
              % (variable, population, outcrop, (1-2*confidence_in_results)*100))
        '''
    else:
        enough_confidence = False
        if print_output:
            '''
            write_output(path, 'Insufficient number of realizations, confidence in %s of population %d on outcrop '
                               '%d = %.1f%%.' % (variable, population, outcrop, max(0, (1-2*confidence_in_results)*100)))
            '''
            print('Insufficient number of realizations, confidence in %s of population %d on outcrop %d = %.1f%%.'
                  % (variable, population, outcrop, max(0, (1-2*confidence_in_results)*100)))

    return enough_confidence


def confidence2(path, variable, population, set_of_values, outcrop_weights, required_confidence,
                print_output):

    input_array = []
    result = 0
    for i in range(len(set_of_values[0][population])):
        for ii in range(len(set_of_values)):
            values = set_of_values[ii][population][i]
            result += np.mean(values)*outcrop_weights[ii]
        result /= sum(outcrop_weights)
        input_array.append(result)

    b = st.t.interval(required_confidence, len(input_array)-1, loc=np.mean(input_array), scale=st.sem(input_array))
    halfinterval_size = (b[1]-b[0])/2
    confidence_in_results = halfinterval_size/np.mean(input_array)

    if confidence_in_results < (1-required_confidence)/2:
        enough_confidence = True
    else:
        enough_confidence = False
        if print_output:
            '''
            write_output(path, 'Insufficient number of realizations, confidence in %s of population %d = %.1f%%.'
                  % (variable, population+1, max(0, (1-2*confidence_in_results)*100)))
            '''
            print('Insufficient number of realizations, confidence in %s of population %d = %.1f%%.'
                  % (variable, population+1, max(0, (1-2*confidence_in_results)*100)))

    return enough_confidence


def confidence3(path, variable, population, mean_length_outcrop, cum_length_outcrop, n_fractures_outcrop,
                outcrop_weights, required_confidence, print_output):

    input_array = []
    result = 0
    sum_n, sum_l = [0.0]*len(cum_length_outcrop), [0.0]*len(cum_length_outcrop)
    for i in range(len(cum_length_outcrop[0][population])):
        for ii in range(len(cum_length_outcrop)):
            sum_n[ii] += n_fractures_outcrop[ii][population][i]
            sum_l[ii] += cum_length_outcrop[ii][population][i]
            if sum_n[ii] > 0:
                value = sum_l[ii] / sum_n[ii]
            else:
                value = 0.0
            result += value*outcrop_weights[ii]
        result /= sum(outcrop_weights)
        input_array.append(result)

    for ii in range(len(cum_length_outcrop)):
        if sum_n[ii] > 0:
            mean_length_outcrop[ii][population] = sum_l[ii] / sum_n[ii]
        else:
            mean_length_outcrop[ii][population] = 0.0

    b = st.t.interval(required_confidence, len(input_array)-1, loc=np.mean(input_array), scale=st.sem(input_array))
    halfinterval_size = (b[1]-b[0])/2
    confidence_in_results = halfinterval_size/np.mean(input_array)

    if confidence_in_results < (1-required_confidence)/2:
        enough_confidence = True
    else:
        enough_confidence = False
        if print_output:
            '''
            write_output(path, 'Insufficient number of realizations, confidence in %s of population %d = %.1f%%.'
                  % (variable, population+1, max(0, (1-2*confidence_in_results)*100)))
            '''
            print('Insufficient number of realizations, confidence in %s of population %d = %.1f%%.'
                  % (variable, population+1, max(0, (1-2*confidence_in_results)*100)))

    return enough_confidence, mean_length_outcrop
