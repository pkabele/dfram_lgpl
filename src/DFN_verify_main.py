#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
"""
Main body of DFN_verify
- This program was constructed by merging tracelen_v01.py and Fisher_params_v03.py
- All functions are kept in DFN_verify_fcns.py

2017.12.29
 - print section heading according to final report

2018.04.05
 - print tables of
   average length, number, and densities of traces on outcrops  (DFN_verify_fcns.py)
 - replaced deprecated functions sheets=wb.get_sheet_names() & ws=wb.get_sheet_by_name(sheets[0])
 - added dip azimuth in Table 1
 - corrected calculation of P21=ltot/Aoc (was lav/Aoc)

2019.03.29 (V. Nežerka)
 - incorporation of the DFN_verify code to DFraM, printing html report
"""

from math import *
from openpyxl import load_workbook
import numpy as np
import scipy as sp
import datetime
import sys
import os
import matplotlib.pyplot as plt
import mplstereonet
from tictoc import *
from output_file import print_html_report
from DFN_verify_fcns import *


class Trace:
    def __init__(self, sim, outcrop, rlv, area, pop, length, strike, dip, tcf):
        self.sim = sim  # no. of simulation, -1...observation
        self.outcrop = outcrop
        self.rlv = rlv  # relevance of outcrop (scale 1-10)
        self.area = area  # area of outcrop
        self.pop = pop
        self.length = length
        self.strike = strike
        self.dip = dip
        self.tcf = tcf  # Terzaghi correction factor


# Initialize some vars, create output dir etc.
print("Entered subprocess DFN_verify_main.py")
initialize_global_vars()
project_name = '%s' % (np.genfromtxt('_temp_project_name.txt', dtype='str'))
site_name = '%s' % (np.genfromtxt('_temp_site_name.txt', dtype='str', delimiter='\n'))
outcrop_file = np.genfromtxt('_temp_outcrop_file.txt', dtype='str')
outcrop_name = np.genfromtxt('_temp_outcrop_name.txt', dtype='str')
outcrop_names = []
for o_n in outcrop_name:
    outcrop_names.append(o_n)
outcrop_relevance = np.genfromtxt('_temp_outcrop_relevance.txt', dtype='float')
verify_dir = '%s' % (np.genfromtxt('_temp_verify_dir.txt', dtype='str'))

# initiate output files
[time_of_start, out_path, report_path] = create_time_folder_verify(project_name)

OClist, OCrlvs = [], []
for idx, relevance in enumerate(outcrop_relevance):
    OClist.append(outcrop_file[idx])
    OCrlvs.append(relevance)

# Read BB files and calculate for outcrops:
OCareas = {}  # Areas of outcrops projected to principal plane
OCnvecs = {}  # Normals of the principal planes
OCverts = {}  # Original vetices
OCpverts = {}  # Vertices projected to princ. plane
OCpsums = {}  # Sums of projection vectors (indicates divergence of oroginal outcrop from plane)
for OCi, OCitem in enumerate(OClist):
    filename, file_extension = os.path.splitext(OCitem)
    BBfile = filename + '_BB' + file_extension
    # read cells from 1st sheet
    try:
        wb = load_workbook(filename=BBfile, read_only=True, data_only=True)
        sheets = wb.sheetnames
        ws = wb[sheets[0]]
        cells = (list(iter_rows(ws)))
        # remove empty cells and the header line
        headerkw = set(['X(East)', 'x'])
        cells[:] = [item for item in cells if not None in item[0:3]]
        cells[:] = [item for item in cells if not bool(headerkw.intersection(item[0:3]))]
        # sort vertices according to index in 4th column
        cells.sort(key=getKey3)
        # store vertices as array
        verts = np.zeros([len(cells), 3])
        for i in range(len(cells)):
            verts[i] = np.asarray(cells[i][:3])
        # calculate area and normal of outcrop projection to its principal plane
        OCverts[OCi] = verts
        OCpverts[OCi], OCareas[OCi], OCnvecs[OCi], OCpsums[OCi] = OCplane(verts)
    except IOError:
        print("Warning: could not read file ", BBfile)
        raise  # pass
try:
    del wb, ws, sheets, cells, verts
except NameError:
    raise


# read outcrop observation files and store traces data in list
obstraces = []
POPlist = []
for OCi, OCitem in enumerate(OClist):
    # read cells from 1st sheet
    try:
        wb = load_workbook(filename=OCitem, read_only=True, data_only=True)
        sheets = wb.sheetnames
        ws = wb[sheets[0]]
        cells = (list(iter_rows(ws)))
        # remove empty cells and the header line (with some redundancy to account for different keywords)
        headerkw = set(['Name', 'NAME', 'Dip', 'dip', 'Azimuth', 'azimuth', 'Strike', 'strike', 'Population', 'Ending on'])
        cells[:] = [item for item in cells if not None in item[0:4]]
        cells[:] = [item for item in cells if not bool(headerkw.intersection(item[0:15]))]
        # interpret trace data and calculate trace length and Terzaghi factor
        spt = np.zeros([3])
        ept = np.zeros([3])
        rlv = OCrlvs[OCi]
        area = OCareas[OCi]
        ocnvec = OCnvecs[OCi]
        for iline in range(len(cells)):
            spt = np.asarray(cells[iline][9:12])
            ept = np.asarray(cells[iline][12:15])
            length = np.linalg.norm(ept - spt)
            pop = cells[iline][4]
            dip = cells[iline][1]
            strike = cells[iline][3]
            tcf = cal_Terzaghi(strike, dip, ocnvec, 10.0)
            # append traces data to list "obstraces"
            trace = Trace(-1, OCitem, rlv, area, pop, length, strike, dip, tcf)
            obstraces.append(trace)
            # create and sort unique list of populations
            if pop not in POPlist:
                POPlist.append(pop)
        POPlist.sort()
    except IOError:
        print("Warning: could not read outcrop traces file ", OCitem)
        raise  # pass

try:
    del wb, ws, sheets, cells, spt, ept, rlv, area, ocnvec, length, pop, dip, strike, tcf, trace
except NameError:
    raise


# read DFraM identification/optimization results
try:
    dips_file = open(verify_dir + "/DFN_dips.txt", "r")
    strikes_file = open(verify_dir + "/DFN_strikes.txt", "r")
    kappas_file = open(verify_dir + "/DFN_kappas.txt", "r")
    alphas_file = open(verify_dir + "/DFN_alphas.txt", "r")
    xmins_file = open(verify_dir + "/DFN_x_mins.txt", "r")
    P30s_file = open(verify_dir + "/DFN_fracture_densities.txt", "r")
    JSDs_file = open(verify_dir + "/jensen-shannon_entropy.txt", "r")
    line = dips_file.readline()
    lst = line.split(",")
    DFraM_dips = [float(item) for item in lst]
    line = strikes_file.readline()
    lst = line.split(",")
    DFraM_strikes = [float(item) for item in lst]
    line = kappas_file.readline()
    lst = line.split(",")
    DFraM_kappas = [float(item) for item in lst]
    line = alphas_file.readline()
    lst = line.split(",")
    DFraM_alphas = [float(item) for item in lst]
    line = xmins_file.readline()
    lst = line.split(",")
    DFraM_xmins = [float(item) for item in lst]
    line = P30s_file.readline()
    lst = line.split(",")
    DFraM_P30s = [float(item) for item in lst]
    DFraM_JSDs = []
    lines = JSDs_file.readlines()
    for iline, line in enumerate(lines):
        l = line.split(None)
        DFraM_JSDs.append(float(l[0]))

    npops = len(POPlist)
    if len(DFraM_dips) != npops or len(DFraM_strikes) != npops or len(DFraM_kappas) != npops \
            or len(DFraM_alphas) != npops or len(DFraM_xmins) != npops or len(DFraM_P30s) != npops or \
            len(DFraM_JSDs) != npops:
        print("Warning: number of populations in observation data and optimization output are not the same.")
except IOError:
    print("Warning: could not read DFraM optimization results files.")
    raise  # pass

del line, lst, npops
dips_file.close()
strikes_file.close()
kappas_file.close()
alphas_file.close()
xmins_file.close()
P30s_file.close()
JSDs_file.close()

# Read DFraM simulated traces data
simtraces = []
try:
    dir_list = os.listdir(verify_dir)
    for item in dir_list:
        if '_fractures_all_' in item:
            sim_file = '%s/%s' % (verify_dir, item)

    simtraces_file = open(sim_file, "r")
    simtraces_lines = simtraces_file.readlines()

    for iline, line in enumerate(simtraces_lines):
        t = line.split(None)
        if iline > 0:
            sim = int(t[0])
            pop = int(t[1])
            length = float(t[2])
            strike = float(t[3])
            dip = float(t[4])
            ocrp = t[5]
            area = float(t[6])
            rlv = float(t[7])
            ocnvec = OCnvecs[outcrop_names.index(ocrp)]
            tcf = cal_Terzaghi(strike, dip, ocnvec, 10.0)
            simtrace = Trace(sim, outcrop_names.index(ocrp), rlv, area, pop, length, strike, dip, tcf)  # Terzaghi factor set 1 here
            simtraces.append(simtrace)
except IOError:
    print("Warning: could not read simulations result file ", sim_file)
    raise  # pass

del sim, pop, length, dip, strike, ocrp, area, rlv, ocnvec, tcf, simtrace
simtraces_file.close()

# check that populations in observation and simulations are the same
tmp = list(set([t.pop for t in simtraces]))
tmp.sort()
if tmp != POPlist:
    print("Warning: populations in observation and simulation files are not the same!")
    print(POPlist)
    print(tmp)
del tmp
# get list of simulations
SIMlist = list(set([t.sim for t in simtraces]))
SIMlist.sort()
SIMcount = len(SIMlist)


# ==================================
# Print header
# ==================================

print("###########################################################")
print("###########################################################\n")
print("   DFraM verification and DFN advanced analysis protocol   ")
print("   Site:", project_name)
print("\n###########################################################")
print("###########################################################")
print("DFraM verification protocol")
print("Date, time:", datetime.datetime.now())
print("Author: Petr Kabele")
print("\nPython version:")
print(sys.version)
print("\nSource file:")
print(os.path.realpath(__file__))
# print(os.path.basename(sys.argv[0]))
print("\nObservation data:")
print(outcrop_file)
# print("- relevance file:", rlvfilename)
print("\nSimulation data:")
print(sim_file)
# print("\nOutcrops to be skipped:", OCskip)
print("Outcrops analyzed:", OClist)
print("Populations:", POPlist)
print("Simulations:", SIMlist)

print_html_report(report_path, '_SITE NAME_', site_name, keep_keyword=False)
print_html_report(report_path, '_TIME AND DATE_', str(datetime.datetime.now()), keep_keyword=False)
print_html_report(report_path, '_SIMULATION FILE_', sim_file, keep_keyword=False)

outcrop_names_inline = ''
for idx, on in enumerate(outcrop_names):
    outcrop_names_inline += on
    outcrop_names_inline += ' (%d)' % int(outcrop_relevance[idx])
    if idx != (len(outcrop_names) - 1):
        outcrop_names_inline += ', '
print_html_report(report_path, '_OUTCROP NAMES_', outcrop_names_inline, keep_keyword=False)
print_html_report(report_path, '_N POPULATIONS_', str(len(POPlist)), keep_keyword=False)
print_html_report(report_path, '_N SIMULATIONS_', str(len(SIMlist)), keep_keyword=False)

# ==================================
# DFraM summary
# ==================================

sec_heading("Summary of DFN parameters identified by DFraM")

print("Jensen-Shannon divergence (JSD) is used to quantify the (dis)similarity between \
the probability distributions of observed and simulated (1 realization) trace \
lengths. It ranges between 0 and 1. JSD = 0 if the distributions are indistinguishable.\n")

tab_label("Summary of DFN parameters identified by DFraM")
# templates for print
template0 = '{:^10}|{:^6}|{:^8}|{:^6}|{:^20}|{:^7}|{:^9}|{:^6}|{:^10}|{:^7}'
template1 = '{:^10} {:6.1f} {:8.1f} {:6.1f} {:6.3f} {:6.3f} {:6.3f} {:7.3f} {:^9.3f} {:6.3f} {:10.5f} {:7.4f}'
template_html = '                <td>{:^10}</td> <td>{:6.1f}</td> <td>{:6.1f}</td> <td>{:6.3f}</td> ' \
                '<td>{:6.3f}</td> <td>{:6.3f}</td> <td>{:7.3f}</td> <td>{:^9.3f}</td> <td>{:6.3f}</td> <td>{:10.5f}' \
                '</td> <td>{:7.4f}'
print(template0.format("Population", "Strike", "Dip azim", "Dip", "Unit mean vector", "kappa", "x_min [m]", "alpha",
                       "P30 [m^-3]", "JSD"))
for ipop, pop in enumerate(POPlist):
    DFraM_myu = sd_to_vec(DFraM_strikes[ipop], DFraM_dips[ipop])
    line_to_print = template1.format(pop, DFraM_strikes[ipop], sa_to_da(DFraM_strikes[ipop]), DFraM_dips[ipop],
                                      DFraM_myu[0], DFraM_myu[1], DFraM_myu[2], DFraM_kappas[ipop], DFraM_xmins[ipop],
                                      DFraM_alphas[ipop], DFraM_P30s[ipop], DFraM_JSDs[ipop])
    line_to_report = template_html.format(pop, DFraM_strikes[ipop], DFraM_dips[ipop],
                                          DFraM_myu[0], DFraM_myu[1], DFraM_myu[2], DFraM_kappas[ipop],
                                          DFraM_xmins[ipop], DFraM_alphas[ipop], DFraM_P30s[ipop], DFraM_JSDs[ipop])
    print(line_to_print)

    print_html_report(report_path, '_DFRAM RESULTS_', '              <tr>', keep_keyword=True)
    print_html_report(report_path, '_DFRAM RESULTS_', line_to_report, keep_keyword=True)
    if ipop < (len(POPlist)-1):
        print_html_report(report_path, '_DFRAM RESULTS_', '              </tr>', keep_keyword=True)
    else:
        print_html_report(report_path, '_DFRAM RESULTS_', '              </tr>', keep_keyword=False)

# ==================================
# DFraM verification
# ==================================

# Outcrops geometry
Atot_ver = 0
Atot_tri = 0
Atot_dfram = 0

sec_heading("Outcrops' geometry")
print("Here we verify the calcualtion of outcrop areas and the effect of \
projecting the outcrops geometry from field data to the principal plane. \
Areas are calcualted by two methods:\n\
A proj. is the area of the polygon obtained by the projection;\n\
A tri. is calculated by triangulation from the geometrical center to the original vertices.\n\
Dist. is the sum of the  norms of the projection vectors; it serves as an \
indicator of how much the original polygon deviates from the principal plane.\n")

template0 = '{0:2} | {1:<55}'
tab_label("Outcrop indices")
for iocr, ocr in enumerate(OClist):
    print(template0.format(iocr, ocr))
print("\n")

template0 = '{0:^7} | {1:^11} | {2:^9}  | {3:^9} | {4:^9} | {5:^9}'
template1 = '{0:^7}   {1:^11}   {2:9.2f}   {3:9.2f}   {4:9.2f}   {5:9.2f}'
template2 = '{0:^7}   {1:^11}   {2:9.2f}   {3:9.2f}   {4:9.2f}'
template_html = '                <td>{0}</td> <td>{1:3d}</td> <td>{2:9.2f}</td> <td>{3:9.2f}</td> <td>{4:9.2f}</td> '

tab_label("Geometrical characteristics of outcrops")
print(template0.format("Outcrop", "# of verts.", "A_DFraM", "A_proj.", "A_triang ", "Dist "))
print(template0.format("", "", "[m^2]", "[m^2]", "[m^2]", "[m]"))
for iocr, ocr in enumerate(outcrop_names):
    oca_ver = OCareas[iocr]
    oca_tri, _ = OCarea_by_tri(OCverts[iocr])
    oca_dfram = [t.area for t in simtraces if t.outcrop == iocr
                 and t.sim == SIMlist[0]][0]
    Atot_ver += oca_ver
    Atot_tri += oca_tri
    Atot_dfram += oca_dfram
    print(template1.format(ocr, len(OCverts[iocr]), oca_dfram, oca_ver, oca_tri, OCpsums[iocr]))

    line_to_report = template_html.format(outcrop_names[iocr], len(OCverts[iocr]), oca_ver, oca_tri, OCpsums[iocr])
    print_html_report(report_path, '_OUTCROP AREAS_', '              <tr>', keep_keyword=True)
    print_html_report(report_path, '_OUTCROP AREAS_', line_to_report, keep_keyword=True)
    print_html_report(report_path, '_OUTCROP AREAS_', '              </tr>', keep_keyword=True)

print(template0.format("", "", "----------", "---------", "---------", ""))
print(template2.format("Total", "", Atot_dfram, Atot_ver, Atot_tri))
template_html = '                <td>{0}</td> <td>{1}</td> <td>{2:9.2f}</td> <td>{3:9.2f}</td> <td>{0}</td>'
line_to_report = template_html.format('Total', ' ', Atot_ver, Atot_tri, ' ')
print_html_report(report_path, '_OUTCROP AREAS_', '              <tr>', keep_keyword=True)
print_html_report(report_path, '_OUTCROP AREAS_', line_to_report, keep_keyword=True)
print_html_report(report_path, '_OUTCROP AREAS_', '              </tr>', keep_keyword=False)


print("\nOutcrops' placement and shape")
print("-----------------------------")
for iocr, ocr in enumerate(outcrop_names):
    img_name = plot3d_OC(OCverts[iocr], OCpverts[iocr], ocr, out_path)
    cur_img = '            <img src="%s" alt="%s" width="400">' % (img_name, outcrop_names[iocr])
    if iocr < (len(outcrop_names)-1):
        print_html_report(report_path, '_IMAGES OUTCROPS_', cur_img, keep_keyword=True)
    else:
        print_html_report(report_path, '_IMAGES OUTCROPS_', cur_img, keep_keyword=False)

fig_label("Placement of outcrops in S-JTSK coordinate system \
(blue... original vertices, red ... vertices projected to principal plane)")

del oca_ver, oca_dfram, oca_tri

# Verification of Fisher stats calculated by DFraM vs DFN_verify
# Fisher parameters identified by DFraM
DFraM_myus = []
for ipop, dip in enumerate(DFraM_dips):
    strike = DFraM_strikes[ipop]
    DFraM_myus.append(sd_to_vec(strike, dip))


# Calcluate parameters for Fisher distribution (observation and all simulations) using the same assumption as DFraM
# (no directional bias and OC relevance correction, kappa by simple formula)
sec_heading("Fisher statistics")
print("Here we compare the parameters of Fisher's distribution and strike and dip \
calculated by DFN_verify (ref. values), identified by DFraM and calculated \
by DFN_verify from", SIMcount, "DFraM simulations.")
print("Difference of the direction (mean vector, strike and dip) is given as spatial \
angle between the mean vectors.\n")

tab_label("Fisher statistics for individual populations")
# templates for print
template0 = '{0:<12} {1:>6}|{2:>6}|{3:^20}|{4:>6}|{5:>6}|{6:>6}'
template1 = '{0:<12} {1:6.1f} {2:6.1f} {3:6.3f} {4:6.3f} {5:6.3f} {6:<11} {7:6.2f}'
template2 = '{0:<12} {1:6.1f} {2:6.1f} {3:6.3f} {4:6.3f} {5:6.3f} {6:11.1f} {7:6.2f} {8:9.1f}'
template_html_ident = '                <td>{0:<12}</td> <td>{1:6.1f}</td> <td>{2:6.1f}</td> <td>{3:6.3f}</td> ' \
                      '<td>{4:6.3f}</td> <td>{5:6.3f}</td> <td>{6:<11}</td> <td>{7:6.2f}</td>'
template_html_simul = '                <td>{0:<12}</td> <td>{1:6.1f}</td> <td>{2:6.1f}</td> <td>{3:6.3f}</td> ' \
                      '<td>{4:6.3f}</td> <td>{5:6.3f}</td> <td>{6:11.1f}</td> <td>{7:6.2f}</td> <td>{8:9.1f}</td>'

for ipop, pop in enumerate(POPlist):
    print("\nPopulation:", pop)
    print(template0.format("", "Strike", "Dip", "Unit mean vector", "diff. [deg]", "kappa", "diff. [%]"))
    obsF = []  # Fisher stats calculated here from observation data
    simF = []  # Fisher stats calculated here from simulation data
    Pr = 0.95
    simlist = [[t.dip, t.strike, 1.0] for t in simtraces if (t.pop == pop)]
    obslist = [[t.dip, t.strike, 1.0] for t in obstraces if (t.pop == pop)]
    simF = cal_Fisher(simlist, Pr, False)
    obsF = cal_Fisher(obslist, Pr, False)
    sim_sd = vec_to_sd(simF[3])
    obs_sd = vec_to_sd(obsF[3])
    ani = vecs_angle(obsF[3], DFraM_myus[ipop])
    ans = vecs_angle(obsF[3], simF[3])
    if obsF[4] != 0:
        eri = (DFraM_kappas[ipop] - obsF[4]) / obsF[4] * 100
        ers = (simF[4] - obsF[4]) / obsF[4] * 100
    else:
        eri = 1e-10
        ers = 1e-10

    print(template1.format("DFN_verify", *obs_sd, *obsF[3], "", obsF[4]))
    line_to_report_ident = template_html_ident.format('DFraM identification', *obs_sd, *obsF[3], '', obsF[4])

    print(template2.format("DFraM ident.", DFraM_strikes[ipop], DFraM_dips[ipop], *DFraM_myus[ipop], ani,
                           DFraM_kappas[ipop], eri))
    print(template2.format("DFraM sim's.", *sim_sd, *simF[3], ans, simF[4], ers))
    line_to_report_simul = template_html_simul.format('DFraM simulations', *sim_sd, *simF[3], ans, simF[4], ers)

    print_html_report(report_path, '_FISHER TABLE_', '            <table>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              <tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '                <th>Population %d</th>' % pop, keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              </tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              <tr>', keep_keyword=True)
    tab_header = '                <th> </th> <th>Strike [°]</th> <th>Dip [°]</th> <th colspan="3"><b>&mu;</b></th> ' \
                 '<th><i>&theta;</i> [°]</th> <th><i>&kappa;</i></th> <th>Diff. in <i>&kappa;</i> [%]</th>'
    print_html_report(report_path, '_FISHER TABLE_', '              </tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              <tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', tab_header, keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              </tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              <tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', line_to_report_ident, keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              </tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              <tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', line_to_report_simul, keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '              </tr>', keep_keyword=True)
    print_html_report(report_path, '_FISHER TABLE_', '            </table>', keep_keyword=True)
    # print_html_report(report_path, '_FISHER TABLE_', '            <p><br /></p>', keep_keyword=True)
print_html_report(report_path, '_FISHER TABLE_', '', keep_keyword=False)

del simF, obsF, simlist, obslist, ani, ans, eri, ers, Pr

# ==================================
# Analyze trace length data (powerlaw)
# ==================================
sec_heading("Fracture size and fracture density")
# print("\n==========================================")
print("Trace length CDF's")
print("------------------")
# print("Observation vs. {0:3} DFraM simulations".format(SIMcount))

# Create files of tracelength CDF
for ipop, pop in enumerate(POPlist):
    # create array of observation trace lengths and sort it
    obslen = [t.length for t in obstraces if (t.pop == pop)]
    obslen.sort()
    # create frequency vector
    n = len(obslen)
    obsfvec = [(n - i) / n for i in range(len(obslen))]
    # create array of observation trace lengths and sort it
    simlen = [t.length for t in simtraces if (t.pop == pop)]
    simlen.sort()
    # create frequency vector
    n = len(simlen)
    simfvec = [(n - i) / n for i in range(len(simlen))]

    img_name = graph_loglog_01([[obslen, obsfvec], [simlen, simfvec]], ['Observation', str(SIMcount) + ' simulations'],
                               r'Trace length $l$ [m]', r'$\Pr(trace \leq l$)', 'Population ' + str(pop), out_path)
    cur_img = '            <img src="%s" alt="cdf population %d" width="600">' % (img_name, ipop+1)
    print_html_report(report_path, '_IMAGES CDF_', cur_img, keep_keyword=True)
print_html_report(report_path, '_IMAGES CDF_', ' ', keep_keyword=False)

fig_label("CDF's of trace lengths for individual \
populations - observation and " + str(SIMcount) + " DFraM simulations")

del n, obsfvec, simfvec, obslen, simlen
# =============================================================================
# ## Per simulation
#     for isim, sim in enumerate (SIMlist):
# ### Create array of observation trace lengths and sort it
#             simlen=[t.length for t in simtraces if (t.pop == pop and t.sim == sim)]
#             simlen.sort()
# ### Create frequency vector
#             n=len(simlen)
#             simfvec = [(n-i)/n for i in range(len(simlen))]
# ## CDF - print observation data
#             outfile3=open(verpath+"cdf_sim"+str(sim)+"_pop"+str(pop)+".txt","w")
#             for il, litem in enumerate(simlen):
#                 print(litem,simfvec[il],file=outfile3)
#             outfile3.close()
# =============================================================================


# Overall fracture lengths and counts
# print("\n==========================================")
print("\nTotal number and length of traces")
print("---------------------------------------------------")
# print("Observation vs. mean of {0:3} simulations by DFraM\n".format(SIMcount))
tab_label("Total number and length of traces over all otcrops \
- observation and mean of " + str(SIMcount) + " DFraM simulations")

print("             Ntot  | ltot [m]")
# Total number and length of traces
l_obs_allpop = 0.0
N_obs_allpop = 0.0
l_sim_allpop = 0.0
N_sim_allpop = 0.0

template_html = '                <td>{0:3d}</td> <td>{1:7}</td> <td>{2:7.2f}</td> <td>{3:7.2f}</td> <td>{4:7.2f}</td>'
for ipop, pop in enumerate(POPlist):
    l_obs = 0.0
    N_obs = 0
    for itrace, trace in enumerate(obstraces):
        if trace.pop == pop:
            l_obs = l_obs + trace.length
            N_obs = N_obs + 1
    Ntot_obs = N_obs  # /Atot_ver
    ltot_obs = l_obs  # /Atot_ver

    Ntot_sim = []
    ltot_sim = []
    for isim, sim in enumerate(SIMlist):
        l_sim = 0.0
        N_sim = 0
        for itrace, trace in enumerate(simtraces):
            if trace.pop == pop and trace.sim == sim:
                l_sim = l_sim + trace.length
                N_sim = N_sim + 1
        Ntot_sim.append(N_sim)  # /Atot_ver
        ltot_sim.append(l_sim)  # /Atot_ver

    l_obs_allpop = l_obs_allpop + ltot_obs
    N_obs_allpop = N_obs_allpop + Ntot_obs
    l_sim_allpop = l_sim_allpop + np.mean(ltot_sim)
    N_sim_allpop = N_sim_allpop + np.mean(Ntot_sim)

    print("---------------------------")
    print("Population:", pop)
    print("Observation {0:7} {1:7.2f}".format(Ntot_obs, ltot_obs))
    print("Simulations {0:7.2f} {1:7.2f}".format(np.mean(Ntot_sim), np.mean(ltot_sim)))

    line_to_report = template_html.format(pop, Ntot_obs, ltot_obs, np.mean(Ntot_sim), np.mean(ltot_sim))
    print_html_report(report_path, '_LENGTH OF TRACES_', '              <tr>', keep_keyword=True)
    print_html_report(report_path, '_LENGTH OF TRACES_', line_to_report, keep_keyword=True)
    print_html_report(report_path, '_LENGTH OF TRACES_', '              </tr>', keep_keyword=True)

print("---------------------------")
print("All populatios:")
print("Observation {0:7} {1:7.2f}".format(N_obs_allpop, l_obs_allpop))
print("Simulations {0:7.2f} {1:7.2f}".format(N_sim_allpop, l_sim_allpop))
template_html = '                <td>All populations</td> <td>{0:7}</td> <td>{1:7.2f}</td> <td>{2:7.2f}</td> ' \
                '<td>{3:7.2f}</td>'
line_to_report = template_html.format(N_obs_allpop, l_obs_allpop, N_sim_allpop, l_sim_allpop)
print_html_report(report_path, '_LENGTH OF TRACES_', '              <tr>', keep_keyword=True)
print_html_report(report_path, '_LENGTH OF TRACES_', line_to_report, keep_keyword=True)
print_html_report(report_path, '_LENGTH OF TRACES_', '              </tr>', keep_keyword=False)

del Ntot_obs, ltot_obs, Ntot_sim, ltot_sim
del N_obs_allpop, l_obs_allpop, N_sim_allpop, l_sim_allpop

# Plot relation of P20, P21, N and lav on outcrop area for population pop
# print("\n==========================================")

print("\nAverage length, number, and densities of traces")
print("-----------------------------------------------")
densities_vs_OCarea('all', obstraces, simtraces, OClist, OCareas, SIMcount, out_path, report_path)

for ipop, pop in enumerate(POPlist):
    OF_a = 0.0
    OF_a_obs = 0.0
    OF_a_sim = 0.0
    OF_P30p = 0.0  # based on P20
    OF_P30p_obs = 0.0
    OF_P30p_sim = 0.0
    OF_P30n = 0.0  # based on N ... no. of traces on outcrop
    OF_P30n_obs = 0.0
    OF_P30n_sim = 0.0
    for iocr, ocr in enumerate(OClist):
        w = OCrlvs[iocr] / 10.0
        t_obs = [t.length for t in obstraces if t.outcrop == ocr
                 and t.pop == pop]  # list of traces for pop and ocr
        N_obs = len(t_obs)  # number of traces - observation
        t_sim = [t.length for t in simtraces if t.outcrop == ocr
                 and t.pop == pop]  # list of traces for pop and ocr, all sims
        N_sim = len(t_sim) / SIMcount  # number of traces - average of all simulations
        if N_obs != 0 and N_sim != 0:  # skip if there is no trace on ocr
            lav_obs = np.mean(t_obs)
            lav_sim = np.mean(t_sim)
            OF_a = OF_a + (lav_sim - lav_obs) ** 2 * w
            OF_a_obs = OF_a_obs + (lav_obs) ** 2 * w
            OF_a_sim = OF_a_sim + (lav_sim) ** 2 * w
        A = OCareas[iocr]
        OF_P30p = OF_P30p + (N_sim / A - N_obs / A) ** 2 * w
        OF_P30p_obs = OF_P30p_obs + (N_obs / A) ** 2 * w
        OF_P30p_sim = OF_P30p_sim + (N_sim / A) ** 2 * w
        OF_P30n = OF_P30n + (N_sim - N_obs) ** 2 * w
        OF_P30n_obs = OF_P30n_obs + (N_obs) ** 2 * w
        OF_P30n_sim = OF_P30n_sim + (N_sim) ** 2 * w

del OF_a, OF_a_obs, OF_a_sim, OF_P30p, OF_P30p_obs, OF_P30p_sim, OF_P30n, OF_P30n_obs, OF_P30n_sim
del A, t_obs, t_sim, N_obs, N_sim

# ==========================
# Advanced analyses
# ==========================
sec_heading("Analysis of outcrop orientation and bias ")

# Analyze orientations of outcrops
# print("\n==========================================")
print("\nPoles of outcrops")
print("-----------------")
print("Check if outcrops are randomly oriented. If they are, the effect of directional bias should be small.")

fig_counter = get_figcounter()

fig = plt.figure(figsize=(4.5, 3), dpi=300)
ax = fig.add_subplot(111, projection='stereonet')
for iocr, ocr in enumerate(OClist):
    ocnvec = OCnvecs[iocr]
    sd = vec_to_sd(ocnvec)
    tp = vec_to_tp(ocnvec)
    ax.pole(sd[0], sd[1], 'bo', markersize=5)
ax.grid()
fig_counter += 1
n_fig = '%02d_' % (fig_counter + 1)
plt.tight_layout()
plt.savefig('%s/images/%ssterograms_orientation_outcrops.png' % (out_path, n_fig))
fig_label("Poles of all analyzed outcrops")

# Analyze Tezaghi correction factors
obstcflist = [t.tcf for t in obstraces]
simtcflist = [t.tcf for t in simtraces]
# print("\n==========================================")
print("\nTerzaghi correction factors for fractures at outcrops")
print("-----------------------------------------------------")
print("This is also an indicator of the directional bias. The more traces have \
the value of the factor close to 1, the lesser is the bias. Nw/N is the average \
value of the weighting factor for all fractures at outcrops (Priest 2012, eq. 3.12).\
 Close to 1 is better.")

plt.figure(figsize=(4.5, 3), dpi=300)
ax = plt.gca()
plt.title('Terzaghi correction factor - observation data')
plt.xlabel('Terzaghi correction factor [-]')
plt.ylabel('No. of fractures')
plt.hist(obstcflist, bins='auto')
fig_counter += 1
n_fig = '%02d_' % (fig_counter + 1)
plt.tight_layout()
plt.savefig('%s/images/%sTerzaghi_correction_histogram_observations.png' % (out_path, n_fig))

print("Nw/N = {0:6.3f}\n".format(sum(obstcflist) / len(obstcflist)))
fig_label("Histogram of Terzaghi correction factor for observation data")

plt.figure(figsize=(4.5, 3), dpi=300)
ax = plt.gca()
plt.title('Terzaghi correction factor - all simulations data')
plt.xlabel('Terzaghi correction factor [-]')
plt.ylabel('No. of fractures')
plt.hist(simtcflist, bins='auto')
fig_counter += 1
n_fig = '%02d_' % (fig_counter + 1)
plt.tight_layout()
plt.savefig('%s/images/%sTerzaghi_correction_histogram_simulations.png' % (out_path, n_fig))

print("Nw/N = {0:6.3f}\n".format(sum(simtcflist) / len(simtcflist)))
fig_label("Histogram of Terzaghi correction factor for data from " + str(SIMcount) + " DFraM simulations")

del obstcflist, simtcflist, fig, ax

# Calcluate parameters for Fisher distribution taking into account
#   Terzaghi bias and outcrop relevance

Pr = 0.99  # Probability for confidence cone

# print("\n==========================================")
print("\nFisher statistics of observation data corrected for biases")
print("----------------------------------------------------------")
print("Here we analyze the effect of directional (Terzaghi) and outcrop-relevance biases.")
print("Theta is the anglular radius of the zone (cone) of", Pr * 100, "% confidence \
about the resultant mean normal vector. The larger is the area of overlap \
between the zones for the unweigheted (no correction) \
and weighted data, the lesser is the effect of bias.")
print("")

template0 = '{0:<10} {1:>6}|{2:>6}|{3:^20}|{4:>6}|{5:>6}|{6:>6}|{7:>6}|{8:>6}'
template1 = '{0:<10} {1:6.1f} {2:6.1f} {3:6.3f} {4:6.3f} {5:6.3f} {6:<11} {7:6.2f} {8:<9} {9:11.2f}'
template2 = '{0:<10} {1:6.1f} {2:6.1f} {3:6.3f} {4:6.3f} {5:6.3f} {6:11.1f} {7:6.2f} {8:9.1f} {9:11.2f} {10:9.1f}'
tab_label("Directional statistics and confidence zone plots evaluated for \
observation data without and with bias corrections")
for ipop, pop in enumerate(POPlist):

    Fs_nc = []  # Fisher stats without correction
    Fs_T = []  # Fisher stats with Terzaghi correction
    Fs_r = []  # Fisher stats with outcrop relevance correction
    Fs_Tr = []  # Fisher stats with Terzaghi and outcrop relevance correction

    list_nc = [[t.dip, t.strike, 1.0] for t in obstraces if (t.pop == pop)]
    list_T = [[t.dip, t.strike, t.tcf] for t in obstraces if (t.pop == pop)]
    list_r = [[t.dip, t.strike, t.rlv / 10.0] for t in obstraces if (t.pop == pop)]
    list_Tr = [[t.dip, t.strike, t.tcf * t.rlv / 10.0] for t in obstraces if (t.pop == pop)]
    # Fisher stats
    Fs_nc = cal_Fisher(list_nc, Pr, False)
    Fs_T = cal_Fisher(list_T, Pr, False)
    Fs_r = cal_Fisher(list_r, Pr, False)
    Fs_Tr = cal_Fisher(list_Tr, Pr, False)
    # Angle between mean normal vectors
    an_T = vecs_angle(Fs_nc[3], Fs_T[3])
    an_r = vecs_angle(Fs_nc[3], Fs_r[3])
    an_Tr = vecs_angle(Fs_nc[3], Fs_Tr[3])
    # Strike and dip
    sd_nc = vec_to_sd(Fs_nc[3])
    sd_T = vec_to_sd(Fs_T[3])
    sd_r = vec_to_sd(Fs_r[3])
    sd_Tr = vec_to_sd(Fs_Tr[3])
    # Plunge and bearing
    p_nc, b_nc = mplstereonet.stereonet_math.pole2plunge_bearing(sd_nc[0], sd_nc[1])
    p_T, b_T = mplstereonet.stereonet_math.pole2plunge_bearing(sd_T[0], sd_T[1])
    p_r, b_r = mplstereonet.stereonet_math.pole2plunge_bearing(sd_r[0], sd_r[1])
    p_Tr, b_Tr = mplstereonet.stereonet_math.pole2plunge_bearing(sd_Tr[0], sd_Tr[1])
    # Error on kappa (simple) and theta
    if Fs_T[4] != 0:
        erk_T = (Fs_nc[4] - Fs_T[4]) / Fs_T[4] * 100
        erk_r = (Fs_nc[4] - Fs_r[4]) / Fs_r[4] * 100
        erk_Tr = (Fs_nc[4] - Fs_Tr[4]) / Fs_Tr[4] * 100
    else:
        erk_T = 1e-10
        erk_r = 1e-10
        erk_Tr = 1e-10
    if Fs_T[6] != 0:
        ert_T = (Fs_nc[6] - Fs_T[6]) / Fs_T[6] * 100
        ert_r = (Fs_nc[6] - Fs_r[6]) / Fs_r[6] * 100
        ert_Tr = (Fs_nc[6] - Fs_Tr[6]) / Fs_Tr[6] * 100
    else:
        ert_T = 1e-10
        ert_r = 1e-10
        ert_Tr = 1e-10
    # print table
    print("\nPopulation:", pop, "(N = " + str(len(list_nc)) + ")")
    print(template0.format("", "Strike", "Dip", "Unit mean vector", "diff. [deg]", "kappa", "diff. [%]", "theta [deg]",
                           "diff. [%]"))
    print(template1.format("No corr.", *sd_nc, *Fs_nc[3], "", Fs_nc[4], "", Fs_nc[6]))
    print(template2.format("Terzaghi", *sd_T, *Fs_T[3], an_T, Fs_T[4], erk_T, Fs_T[6], ert_T))
    print(template2.format("Relevance", *sd_r, *Fs_r[3], an_r, Fs_r[4], erk_r, Fs_r[6], ert_r))
    print(template2.format("Terz & rlv", *sd_Tr, *Fs_Tr[3], an_Tr, Fs_Tr[4], erk_Tr, Fs_Tr[6], ert_Tr))
    # Print stereogram
    fig, ax = mplstereonet.subplots()
    ax.pole(sd_nc[0], sd_nc[1], color='c', marker="+", label="No correction")
    ax.pole(sd_T[0], sd_T[1], color='b', marker="+", label="Terzaghi correction")
    ax.pole(sd_r[0], sd_r[1], color='g', marker="+", label="Relevance correction")
    ax.pole(sd_Tr[0], sd_Tr[1], color='r', marker="+", label="Terz. & rlv. correction")
    ax.legend(loc='upper right', bbox_to_anchor=(2, 1.1))
    ax.cone(p_nc, b_nc, Fs_nc[6], bidirectional=True, facecolor='', zorder=4, linewidth=2,
            edgecolors=['cyan'])
    ax.cone(p_T, b_T, Fs_T[6], bidirectional=True, facecolor='', zorder=4, linewidth=2,
            edgecolors=['blue'])
    ax.cone(p_r, b_r, Fs_r[6], bidirectional=True, facecolor='', zorder=4, linewidth=2,
            edgecolors=['green'])
    ax.cone(p_Tr, b_Tr, Fs_Tr[6], bidirectional=True, facecolor='', zorder=4, linewidth=2,
            edgecolors=['red'])
    ax.grid()
    fig_counter += 1
    n_fig = '%02d_' % (fig_counter + 1)
    plt.tight_layout()
    plt.savefig('%s/images/%ssterograms_population-%d.png' % (out_path, n_fig, ipop+1))

del Fs_nc, sd_nc, p_nc, b_nc, list_nc
del Fs_T, an_T, sd_T, p_T, b_T, erk_T, ert_T, list_T
del Fs_r, an_r, sd_r, p_r, b_r, erk_r, ert_r, list_r
del Fs_Tr, an_Tr, sd_Tr, p_Tr, b_Tr, erk_Tr, ert_Tr, list_Tr
del template0, template1, template2
