#
#    Copyright 2016-2021 Václav Nežerka 
#    Copyright 2016-2020 Michael Somr
#    Copyright 2016-2021 Petr Kabele
#    Copyright 2016-2017 Jan Zeman
#
#    This file is part of DFraM.
#
#    DFraM is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or any later version.
#
#    DFraM is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with DFraM.  If not, see <https://www.gnu.org/licenses/>.
#
def of_lengths(mean_lengths_in, mean_lengths_out, outcrop_weights):
    of_value, of_derivative, target, result = 0, 0, 0, 0,
    for idx in range(len(outcrop_weights)):
        if mean_lengths_out[idx] > 0 and mean_lengths_in[idx] > 0:
            res_obs = mean_lengths_in[idx] * 1.0
            res_sim = mean_lengths_out[idx] * 1.0
            wi = outcrop_weights[idx] * 1.0
            of_value += ((res_sim-res_obs)**2)*wi
            of_derivative += (res_sim-res_obs)*wi
            target += res_obs*wi
            result += res_sim*wi
    if result > 0:        
        iter_1_value = target/result
    else:
        iter_1_value = 0.5

    return of_value, of_derivative, iter_1_value


def of_densities(dens_in, dens_out, n_fr_in, n_fr_out, opt_dens, outcrop_weights):
    of_value, of_derivative, target, result = 0, 0, 0, 0
    for idx in range(len(outcrop_weights)):
        if opt_dens:
            res_obs = dens_in[idx] * 1.0
            res_sim = dens_out[idx] * 1.0
        else:
            res_obs = n_fr_in[idx] * 1.0
            res_sim = n_fr_out[idx] * 1.0
        wi = outcrop_weights[idx]
        of_value += ((res_sim-res_obs)**2)*wi
        of_derivative += (res_sim-res_obs)*wi
        target += res_obs*wi
        result += res_sim*wi
    if result > 0:        
        iter_1_value = target/result
    else:
        iter_1_value = 0.5

    return of_value, of_derivative, iter_1_value
